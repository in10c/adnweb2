import React, { useContext, useEffect, useState } from "react"
import { Redirect } from "react-router-dom"
import { UserContext } from "../Context/UserContext"
import { useTranslation } from "react-i18next"
import Header from "./Header"
import Container from "./Container"
import Footer from "./Footer"

export default function Panel({
    title,
    smallBackground,
    children,
    hideHeader,
    backgroundWhite,
    secondFooter,
    headerChild,
}) {
    const { t } = useTranslation()
    const { isAnUserAuthenticated } = useContext(UserContext)

    const [redirect, setRedirect] = useState(undefined)

    useEffect(() => {
        if (isAnUserAuthenticated !== undefined) setRedirect(!isAnUserAuthenticated)
    }, [isAnUserAuthenticated])

    return (
        <div id="panel" className={`${backgroundWhite ? "background-white" : ""}`}>
            {redirect && <Redirect to="/acceder" />}
            {!hideHeader && (
                <Header smallBackground={smallBackground}>
                    {smallBackground ? (
                        <div className="row my-5 pt-5 d-flex justify-content-center">
                            <h2 id="confirmed-title" className="text-center">
                                {title}
                            </h2>
                        </div>
                    ) : (
                        <React.Fragment>
                            {headerChild || (
                                <React.Fragment>
                                    <div className="row my-5 pt-5 d-flex justify-content-center">
                                        <h2 id="confirmed-title">{t("Panel.confirmed-title")}</h2>
                                    </div>
                                    <div className="row my-5 d-flex flex-column align-items-center">
                                        <h2 className="section-title">{t("Panel.get-adn-crypto")}</h2>
                                        <p id="confirmed-description">{t("Panel.confirmed-description")}</p>
                                    </div>
                                    <div className="row mt-2 d-flex justify-content-center">
                                        <img width="64" height="25" src="/images/svg/gray-arrow.svg" />
                                    </div>
                                </React.Fragment>
                            )}
                        </React.Fragment>
                    )}
                </Header>
            )}
            {children}
            <Footer secondFooter={secondFooter} />
        </div>
    )
}
