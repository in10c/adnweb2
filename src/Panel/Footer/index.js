import React from "react"
import { useTranslation } from "react-i18next"
import "./styles.css"

export default function Footer({ secondFooter }) {
  const { t } = useTranslation()

  return (
    <div
      id="panel-footer-section"
      className={`${secondFooter ? "second-footer" : ""}`}
    >
      <div className="container">
        <div className="row">
          <div className="col-md-6">
            <div className="mb-1">
              <img
                width="159"
                height="30"
                src="/images/svg/logo.svg"
                alt="footer-logo"
              />
            </div>
            <div className="copyright-text">
              {t("Panel.Footer.copyright-text")}
            </div>
          </div>
          <div className="col-md-6 text-right social-media">
            <a href="#">
              <img
                className="mx-3"
                width="40"
                height="45"
                src="/images/svg/telegram-logo.svg"
                alt="telegram-logo"
              />
            </a>
            <a href="#">
              <img
                className="mx-3"
                width="50"
                height="45"
                src="/images/svg/twitter-logo.svg"
                alt="twitter-logo"
              />
            </a>
            <a href="#">
              <img
                className="mx-3"
                width="40"
                height="45"
                src="/images/svg/instagram-logo.svg"
                alt="instagram-logo"
              />
            </a>
          </div>
        </div>
      </div>
    </div>
  )
}
