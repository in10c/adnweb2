import React, { useState, useContext } from "react"
import { NavLink, Link } from "react-router-dom"
import { useTranslation } from "react-i18next"
import { UserContext } from "../../Context/UserContext"
import LanguageSelector from "../../componentes/LanguageSelector"
import "./styles.css"

export default function Header({ smallBackground, children }) {
    const { user, signOutUser } = useContext(UserContext)
    const { t } = useTranslation()
    const [menuToggled, setMenuToggled] = useState(false)

    return (
        <div id="panel-header-container" className={smallBackground ? "smallBackground" : ""}>
            <div className="container">
                <div className="row mb-5">
                    <nav
                        className="navbar navbar-expand-lg navbar-dark d-flex"
                        style={{ backgroundColor: "#FFFFFF00", width: "100%" }}
                    >
                        <Link className="navbar-brand" to="/panel">
                            <img
                                src="/images/svg/logo.svg"
                                id="header-logo"
                                width="254"
                                height="48"
                                alt="header-logo"
                            />
                        </Link>
                        <a
                            id="support-button"
                            href="https://telegram.me/soporteadncrypto"
                            target="_blank"
                            className="ml-auto"
                        >
                            {t("Panel.Header.support-button")}
                        </a>
                        <LanguageSelector />
                        <div className="toggle">
                            <button
                                className="dropdown-toggle"
                                id="toggle-button"
                                onClick={() => {
                                    setMenuToggled(!menuToggled)
                                }}
                            >
                                {t("Panel.Header.toggle-button")} {user.nombre}
                            </button>
                            <div id="toggle-menu" className={`${menuToggled ? "visible" : ""}`}>
                                <Link to="/panel" className="toggle-item">
                                    {t("Panel.Header.toggle-menu.home")}
                                </Link>
                                <Link to="/controlPanel/facturas" className="toggle-item">
                                    {t("Panel.Header.toggle-menu.account")}
                                </Link>
                                <Link className="toggle-item" component="a" to="/paquetes">
                                    {t("Panel.Header.toggle-menu.upgrade")}
                                </Link>
                                <a
                                    className="toggle-item"
                                    onClick={() => {
                                        signOutUser()
                                    }}
                                >
                                    {t("Panel.Header.toggle-menu.logout")}
                                </a>
                            </div>
                        </div>
                    </nav>
                </div>
                {children}
            </div>
        </div>
    )
}
