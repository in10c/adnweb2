import React, { useContext, useEffect, useState } from "react"
import * as FirebaseMngr from "../../Utilities/FirebaseMngr"
import { UserContext } from "../../Context/UserContext"
import Panel from "../../Panel"

export default function Tree() {
    const { user } = useContext(UserContext)

    const [tree, setTree] = useState(undefined)

    const [path, setPath] = useState(undefined)
    const firebaseDatabase = FirebaseMngr.get()
    const [packages, setPackages] = useState({})

    useEffect(() => {
        const getInfo = async () => {
            if (user.userID) {
                const snapshot = await firebaseDatabase
                    .ref("path/" + user.userID)
                    .once("value")
                const pathArray = snapshot.val()
                let pathFormat = ""
                if (pathArray) {
                    for (let index = 0; index < pathArray.length; index++) {
                        const referUser = pathArray[index]
                        pathFormat += referUser + "/children/"
                    }
                }
                setPath(pathFormat)
            }
        }
        getInfo()
    }, [user])

    useEffect(() => {
        if (path != undefined && user.userID) {
            firebaseDatabase
                .ref("tree/" + path + user.userID)
                .on("value", (snap) => {
                    let returnedTree = snap.val()
                    setTree(returnedTree)
                })
        }
    }, [path])

    useEffect(() => {
        const getInfo = async () => {
            const snapshot = await firebaseDatabase.ref("packages").once("value")
            const response = snapshot.val()
            if (response) {
                setPackages(response)
            }
        }
        getInfo()
    }, [])

    return (
        <Panel title="Mis Referidos">
            <div className="content-body">
                <div className="container-fluid">
                    <React.Fragment>
                        {tree === undefined ? (
                            <div></div>
                        ) : (
                            <div className="row">
                                {tree === null || tree.children === undefined ? (
                                    <div className="col-xl-12">
                                        <div className="alert alert-warning left-icon-big alert-dismissible fade show">
                                            <div className="media">
                                                <div className="alert-left-icon-big">
                                                    <span>
                                                        <i className="mdi mdi-help-circle-outline"></i>
                                                    </span>
                                                </div>
                                                <div className="media-body">
                                                    <h5 className="mt-1 mb-2">Vacío</h5>
                                                    <p className="mb-0">Aún no tienes ningun referido.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                ) : (
                                    <div className="col-lg-12">
                                        <div className="card">
                                            <div className="card-body">
                                                <div className="table-responsive">
                                                    <table className="table table-sm mb-0 table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>Nombre</th>
                                                                <th>Email</th>
                                                                <th>Paquete</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="customers">
                                                            {Object.keys(tree.children).map((index, i) => {
                                                                const child = tree.children[index]
                                                                return (
                                                                    <React.Fragment>
                                                                        <tr className="btn-reveal-trigger" key={i}>
                                                                            <td className="py-3">
                                                                                <a href="#">
                                                                                    <div className="media d-flex align-items-center">
                                                                                        <div className="avatar avatar-xl mr-2">
                                                                                            <div className="">
                                                                                                <img
                                                                                                    className="rounded-circle img-fluid"
                                                                                                    src="./images/avatar/5.png"
                                                                                                    width="30"
                                                                                                    alt=""
                                                                                                />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div className="media-body">
                                                                                            <h5 className="mb-0 fs--1">
                                                                                                {child.name}
                                                                                            </h5>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </td>
                                                                            <td className="py-2">
                                                                                <a href={`mailto:${child.email}`}>
                                                                                    {child.email}
                                                                                </a>
                                                                            </td>
                                                                            <td className="py-2">
                                                                                {packages[child.packageID]
                                                                                    ? packages[child.packageID].name
                                                                                    : ""}
                                                                            </td>
                                                                        </tr>
                                                                    </React.Fragment>
                                                                )
                                                            })}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )}
                            </div>
                        )}
                    </React.Fragment>
                </div>
            </div>
        </Panel>
    )
}
