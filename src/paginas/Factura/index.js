import React, { useContext, useEffect, useState } from "react"
import { NavLink, Redirect, useParams } from "react-router-dom"
import * as FirebaseMngr from "../../Utilities/FirebaseMngr"
import Loader from "react-loader-spinner"
import Axios from "axios"
import { useTranslation } from "react-i18next"
import { UserContext } from "../../Context/UserContext"
import Constants from "../../Utilities/Constants"
import Panel from "../../Panel"
import { dateWithFormatFromTime } from "../../Utilities/DateUtilities.js"
import "./index.css"

export default function Factura() {
    const { t } = useTranslation()
    const { invoiceID } = useParams()
    const { cancelPackageRequestFromUser, user } = useContext(UserContext)
    const firebaseDatabase = FirebaseMngr.get()
    const [invoice, setInvoice] = useState(undefined)
    const [leftTime, setLeftTime] = useState(0)
    const [cargando, setCargando] = useState(false)
    const [redirect, setRedirect] = useState(false)
    const [packageName, setpackageName] = useState("")
    const tipoPaquetes = [
        t("Pages.Package-request.packages.types.monthly"),
        t("Pages.Package-request.packages.types.quarterly"),
        t("Pages.Package-request.packages.types.biannual"),
        t("Pages.Package-request.packages.types.annual"),
    ]

    useEffect(() => {
        if (invoiceID && user && user.userID)
            firebaseDatabase.ref("facturas/" + user.userID + "/" + invoiceID).on("value", async (snap) => {
                let returnedInvoice = snap.val()
                console.log("la factura es ", returnedInvoice)
                if (returnedInvoice) {
                    setInvoice(returnedInvoice)
                    const snapshot = await firebaseDatabase.ref("packages/" + returnedInvoice.packageID).once("value")
                    const packageItem = snapshot.val()
                    if (packageItem) {
                        setpackageName(packageItem.name)
                        if (returnedInvoice.status === "pending") {
                            tiempoQueda(returnedInvoice)
                        }
                    }
                } else setRedirect(true)
            })
    }, [user, invoiceID])

    const cancelarFactura = async (ev) => {
        ev.preventDefault()
        if (window.confirm(t("Pages.Invoice.cancel.confirm"))) {
            setCargando(true)
            const response = await Axios.post(Constants.servidorFunciones() + "/cancelPackageRequest", {
                userID: user.userID,
                facturaID: invoiceID,
            })
            setCargando(false)
            console.log("respuesta de cancelar factura", response)
            //setRedirect(response.data.facturaID)
        }
    }

    const tiempoQueda = async (returnedInvoice) => {
        let tiempoPasado = new Date().getTime() - returnedInvoice.createdAt
        let seconds = tiempoPasado / 1000
        let minutes = seconds / 60
        let restante = minutes.toString().split(".")

        let minuquedan = parseInt(restante[0])
        let segundosresta = minuquedan * 60
        let minuresta = 20 - minuquedan
        let segundosPasados = (seconds - segundosresta).toString().split(".")[0]
        let segundosQuedan = 60 - parseInt(segundosPasados)
        console.log("el tiempo pasado", minuquedan, restante, segundosresta, seconds, segundosPasados)
        setLeftTime(
            minuresta -
                1 +
                " " +
                t("Pages.Invoice.timer.minutes") +
                " " +
                segundosQuedan +
                " " +
                t("Pages.Invoice.timer.seconds")
        )
        setTimeout(() => tiempoQueda(returnedInvoice), 1000)
    }

    return (
        <Panel smallBackground={true} title="Facturación">
            <React.Fragment>
                {invoice && (
                    <React.Fragment>
                        <div className="container pb-5">
                            <div className="row">
                                <div className="col-lg-6 col-md-12 route">
                                    <NavLink to="/controlPanel/facturas" className="route-link">
                                        {t("Pages.Invoice.return-buttons.to-list")}
                                    </NavLink>{" "}
                                    <span className="route-arrow">&gt;</span>{" "}
                                    <span className="route-current">
                                        {t("Pages.Invoice.return-buttons.to-details")}
                                    </span>
                                </div>
                                <div className="col-lg-6 col-md-12 text-right">
                                    <NavLink to="/controlPanel/facturas" className="return-button">
                                        {t("Pages.Invoice.return-buttons.to-panel")}
                                    </NavLink>
                                </div>
                            </div>
                        </div>
                        <div className="container bg-white facturaDetalles">
                            <div className="row py-5 d-flex">
                                <div className="col-2 d-flex flex-column pr-0 mr-0">
                                    <div className="billing-element">{t("Pages.Invoice.billing.previus")}</div>
                                    <div className="billing-element selected">{t("Pages.Invoice.billing.invoice")}</div>
                                </div>
                                {!cargando ? (
                                    <div className="col-10 ml-0 info-container">
                                        <h5 style={{ textAlign: "right" }}>
                                            <strong>{t("Pages.Invoice.current.invoice")}:</strong> {invoiceID}
                                        </h5>
                                        <p style={{ textAlign: "right" }}>
                                            <strong>{t("Pages.Invoice.current.date")}:</strong>{" "}
                                            {dateWithFormatFromTime(invoice.createdAt).map((piece, i) =>
                                                !(i % 2) ? t(`Date.${i === 0 ? "days" : "months"}.${piece}`) : piece
                                            )}
                                            <br />
                                        </p>
                                        <table class="table" style={{ marginTop: 35 }}>
                                            <thead>
                                                <tr>
                                                    <th className="border-top-0" scope="col">
                                                        {t("Pages.Invoice.current.quantity")}
                                                    </th>
                                                    <th className="border-top-0" scope="col">
                                                        {t("Pages.Invoice.current.description")}
                                                    </th>
                                                    <th className="border-top-0" scope="col">
                                                        {t("Pages.Invoice.current.cost")}
                                                    </th>
                                                    <th className="border-top-0" scope="col">
                                                        {t("Pages.Invoice.current.to-pay")}
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr className="border-bottom">
                                                    <th scope="row">1</th>
                                                    <td>
                                                        {t(`Pages.Invoice.descriptions.${invoice.subject}`)} (
                                                        {tipoPaquetes[invoice.tipoPlan]})
                                                    </td>
                                                    <td>${invoice.amount}</td>
                                                    <td className="price-btc">
                                                        {!invoice.red
                                                            ? invoice.apagarBTC + " BTC"
                                                            : invoice.apagarUSDT + " USDT"}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <div className="row">
                                            <div className="col-md-6 col-lg-5 d-flex align-items-center border-right">
                                                <img
                                                    width="150"
                                                    height="150"
                                                    alt="qrcode"
                                                    src={
                                                        "https://chart.googleapis.com/chart?chs=225x225&chld=L|2&cht=qr&chl=" +
                                                        invoice.wallet
                                                    }
                                                />
                                                <div style={{ overflowWrap: "anywhere" }}>
                                                    <span className="btc-address-title">
                                                        {t("Pages.Invoice.checkout-info.address")}{" "}
                                                        {invoice.red ? "USDT (TRC20)" : "BTC"}
                                                    </span>{" "}
                                                    <span className="btc-address">{invoice.wallet}</span>
                                                </div>
                                            </div>
                                            {invoice.status === "pending" ? (
                                                <React.Fragment>
                                                    <div
                                                        className={
                                                            "col-md-6 col-lg-4 flex-column d-flex justify-content-center align-items-center border-right"
                                                        }
                                                    >
                                                        {invoice.status === "pending" && (
                                                            <React.Fragment>
                                                                <div>{t("Pages.Invoice.checkout-info.expires")}</div>
                                                                <div className="text-center">
                                                                    <strong className="left-time">{leftTime}</strong>
                                                                </div>
                                                            </React.Fragment>
                                                        )}
                                                    </div>
                                                    <div className="col-md-6 col-lg-3 d-flex justify-content-center align-items-center">
                                                        {!invoice.generated && invoice.status === "pending" && (
                                                            <button
                                                                type="button"
                                                                className="btn btn-outline-danger"
                                                                onClick={cancelarFactura}
                                                            >
                                                                {t("Pages.Invoice.cancel.buttton")}
                                                            </button>
                                                        )}
                                                    </div>
                                                </React.Fragment>
                                            ) : (
                                                <div className="col-md-12 col-lg-7 d-flex justify-content-center align-items-center">
                                                    <div
                                                        className={
                                                            "myAlert " +
                                                            (invoice.status == "cancelled"
                                                                ? "error"
                                                                : invoice.status == "paid"
                                                                ? "success"
                                                                : "alert")
                                                        }
                                                    >
                                                        {t(`Pages.Invoice.status.${invoice.status}`)}
                                                    </div>
                                                </div>
                                            )}
                                        </div>
                                        <div className="row" style={{ marginTop: 35, opacity: 0.6 }}>
                                            <p className="col-md-12">
                                                <small>* {t("Pages.Invoice.close.message")}</small>
                                                <br />
                                                <small>** {t("Pages.Invoice.close.warning")}</small>
                                            </p>
                                        </div>
                                    </div>
                                ) : (
                                    <div className="loaderWrap col-10">
                                        <Loader type="BallTriangle" color="#9b5bc1" height={100} width={100} />
                                    </div>
                                )}
                            </div>
                        </div>
                    </React.Fragment>
                )}
            </React.Fragment>
        </Panel>
    )
}
