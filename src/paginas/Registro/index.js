import React from "react"
import * as axios from "axios"
import { Link } from "react-router-dom"
import Loader from "react-loader-spinner"
import { useTranslation } from "react-i18next"
import Constants from "../../Utilities/Constants"
import Scroller from "../../componentes/Scroller"
import { FaEye, FaLaughWink } from "react-icons/fa"
import "./index.css"

export default function Registro() {
    const { t } = useTranslation()
    const [indice, setIndice] = React.useState(0)
    const [registroPublico, setRegistroPublico] = React.useState(false) //cambiar
    const [error, setError] = React.useState(false)
    const [listo, setListo] = React.useState(false) //cambiar
    const [tipoPass, setTipoPass] = React.useState(true)
    const [cargando, setCargando] = React.useState(false)
    const inputs = [React.useRef(null), React.useRef(null), React.useRef(null), React.useRef(null)]
    const [form, setForm] = React.useState({})
    const handleInputChange = (event) => {
        event.persist()
        setForm((form) => ({ ...form, [event.target.name]: event.target.value }))
    }
    const nextControl = (ev, step) => {
        ev.preventDefault()
        setIndice(step)
        if (step < 4) {
            setTimeout(() => {
                inputs[step].current.focus()
            }, 1000)
        }
    }
    const buscarBitwabi = async (ev) => {
        setCargando(true)
        ev.preventDefault()
        let response = await axios.post(Constants.servidorFunciones() + "/validateBWEmail", {
            correo: form.correo,
        })
        console.log("respuesta de buscar bitwabi", response)
        if (response.data.exito === 0) {
            setForm((form) => ({ ...form, deBitwabi: false }))
        } else {
            let usr = response.data.usuario
            setForm((form) => ({
                ...form,
                deBitwabi: true,
                nombre: usr.lastName ? usr.name + " " + usr.lastName : usr.name,
                apodo: usr.nickName,
                paquete: usr.selectedPackage,
            }))
        }
        setCargando(false)
        nextControl(ev, 1)
    }
    const regresaPunto = (dot) => {
        let str = "dot"
        if (dot === indice) {
            str += " hecho"
        }
        if (indice > dot) {
            str += " actual"
        }
        return str
    }
    const registrarUsuario = async (ev) => {
        setCargando(true)
        ev.preventDefault()
        let response = await axios.post(Constants.servidorFunciones() + "/registrarUsuario", form)
        if (response.data.success === 0) {
            setRegistroPublico(true)
            setError(response.data.error)
            setCargando(false)
        } else {
            setRegistroPublico(true)
            setListo(true)
            setCargando(false)
        }
        console.log("respuesta de buscar bitwabi", response)
    }
    React.useEffect(() => {
        inputs[0].current.focus() //cambiar
    }, [])
    return (
        <div className="fullScreen">
            <div className="container-form">
                <div>
                    <img src="./images/logoblanco.svg" width="100%" style={{ padding: 20 }} className="logo" />
                </div>
                <div></div>
                <div className="wrpLink">
                    <Link to="/" className="link">
                        {t("Pages.Register.cancel-button")}
                    </Link>
                </div>
                <div></div>
                <div className="form">
                    {!cargando && !registroPublico ? (
                        <Scroller direction="horizontal" currentItemIndex={indice}>
                            <form className="centered whited filas" onSubmit={(ev) => buscarBitwabi(ev)}>
                                <div style={{ fontSize: 24, opacity: 0.5, fontWeight: 400 }}>
                                    {t("Pages.Register.form.title")}
                                </div>
                                <div className="form-title" style={{ fontSize: 48, fontWeight: 700 }}>
                                    {t("Pages.Register.form.email.label")}
                                </div>
                                <input
                                    type="email"
                                    className="entradaDatos"
                                    required
                                    ref={inputs[0]}
                                    name="correo"
                                    value={form.correo}
                                    onChange={handleInputChange}
                                />
                                <button className="buttnFull" type="submit">
                                    {t("Pages.Register.form.next-button")}
                                </button>
                            </form>
                            <form className="centered whited filas" onSubmit={(ev) => nextControl(ev, 2)}>
                                <div style={{ fontSize: 24, opacity: 0.5, fontWeight: 400 }}>
                                    {t("Pages.Register.form.title")}
                                </div>
                                <div className="form-title" style={{ fontSize: 48, fontWeight: 700 }}>
                                    {t("Pages.Register.form.name.label")}
                                </div>
                                <input
                                    type="text"
                                    className="entradaDatos"
                                    ref={inputs[1]}
                                    required
                                    minLength="8"
                                    name="nombre"
                                    value={form.nombre}
                                    onChange={handleInputChange}
                                />
                                <button className="buttnFull" type="submit">
                                    {t("Pages.Register.form.next-button")}
                                </button>
                            </form>
                            <form className="centered whited filas" onSubmit={(ev) => nextControl(ev, 3)}>
                                <div style={{ fontSize: 24, opacity: 0.5, fontWeight: 400 }}>
                                    {t("Pages.Register.form.title")}
                                </div>
                                <div className="form-title" style={{ fontSize: 48, fontWeight: 700 }}>
                                    {t("Pages.Register.form.alias.label")}
                                </div>
                                <input
                                    type="text"
                                    className="entradaDatos"
                                    ref={inputs[2]}
                                    name="apodo"
                                    required
                                    minLength="3"
                                    value={form.apodo}
                                    onChange={handleInputChange}
                                />
                                <button className="buttnFull" type="submit">
                                    {t("Pages.Register.form.next-button")}
                                </button>
                            </form>
                            <form className="centered whited filas" onSubmit={(ev) => registrarUsuario(ev)}>
                                <div style={{ fontSize: 24, opacity: 0.5, fontWeight: 400 }}>
                                    {t("Pages.Register.form.title")}
                                </div>
                                <div className="form-title" style={{ fontSize: 48, fontWeight: 700 }}>
                                    {t("Pages.Register.form.password.label")}
                                </div>
                                <div style={{ width: "100%", position: "relative", zIndex: 2 }}>
                                    <input
                                        type={tipoPass ? "password" : "text"}
                                        className="entradaDatos"
                                        placeholder={t("Pages.Register.form.password.placeholder")}
                                        required
                                        minLength="8"
                                        ref={inputs[3]}
                                        name="contrasena"
                                        value={form.contrasena}
                                        onChange={handleInputChange}
                                    />
                                    <FaEye
                                        size={30}
                                        style={{
                                            position: "absolute",
                                            right: 0,
                                            bottom: 7,
                                            cursor: "pointer",
                                        }}
                                        onClick={(ev) => setTipoPass((tipoPass) => !tipoPass)}
                                    />
                                </div>
                                <button className="buttnFull" type="submit">
                                    {t("Pages.Register.form.next-button")}
                                </button>
                            </form>
                        </Scroller>
                    ) : !registroPublico ? (
                        <div className="centered filas">
                            <Loader type="BallTriangle" color="white" height={100} width={100} />
                        </div>
                    ) : (
                        <div className="centered whited filas" style={{ fontSize: 24, textAlign: "center" }}>
                            {error && (
                                <span>
                                    {t("Pages.Register.error.message")} <br />
                                    {error}
                                </span>
                            )}
                            {listo && (
                                <React.Fragment>
                                    <span>{t("Pages.Register.success.message")}</span>
                                    <br />
                                    <FaLaughWink size={50} />
                                    <Link className="buttnFull" style={{ textDecoration: "none" }} to="/acceder">
                                        {t("Pages.Register.success.panel-link")}
                                    </Link>
                                </React.Fragment>
                            )}
                            {!error && !listo && (
                                <span>
                                    {t("Pages.Register.closed.message")}{" "}
                                    <a href="mailto:info@adncrypto.com">info@adncrypto.com</a>
                                </span>
                            )}
                        </div>
                    )}
                </div>
                <div>
                    <img src="./images/flechas.svg" className="flechas" />
                </div>
                <div></div>
                <div className="social-container">
                    <div style={{ width: "33%" }}></div>
                    <div className="centered">
                        {!registroPublico && (
                            <React.Fragment>
                                <span className={regresaPunto(0)}></span>
                                <span className={regresaPunto(1)}></span>
                                <span className={regresaPunto(2)}></span>
                                <span className={regresaPunto(3)}></span>
                            </React.Fragment>
                        )}
                    </div>
                    <div className="social">
                        <a href="#">
                            <img
                                className="mx-3"
                                width="35"
                                height="35"
                                src="/images/svg/telegram-logo.svg"
                                alt="telegram-logo"
                            />
                        </a>
                        <a href="#">
                            <img
                                className="mx-3"
                                width="35"
                                height="35"
                                src="/images/svg/twitter-logo.svg"
                                alt="twitter-logo"
                            />
                        </a>
                        <a href="#">
                            <img
                                className="mx-3"
                                width="35"
                                height="35"
                                src="/images/svg/instagram-logo.svg"
                                alt="instagram-logo"
                            />
                        </a>
                    </div>
                </div>
                <div></div>
            </div>
        </div>
    )
}
