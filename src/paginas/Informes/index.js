import React, { useContext, useEffect, useState } from "react"
import { NavLink } from "react-router-dom"
import { UserContext } from "../../Context/UserContext"
import * as FirebaseMngr from "../../Utilities/FirebaseMngr"
import Panel from "../../Panel"
import { dateWithFormatFromTime } from "../../Utilities/DateUtilities.js"

export default function Facturas() {
    const { user } = useContext(UserContext)
    const firebaseDatabase = FirebaseMngr.get()
    const [reports, setReports] = useState(undefined)

    useEffect(() => {
        firebaseDatabase
            .ref("reports")
            .on("value", (snap) => {
                let returnedReports = snap.val()
                setReports(returnedReports)
            })
    }, [user])

    return (
        <Panel title="Informes">
            <div className="content-body">
                <div className="container-fluid">
                    <React.Fragment>
                        {reports === undefined ? (
                            <div></div>
                        ) : (
                            <div className="row">
                                {reports === null ? (
                                    <div className="col-xl-12">
                                        <div className="alert alert-warning left-icon-big alert-dismissible fade show">
                                            <div className="media">
                                                <div className="alert-left-icon-big">
                                                    <span>
                                                        <i className="mdi mdi-help-circle-outline"></i>
                                                    </span>
                                                </div>
                                                <div className="media-body">
                                                    <h5 className="mt-1 mb-2">Vacío</h5>
                                                    <p className="mb-0">No hay reportes por aqui</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                ) : (
                                    <div className="col-xl-12">
                                        <NavLink to={`/newReportAFfeaewfAWEGawefgWAEFwefaawefWAFawefAWEFawefawefwaefawef`} className="btn btn-primary btn-xs btn-rounded">Nuevo reporte</NavLink>
                                        <div className="table-responsive table-hover fs-14">
                                            <table className="table display mb-4 dataTablesCard  card-table" id="example5">
                                                <thead>
                                                    <tr>
                                                        <th>id</th>
                                                        <th>Fecha de inicio</th>
                                                        <th>Fecha de cierre</th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {Object.keys(reports).map((index, i) => {
                                                        let report = reports[index]
                                                        return (
                                                            <tr key={i}>
                                                                <td className="pr-0">
                                                                    {index}
                                                                </td>
                                                                <td className="wspace-no">
                                                                    {report.startDate}
                                                                </td>
                                                                <td>
                                                                    {report.endDate}
                                                                </td>
                                                                <td>
                                                                    <NavLink to={`/report/${index}`} className="btn btn-info btn-xs btn-rounded">Ver detalles</NavLink>
                                                                </td>
                                                                <td>
                                                                    <NavLink to={`/newReportAFfeaewfAWEGawefgWAEFwefaawefWAFawefAWEFawefawefwaefawef/${index}`} className="btn btn-success btn-xs btn-rounded">Modificar</NavLink>
                                                                </td>
                                                            </tr>
                                                        )
                                                    })}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                )}
                            </div>
                        )}
                    </React.Fragment>
                </div>
            </div>
        </Panel>
    )
}
