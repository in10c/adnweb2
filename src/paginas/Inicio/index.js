import React from "react"
import axios from "axios"
import parse from "html-react-parser"
import { Link } from "react-router-dom"
import { useTranslation } from "react-i18next"
import Constants from "../../Utilities/Constants"
import { UserContext } from "../../Context/UserContext"
import CarrouselCrypto from "../../componentes/CarrouselCrypto"
import LanguageSelector from "../../componentes/LanguageSelector"

export default function Inicio() {
    const [noticias, setNoticias] = React.useState([])
    const { t } = useTranslation()
    const [show, setShow] = React.useState(false)
    const [collapsed, setCollapsed] = React.useState(true)
    const [collapsing, setCollapsing] = React.useState(false)

    React.useEffect(async () => {
        let response = await axios.get(Constants.servidorFunciones() + "/cryptoNoticias")
        console.log("los response", response)
        setNoticias(response.data.data)
    }, [])

    return (
        <React.Fragment>
            <div id="header-container">
                <div className="container">
                    <div className="row">
                        <nav
                            className="navbar navbar-expand-lg navbar-dark"
                            style={{ backgroundColor: "#FFFFFF00", width: "100%" }}
                        >
                            <a className="navbar-brand" href="#">
                                <img
                                    src="/images/svg/logo.svg"
                                    id="header-logo"
                                    width="254"
                                    height="48"
                                    alt="header-logo"
                                />
                            </a>
                            <button
                                className={`navbar-toggler`}
                                type="button"
                                data-toggle="collapse"
                                data-target="#navbarNav"
                                aria-controls="navbarNav"
                                aria-expanded="false"
                                aria-label="Toggle navigation"
                            >
                                <span className="navbar-toggler-icon"></span>
                            </button>
                            <div className={`collapse navbar-collapse justify-content-end`} id="navbarNav">
                                <ul className="navbar-nav">
                                    <li className="nav-item">
                                        <a className="nav-link" href="#about-container">
                                            {t("Pages.Index.navbar-nav.about")}
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="#routesheet-section">
                                            {t("Pages.Index.navbar-nav.routesheet")}
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="#news-section">
                                            {t("Pages.Index.navbar-nav.news")}
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <Link className="nav-link" to="/registrar">
                                            {t("Pages.Index.navbar-nav.register")}
                                        </Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link className="nav-link" to="/acceder">
                                            {t("Pages.Index.navbar-nav.login")}
                                        </Link>
                                    </li>
                                </ul>
                            </div>
                            <LanguageSelector />
                        </nav>
                    </div>
                    <div className="row align-items-center">
                        <div className="col-lg-4 col-md-6 col-sm-8">
                            <h2 id="main-title">{t("Pages.Index.main.title")}</h2>
                            <ul>
                                <li>{t("Pages.Index.main.description.signals")}</li>
                                <li>{t("Pages.Index.main.description.forex")}</li>
                                <li>{t("Pages.Index.main.description.academy")}</li>
                                <li>{t("Pages.Index.main.description.crypto")}</li>
                            </ul>
                        </div>
                        <div className="col-lg-8 col-md-6 col-sm-4 headerCoins text-right">
                            <img
                                src="./images/KV-Ethereum-Silver.svg"
                                className="img-fluid coin"
                                alt="criptocurrencies"
                            />
                            <img
                                src="./images/KV-Bitcoin-Gold.svg"
                                className="img-fluid delayCoin"
                                alt="criptocurrencies"
                            />
                            <img
                                src="./images/KV-Litecoin-Silver.svg"
                                className="img-fluid coin"
                                alt="criptocurrencies"
                            />
                            <img src="./images/KV-Shadows-Coins.svg" className="img-fluid" alt="criptocurrencies" />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-1 pr-0">
                            <img id="play-logo" width="100%" src="/images/svg/play-logo.svg" alt="play-logo" />
                        </div>
                        <div className="col-7">
                            <h2>{t("Pages.Index.market-apertures.title")}</h2>
                            <p>{t("Pages.Index.market-apertures.description")}</p>
                        </div>
                        <div className="col-4 d-flex justify-content-center align-items-center">
                            <Link to="/registrar">
                                <button id="button-header">{t("Pages.Index.market-apertures.join-now-button")}</button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
            <CarrouselCrypto />
            <div id="about-container">
                <div className="container">
                    <div className="row d-flex justify-content-center pt-5">
                        <img width="254" height="53" src="/images/svg/gray-logo.svg" alt="gray-logo" />
                    </div>
                    <div className="row text-center py-5">{t("Pages.Index.about.description")}</div>
                    <div className="row d-flex justify-content-center align-items-center">
                        <div className="col-md-4 mx-2 card-about-container">
                            <div className="card-about text-center">
                                <div className="card-about-header">
                                    <img width="103" height="98" src="./images/servicio_fundamentales.svg" />
                                </div>
                                <div className="card-about-body">
                                    <h3>
                                        <b>{t("Pages.Index.about.fundamentals.title")}</b>
                                    </h3>
                                    <p>{t("Pages.Index.about.fundamentals.description")}</p>
                                </div>
                                <div className="card-about-footer">
                                    <Link to="/panel">
                                        <button className="card-about-button">
                                            {t("Pages.Index.about.start-button")}
                                        </button>
                                    </Link>
                                </div>
                                <div className="card-about-shadow">
                                    <img src="./images/shadow.png" />
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4 mx-2 card-about-container">
                            <div className="card-about text-center">
                                <div className="card-about-header">
                                    <img width="103" height="98" src="./images/servicio_informes.svg" />
                                </div>
                                <div className="card-about-body primary">
                                    <h3>
                                        <b>{t("Pages.Index.about.signals.title")}</b>
                                    </h3>
                                    <p>{t("Pages.Index.about.signals.description")}</p>
                                </div>
                                <div className="card-about-footer">
                                    <Link to="/panel">
                                        <button className="card-about-button">
                                            {t("Pages.Index.about.start-button")}
                                        </button>
                                    </Link>
                                </div>
                                <div className="card-about-shadow">
                                    <img src="./images/shadow.png" />
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4 mx-2 card-about-container">
                            <div className="card-about text-center">
                                <div className="card-about-header">
                                    <img width="79" height="98" src="./images/servicio_senales.svg" />
                                </div>
                                <div className="card-about-body">
                                    <h3>
                                        <b>{t("Pages.Index.about.reports.title")}</b>
                                    </h3>
                                    <p>{t("Pages.Index.about.reports.description")}</p>
                                </div>
                                <div className="card-about-footer">
                                    <Link to="/panel">
                                        <button className="card-about-button">
                                            {t("Pages.Index.about.start-button")}
                                        </button>
                                    </Link>
                                </div>
                                <div className="card-about-shadow">
                                    <img src="./images/shadow.png" />
                                </div>
                            </div>
                        </div>
                    </div>
                    {/*<div className="row">
                <div className="col-md-4">
                <div className="number-stadistics text-center">
                    <b>+3,000</b>
                </div>
                <div className="text-stadistics text-center">
                    Señales
                </div>
                </div>
                <div className="col-md-4">
                <div className="number-stadistics text-center">
                    <b>+3,000</b>
                </div>
                <div className="text-stadistics text-center">
                    Efectividad
                </div>
                </div>
                <div className="col-md-4">
                <div className="number-stadistics text-center">
                    <b>+3,000</b>
                </div>
                <div className="text-stadistics text-center">
                    Ganancias
                </div>
                </div>
                </div>*/}
                </div>
            </div>

            <div id="services-section">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-6">
                            <div className="number-stadistics">{t("Pages.Index.services.number-stadistics.title")}</div>
                            <div>
                                <h3>
                                    <b>{t("Pages.Index.services.number-stadistics.subtitle")}</b>
                                </h3>
                            </div>
                            <div>
                                <p>{t("Pages.Index.services.number-stadistics.description")}</p>
                            </div>
                            <div>
                                <Link to="/panel">
                                    <button className="services-button">
                                        {t("Pages.Index.services.number-stadistics.more-button")}
                                    </button>
                                </Link>
                            </div>
                        </div>
                        <div className="col-lg-6 d-flex justify-content-end align-items-end bitcoin-blue-container">
                            <img src="./images/infome-graf.svg" className="graph" width="300" alt="bitcoin-blue" />
                            <img
                                src="./images/infome-luz.svg"
                                className="luz luzAnimada"
                                width="300"
                                alt="bitcoin-blue"
                            />
                            <img
                                src="./images/infome-token.svg"
                                width="270"
                                alt="bitcoin-blue"
                                style={{ marginBottom: -50 }}
                            />
                        </div>
                    </div>
                </div>
            </div>

            <div id="routesheet-section">
                <div className="container">
                    <div className="row">
                        <div className="col-12 text-center mt-4 mb-5">
                            <h2 className="number-stadistics text-white" style={{ marginBottom: 160 }}>
                                {t("Pages.Index.routesheet.title")}
                            </h2>
                        </div>
                    </div>
                    <div className="row">
                        {[...Array(4)].map((e, i) => (
                            <div className="col-md-3 d-flex justify-content-center route-card-container" key={i}>
                                <div className="route-card text-center">
                                    <div className="route-card-code">
                                        <b>{t(`Pages.Index.routesheet.routes.Q${i + 1}.title`)}</b>
                                    </div>
                                    <div className="route-card-title">
                                        {t(`Pages.Index.routesheet.routes.Q${i + 1}.subtitle`)}
                                    </div>
                                    <div className="route-card-body">
                                        {t(`Pages.Index.routesheet.routes.Q${i + 1}.description`)}
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </div>

            <div id="news-section">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <h2>{t("Pages.Index.news.title")}</h2>
                        </div>
                    </div>
                    <div className="row">
                        {noticias.map((element) => {
                            //console.log("element", element.description)
                            var parsedHtml = parse(element.description)
                            console.log("parseff", parsedHtml)
                            return (
                                <div className="col-lg-4 d-flex flex-column align-items-center">
                                    <div className="news-card text-center">
                                        <div className="news-card-image">
                                            <img
                                                src={parsedHtml[0].props.children.props.src}
                                                width="100%"
                                                style={{ maxHeight: 150 }}
                                                alt=""
                                            />
                                        </div>
                                        <div className="news-card-title">{element.title}</div>
                                        <div className="news-card-description">{parsedHtml[1].props.children}</div>
                                    </div>
                                    <div className="news-button-container">
                                        <button className="news-button">
                                            <a href={element.link} target="_blank">
                                                {t("Pages.Index.news.more-button")}
                                            </a>
                                        </button>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>

            <div id="footer-section">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="mb-1">
                                <img width="159" height="30" src="/images/svg/logo.svg" alt="footer-logo" />
                            </div>
                            <div className="copyright-text">{t("Panel.Footer.copyright-text")}</div>
                        </div>
                        <div className="col-md-6 text-right social-media">
                            <a href="#">
                                <img
                                    className="mx-3"
                                    width="40"
                                    height="45"
                                    src="/images/svg/telegram-logo.svg"
                                    alt="telegram-logo"
                                />
                            </a>
                            <a href="#">
                                <img
                                    className="mx-3"
                                    width="50"
                                    height="45"
                                    src="/images/svg/twitter-logo.svg"
                                    alt="twitter-logo"
                                />
                            </a>
                            <a href="#">
                                <img
                                    className="mx-3"
                                    width="40"
                                    height="45"
                                    src="/images/svg/instagram-logo.svg"
                                    alt="instagram-logo"
                                />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}
