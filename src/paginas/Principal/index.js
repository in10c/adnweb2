import React, { useContext } from "react"
import Panel from "../../Panel"
import { Link } from "react-router-dom"
import { useTranslation } from "react-i18next"
import { UserContext } from "../../Context/UserContext"
import "./styles.css"

export default function Principal() {
    const { t } = useTranslation()
    const { user } = useContext(UserContext)

    return (
        <Panel>
            <div className="container">
                <div className="row mb-5">
                    <div className="col-12 text-center">
                        <h2 className="section-title">{t("Pages.Main.channels.title")}</h2>
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-lg-4 col-md-12 d-flex flex-column align-items-center channel">
                        <div className="channel-icon">
                            <img src={`/images/package-icons/Icon-ADN-News.svg`} width="100%" style={{ opacity: 1 }} />
                        </div>
                        <h3 className="text-light text-center">
                            <strong>{t("Pages.Main.channels.news.title")}</strong>
                        </h3>
                        <h4 className="channel-type text-center">{t("Pages.Main.channels.news.subtitle")}</h4>
                        <p className="channel-description">{t("Pages.Main.channels.news.description")}</p>
                        <a href="https://t.me/ADNCryptos" target="_blank">
                            <button className="card-about-button mt-1">{t("Pages.Main.telegram-button")}</button>
                        </a>
                    </div>

                    <div className="col-lg-4 col-md-12 d-flex flex-column align-items-center channel">
                        <div className="channel-icon">
                            <img
                                src={`/images/package-icons/Icon-Beyond-Cryptobot.svg`}
                                width="100%"
                                style={{ opacity: 1 }}
                            />
                        </div>
                        <h3 className="text-light text-center">
                            <strong>{t("Pages.Main.channels.forex.title")}</strong>
                        </h3>
                        <h4 className="channel-type text-center">{t("Pages.Main.channels.forex.subtitle")}</h4>
                        <p className="channel-description">{t("Pages.Main.channels.forex.description")}</p>
                        <a href="https://telegram.me/soporteadncrypto" target="_blank">
                            <button className="card-about-button mt-1">{t("Pages.Main.telegram-button")}</button>
                        </a>
                    </div>
                    <div className="col-lg-4 col-md-12 d-flex flex-column align-items-center channel">
                        <div className="channel-icon">
                            <img
                                src={`/images/package-icons/Icon-Osirius-Cryptobot${
                                    user.deBitwabi || (!user.deBitwabi && user.licenciaADN > 0) ? "" : "-Disable"
                                }.svg`}
                                width="100%"
                                style={{
                                    opacity: user.deBitwabi || (!user.deBitwabi && user.licenciaADN > 0) ? 1 : 0.5,
                                }}
                            />
                        </div>
                        <h3 className="text-light text-center">
                            <strong>{t("Pages.Main.channels.signals.title")}</strong>
                        </h3>
                        <h4 className="channel-type text-center">{t("Pages.Main.channels.signals.subtitle")}</h4>
                        <p className="channel-description">{t("Pages.Main.channels.signals.description")}</p>
                        {user.deBitwabi || (!user.deBitwabi && user.licenciaADN > 0) ? (
                            <a href="https://telegram.me/soporteadncrypto" target="_blank">
                                <button className="card-about-button mt-1">{t("Pages.Main.telegram-button")}</button>
                            </a>
                        ) : (
                            <Link to="/paquetes">
                                <button
                                    className="card-about-button mt-1"
                                    style={{ backgroundColor: "white", color: "#744ca9" }}
                                >
                                    {t("Pages.Main.uopgrade-button")}
                                </button>
                            </Link>
                        )}
                    </div>
                </div>
            </div>
            <div id="coming-soon-section" className="container">
                <div className="row">
                    <div className="col-lg-6 col-md-12 mb-5">
                        <h2 id="coming-soon-next">{t("Pages.Main.trading.title")}</h2>
                        <h2 id="coming-soon-title">{t("Pages.Main.trading.subtitle")}</h2>
                        <p className="coming-soon-description">{t("Pages.Main.trading.description")}</p>
                        <div className="list">
                            {[...Array(5)].map((e, i) => (
                                <p className="coming-soon-description">
                                    {t(`Pages.Main.trading.descriptions-list.${i}`)}
                                </p>
                            ))}
                        </div>
                        {(user.deBitwabi && parseInt(user.paquete) > 2) || (!user.deBitwabi && user.licenciaADN > 1) ? (
                            <Link to="/bitcoinBot">
                                <button className="coming-soon-button">{t("Pages.Main.talk-button")}</button>
                            </Link>
                        ) : (
                            <Link to="/paquetes">
                                <button
                                    className="coming-soon-button"
                                    style={{ backgroundColor: "#5a5a5a", color: "white" }}
                                >
                                    {t("Pages.Main.uopgrade-button")}
                                </button>
                            </Link>
                        )}
                    </div>
                    <div className="col-lg-6 col-md-12">
                        <img
                            src={`/images/package-icons/Img-Copy-Tranding${
                                (user.deBitwabi && parseInt(user.paquete) > 2) ||
                                (!user.deBitwabi && user.licenciaADN > 1)
                                    ? ""
                                    : "-Disable"
                            }.svg`}
                            id="coming-soon-image"
                            className="img-fluid"
                            style={{
                                opacity:
                                    (user.deBitwabi && parseInt(user.paquete) > 2) ||
                                    (!user.deBitwabi && user.licenciaADN > 1)
                                        ? 1
                                        : 0.5,
                            }}
                        />
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row mb-5">
                    <div className="col-12 text-center">
                        <h2 className="section-title">{t("Pages.Main.more-information.title")}</h2>
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-lg-6 col-md-12 d-flex flex-column align-items-center info">
                        <div className="info-icon">
                            <img
                                src={`/images/package-icons/Img-Apertura-Mercado${
                                    user.paquete < 3 ? "-Disable" : ""
                                }.svg`}
                                style={{ opacity: user.paquete < 3 ? 0.5 : 1 }}
                            />
                        </div>
                        <h3 className="info-title mt-3 text-center">
                            <strong>{t("Pages.Main.more-information.info.market.title")}</strong>
                        </h3>
                        <p className="info-description">{t("Pages.Main.more-information.info.market.description")}</p>
                        <button
                            className="card-about-button mt-1"
                            style={{ backgroundColor: "#7d7d7d", color: "white" }}
                        >
                            {t("Pages.Main.coming-soon-button")}
                        </button>
                    </div>
                    <div className="col-lg-6 col-md-12 d-flex flex-column align-items-center info">
                        <div className="info-icon">
                            <img
                                src={`/images/package-icons/Img-Informes-Monedas${
                                    user.paquete < 3 ? "-Disable" : ""
                                }.svg`}
                                style={{ opacity: user.paquete < 3 ? 0.5 : 1 }}
                            />
                        </div>
                        <h3 className="info-title mt-3 text-center">
                            <strong>{t("Pages.Main.more-information.info.coin-info.title")}</strong>
                        </h3>
                        <p className="info-description">
                            {t("Pages.Main.more-information.info.coin-info.description")}
                        </p>
                        <button
                            className="card-about-button mt-1"
                            style={{ backgroundColor: "#7d7d7d", color: "white" }}
                        >
                            {t("Pages.Main.coming-soon-button")}
                        </button>
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row">
                    <div className="col-12 arrow-background p-0">
                        <img src="/images/arrow-background.png" className="arrow-image" />
                        <div className="potency-bnefits">
                            <h2 id="potency-bnefits-next">{t("Pages.Main.potency-benefits.title")}</h2>
                            <h2 id="potency-bnefits-title">
                                <span className="benefits">{t("Pages.Main.potency-benefits.benefits")}</span>
                                {t("Pages.Main.potency-benefits.subtitle")}
                            </h2>
                            <p className="potency-bnefits-description">
                                {t("Pages.Main.potency-benefits.description")}
                            </p>
                            <Link to="/controlPanel/referidos">
                                <button className="potency-bnefits-button">{t("Pages.Main.start-now-button")}</button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </Panel>
    )
}
