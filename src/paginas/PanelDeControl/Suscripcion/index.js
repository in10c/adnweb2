import React, { useContext, useEffect, useState } from "react"
import { FaInbox, FaTrophy } from 'react-icons/fa'
import * as FirebaseMngr from "../../../Utilities/FirebaseMngr"
import { UserContext } from "../../../Context/UserContext"
import "./index.css"
import { Link } from "react-router-dom"

export default function Suscripcion() {
    const { user } = useContext(UserContext)
    const [productos, setProd] = useState(undefined)
    const [selected, setSelected] = useState(0)
    const firebaseDatabase = FirebaseMngr.get()

    useEffect( () => {
        firebaseDatabase.ref("productos/").on("value", snapshot=>{
            let auxProd = snapshot.val()
            setProd(auxProd)
        })
    }, [])

    const getComponent = (accion) =>{
        switch (accion.componente) {
            case "a":
                return <a href={accion.enlace} target="_blank">{accion.label}</a>
            case "interno":
                return <Link to={accion.enlace}>{accion.label}</Link>
                break;
            default:
                return null
        }
    }

    return (
    productos ? <div className="row main-section" style={{minHeight: 500, paddingBottom: 0}}>
        <div className="col-md-9 grilla">
            <div className="row">
                {productos && productos.map((prod, idx)=>{
                console.log("contra", selected, idx)
                return <div 
                    onClick={ev=>setSelected(idx)}
                    className={"col-md-4 prodCol"+( selected === idx ? " prodSelected" : "")}>
                    <img src={prod.imagen} />
                </div>})}
            </div>
        </div>
        <div className="col-md-3 bg-white panelProds">
            <h6>Detalle del servicio</h6>
            <hr/>
            <h4>{productos[selected].titulo}</h4>
            <h6 className="subtt">{productos[selected].subtitulo}</h6>
            <p>
                {productos[selected].descripcion}
            </p>
            {productos[selected].actions && <ul className="listaAcciones">
                {productos[selected].actions.map(actn=>
                    <li>{getComponent(actn)}</li>)}
            </ul>}
        </div>
    </div> : null)
}
