import React, { useState, useContext, useEffect } from "react"
import { Link, NavLink } from "react-router-dom"
import { useTranslation } from "react-i18next"
import * as FirebaseMngr from "../../../Utilities/FirebaseMngr"
import { UserContext } from "../../../Context/UserContext"
import LanguageSelector from "../../../componentes/LanguageSelector"
import "./index.css"

export default function Header({ section }) {
    const { t } = useTranslation()
    const { user, signOutUser } = useContext(UserContext)
    const [menuToggled, setMenuToggled] = useState(false)
    const [packages, setPackages] = useState({})
    const firebaseDatabase = FirebaseMngr.get()
    useEffect(async () => {
        const snapshot = await firebaseDatabase.ref("packages").once("value")
        const response = snapshot.val()
        if (response) {
            console.log("en pacjages", response, user)
            setPackages(response)
        }
    }, [])
    return (
        <div id="control-panel-header-container">
            <div className="container">
                <div className="row mb-5">
                    <nav
                        className="navbar navbar-expand-lg navbar-dark d-flex"
                        style={{ backgroundColor: "#FFFFFF00", width: "100%", alignItems: "center" }}
                    >
                        <Link className="navbar-brand" to="/panel">
                            <img id="header-logo" width="254" height="48" src="/images/svg/logo.svg" alt="Logo" />
                        </Link>
                        <div
                            id="support-button"
                            className="toggle"
                            onClick={() => {
                                setMenuToggled(!menuToggled)
                            }}
                            style={{ marginLeft: "auto" }}
                        >
                            <span className="dropdown-toggle">
                                {t("Panel.Header.toggle-button")} {user.nombre}
                            </span>
                            <div id="toggle-menu" className={`${menuToggled ? "visible" : ""}`}>
                                <NavLink to="/panel" className="toggle-item">
                                    {t("Panel.Header.toggle-menu.home")}
                                </NavLink>
                                <NavLink to="/controlPanel/facturas" className="toggle-item">
                                    {t("Panel.Header.toggle-menu.account")}
                                </NavLink>
                                <NavLink to="/paquetes" className="toggle-item">
                                    {t("Panel.Header.toggle-menu.upgrade")}
                                </NavLink>
                                <a
                                    className="toggle-item"
                                    onClick={() => {
                                        signOutUser()
                                    }}
                                >
                                    {t("Panel.Header.toggle-menu.logout")}
                                </a>
                            </div>
                        </div>
                        <LanguageSelector />
                    </nav>
                </div>
                <div className="row mt-5 pt-5">
                    <div className="col-sm-12 col-md-9 d-flex justify-content-between">
                        {/* <Link id="informUsersLink" className={`controlPanelLink ${section === "suscripcion" ? "selected" : ""}`} to="/controlPanel/suscripcion">
                            <img src="/images/menu/sus.svg" />
                        </Link> */}
                        <Link
                            id="informUsersLink"
                            className={`controlPanelLink ${section === "informes" ? "selected" : ""}`}
                            to="/controlPanel/informes"
                        >
                            <img src="/images/menu/informes.svg" />
                        </Link>
                        <Link
                            id="invoicesLink"
                            className={`controlPanelLink ${section === "facturas" ? "selected" : ""}`}
                            to="/controlPanel/facturas"
                        >
                            <img src="/images/menu/factu.svg" />
                        </Link>
                        <Link
                            id="referUsersLink"
                            className={`controlPanelLink ${section === "referidos" ? "selected" : ""}`}
                            to="/controlPanel/referidos"
                        >
                            <img src="/images/menu/ref.svg" />
                        </Link>
                        <Link
                            id="paymentsLink"
                            className={`controlPanelLink ${section === "pagos" ? "selected" : ""}`}
                            to="/controlPanel/pagos"
                        >
                            <img src="/images/menu/pagos.svg" />
                        </Link>
                    </div>
                    <div className="col-sm-12 col-md-3 d-flex justify-content-end">
                        <div className="d-flex flex-column align-items-center">
                            {/*<div id="daysLeftText">{packages[user.licenciaADN].name}</div>*/}
                            <div id="packageText">
                                {t("Pages.Control-panel.account-info.plan")}{" "}
                                {(user.licenciaADN || user.licenciaADN === 0) && packages && packages[user.licenciaADN]
                                    ? packages[user.licenciaADN].name
                                    : "N/A"}
                            </div>
                            <Link to="/paquetes" id="upgradeButton">
                                {t("Pages.Control-panel.account-info.upgrade-button")}
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
