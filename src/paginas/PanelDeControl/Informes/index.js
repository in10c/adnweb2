import React, { useContext, useEffect, useState } from "react"
import { NavLink, Link } from "react-router-dom"
import * as FirebaseMngr from "../../../Utilities/FirebaseMngr"
import { IoGridOutline, FaTrophy, FaBitcoin } from "react-icons/io5"
import { useTranslation } from "react-i18next"
import { UserContext } from "../../../Context/UserContext"
import Panel from "../../../Panel"
import { dateWithFormatFromTime } from "../../../Utilities/DateUtilities.js"
import Constants from "../../../Utilities/Constants"
import "./index.css"

export default function Informes() {
    const { t } = useTranslation()
    const { user } = useContext(UserContext)
    const firebaseDatabase = FirebaseMngr.get()
    const [reports, setReports] = useState(undefined)

    useEffect(() => {
        firebaseDatabase.ref("reports").on("value", (snap) => {
            let returnedInvoices = snap.val()
            setReports(returnedInvoices)
        })
    }, [user])

    return (
        <div className="container main-section" style={{ paddingTop: 50 }}>
            <div className="row px-4" style={{ marginBottom: 30 }}>
                <div className="col-md-6 col-sm-12 d-flex flex-column">
                    <span className="results-title">{t("Pages.Control-panel.results.title")}</span>
                    <span>
                        {reports && Object.keys(reports).length > 0 ? Object.keys(reports).length : 0}{" "}
                        {t("Pages.Control-panel.Reports.published-reports.label")}
                    </span>
                </div>
                <div className="col-md-6 col-sm-12 text-right">
                    <IoGridOutline size="30" color="#9B5BC1" />
                </div>
            </div>
            <div className="row">
                {reports &&
                    Object.keys(reports).map((index) => {
                        const reporte = reports[index]
                        return (
                            <div class="col-4 informeUnico">
                                <div className="informeCard">
                                    <div
                                        className="asset"
                                        style={{ backgroundImage: 'url("/images/Informe-ADN-Cover-05.jpg")' }}
                                    >
                                        <p>
                                            {reporte.symbol}
                                            <span>{reporte.returnBenefits}</span>
                                        </p>
                                    </div>
                                    <div className="detalles">
                                        <h2>{reporte.title}</h2>
                                        <p>{reporte.startDate + " / " + reporte.endDate}</p>
                                        <Link to={"/report/" + index}>
                                            <button>
                                                {t("Pages.Control-panel.Reports.published-reports.more-button")}
                                            </button>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        )
                    })}
            </div>
        </div>
    )
}
