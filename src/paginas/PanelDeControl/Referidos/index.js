import React, { useContext, useEffect, useState } from "react"
import * as FirebaseMngr from "../../../Utilities/FirebaseMngr"
import { UserContext } from "../../../Context/UserContext"
import { FaInbox, FaShareAlt } from "react-icons/fa"
import { useToasts } from "react-toast-notifications"
import { useTranslation } from "react-i18next"
import "./index.css"

export default function Tree() {
    const { t } = useTranslation()
    const { user } = useContext(UserContext)
    const { addToast } = useToasts()
    const [tree, setTree] = useState(undefined)
    const firebaseDatabase = FirebaseMngr.get()
    const [path, setPath] = useState(undefined)

    const [packages, setPackages] = useState({})

    const [showFilters, setShowFilters] = useState(false)

    useEffect(() => {
        const getInfo = async () => {
            if (user.userID) {
                const snapshot = await firebaseDatabase.ref("path/" + user.userID).once("value")
                const pathArray = snapshot.val()
                let pathFormat = ""
                if (pathArray) {
                    for (let index = 0; index < pathArray.length; index++) {
                        const referUser = pathArray[index]
                        pathFormat += referUser + "/children/"
                    }
                }
                setPath(pathFormat)
            }
        }
        getInfo()
    }, [user])

    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])

    useEffect(() => {
        if (path != undefined && user.userID) {
            firebaseDatabase.ref("tree/" + path + user.userID).on("value", (snap) => {
                let returnedTree = snap.val()
                setTree(returnedTree)
            })
        }
    }, [path])

    useEffect(() => {
        const getInfo = async () => {
            const snapshot = await firebaseDatabase.ref("packages").once("value")
            const response = snapshot.val()
            if (response) {
                setPackages(response)
            }
        }
        getInfo()
    }, [])

    const copy = (ev) => {
        ev.preventDefault()
        let url = `${window.location.origin}/registrarReferido/${user.userID}`
        navigator.clipboard.writeText(url).then(function () {
            addToast(
                t("Pages.Control-panel.Referrals.referral.copied.first") +
                    " '" +
                    url +
                    "' " +
                    t("Pages.Control-panel.Referrals.referral.copied.first"),
                {
                    appearance: "info",
                    autoDismiss: true,
                }
            )
        })
    }

    return (
        <React.Fragment>
            {tree === undefined ? (
                <div></div>
            ) : (
                <React.Fragment>
                    {tree === null || tree.children === undefined ? (
                        <div className="container main-section" style={{ paddinTtop: 50 }}>
                            <div className="row">
                                <div className="col-xl-12 mensajeSolo">
                                    <FaInbox size={150} color="#e2e2e2" />
                                    <h3>{t("Pages.Control-panel.Referrals.empty.message")}</h3>
                                    <p>{t("Pages.Control-panel.Referrals.empty.advice")}</p>
                                    <button onClick={copy}>
                                        <FaShareAlt style={{ marginRight: 10 }} />{" "}
                                        {t("Pages.Control-panel.Referrals.referral.link")}
                                    </button>
                                </div>
                            </div>
                        </div>
                    ) : (
                        <React.Fragment>
                            {/*<div className="container filter-border"></div>
                            <div className={`container filter-section ${!showFilters ? "hide" : ""}`}>
                                <div className="row">
                                    <div className="col-4">
                                        <select id="filterOption">
                                            <option value="">A</option>
                                            <option value="">B</option>
                                            <option value="">C</option>
                                        </select>
                                    </div>
                                    <div className="col-8">
                                        <input type="text" id="filterText" placeholder="Buscar por..." />
                                    </div>
                                </div>
                            </div>
                            <div className="container filter-border shadow-box"></div>*/}
                            <div className="container main-section" style={{ paddingTop: 50 }}>
                                {/*<div className="row d-flex justify-content-center">
                                    <div className="filterButton" onClick={() => setShowFilters(!showFilters)}>
                                        {showFilters ? <span>Ocultar filtros</span> : <span>Mostrar filtros</span>}
                                        {!showFilters ? (
                                            <svg
                                                width="10"
                                                height="5"
                                                viewBox="0 0 10 5"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path d="M0 0L5 5L10 0H0Z" fill="#9B5BC1" />
                                            </svg>
                                        ) : (
                                            <svg
                                                width="10"
                                                height="5"
                                                viewBox="0 0 10 5"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path d="M10 5L5 -4.37114e-07L0 5L10 5Z" fill="#9B5BC1" />
                                            </svg>
                                        )}
                                    </div>
                                </div>*/}
                                <div className="row px-4" style={{ marginBottom: 30 }}>
                                    <div className="col-md-6 col-sm-12 d-flex flex-column">
                                        <span className="results-title">{t("Pages.Control-panel.results.title")}</span>
                                        <span>
                                            {Object.keys(tree.children).length}{" "}
                                            {t("Pages.Control-panel.Referrals.direct-referrals.label")}
                                        </span>
                                    </div>
                                    <div className="col-md-6 col-sm-12 text-right">
                                        <button className="exportRegisters" onClick={copy}>
                                            <FaShareAlt style={{ marginRight: 10 }} />{" "}
                                            {t("Pages.Control-panel.Referrals.referral.link")}
                                        </button>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-12">
                                        {Object.keys(tree.children).map((index, i) => {
                                            const child = tree.children[index]
                                            return (
                                                <React.Fragment>
                                                    <div className="referUser" key={i}>
                                                        <div className="referUserDot">
                                                            <img
                                                                width="12"
                                                                height="12"
                                                                src="/images/svg/gray-point.svg"
                                                            />
                                                        </div>
                                                        <div className="referUserName">
                                                            <strong>{child.name}</strong>
                                                        </div>
                                                        {/* <div className="referUserEmail">
                                                            <a href={`mailto:${child.email}`}>{child.email}</a>
                                                        </div> */}
                                                        <div className="referUserReferCount">
                                                            {t(
                                                                "Pages.Control-panel.Referrals.direct-referrals.referrals"
                                                            )}
                                                            :{" "}
                                                            <strong>
                                                                {child.children
                                                                    ? Object.keys(child.children).length
                                                                    : 0}
                                                            </strong>
                                                        </div>
                                                        <div className="referUserLevel">
                                                            {t("Pages.Control-panel.Referrals.direct-referrals.level")}:{" "}
                                                            <strong>1</strong>
                                                        </div>
                                                        <div className="referUserPackage">
                                                            {/* Paquete:{" "}
                                                        <strong> */}
                                                            {packages[child.packageID]
                                                                ? packages[child.packageID].name
                                                                : ""}
                                                            {/* </strong> */}
                                                        </div>
                                                    </div>
                                                </React.Fragment>
                                            )
                                        })}
                                    </div>
                                </div>
                            </div>
                        </React.Fragment>
                    )}
                </React.Fragment>
            )}
        </React.Fragment>
    )
}
