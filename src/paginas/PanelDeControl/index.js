import React, { useState } from "react"
import { useTranslation } from "react-i18next"
import Panel from "../../Panel"
import Header from "./Header"
import Facturas from "./Facturas"
import Referidos from "./Referidos"
import Pagos from "./Pagos"
import Informes from "./Informes"
import Suscripcion from "./Suscripcion"
import { BrowserRouter as Router, Switch, Route, Link, useParams } from "react-router-dom"
import "./index.css"

export default function PanelDeControl() {
    const { t } = useTranslation()
    const { pestana } = useParams()

    return (
        <Panel hideHeader={true}>
            <Header section={pestana} />
            {pestana === "referidos" ? (
                <Referidos />
            ) : pestana === "facturas" ? (
                <Facturas />
            ) : pestana === "pagos" ? (
                <Pagos />
            ) : pestana === "informes" ? (
                <Informes />
            ) : pestana === "suscripcion" ? (
                <Suscripcion />
            ) : null}
            {/*<div className="container main-section">
                <div className="row">
                    <div className="col-12 control-panel-arrow-background p-0">
                        <img src="/images/arrow-background.png" className="arrow-image" />
                        <div className="control-panel-potency-bnefits">
                            <h2 id="control-panel-potency-bnefits-next">Potencia hasta</h2>
                            <h2 id="control-panel-potency-bnefits-title">
                                <span className="control-panel-benefits">+20%</span>Tus ingresos pasivos
                            </h2>
                            <p className="control-panel-potency-bnefits-description">
                                El plan de referidos te permite incrementar tus ganancias y disfrutar de más tiempo para
                                invertir
                            </p>
                            <Link to="/controlPanel/referidos">
                                <button className="potency-bnefits-button">Comienza ahora</button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>*/}
            <div className="container warning-section">
                <div className="row">
                    <div className="col-12">
                        <p>
                            <strong>{t("Pages.Control-panel.important.title")}:</strong>{" "}
                            {t("Pages.Control-panel.important.message")}
                        </p>
                    </div>
                </div>
            </div>
        </Panel>
    )
}
