import React, { useContext, useEffect, useState } from "react"
import { NavLink, Link } from "react-router-dom"
import { useTranslation } from "react-i18next"
import * as FirebaseMngr from "../../../Utilities/FirebaseMngr"
import { FaInbox, FaTrophy } from "react-icons/fa"
import { UserContext } from "../../../Context/UserContext"
import Panel from "../../../Panel"
import { dateWithFormatFromTime } from "../../../Utilities/DateUtilities.js"
import "./index.css"

export default function Pagos() {
    const { t } = useTranslation()
    const { user } = useContext(UserContext)
    const firebaseDatabase = FirebaseMngr.get()
    const [payments, setPayments] = useState(undefined)

    const [total, setTotal] = useState("Calculando...")

    useEffect(() => {
        console.log("mando el equiato ", user.userID, user)
        firebaseDatabase.ref("pagos/" + user.userID).on("value", (snap) => {
            let returnedPayments = snap.val()
            setPayments(returnedPayments)
        })
    }, [user])

    return (
        <React.Fragment>
            {payments === undefined ? (
                <div></div>
            ) : payments === null ? (
                <div className="container main-section">
                    <div className="row">
                        <div className="col-xl-12 mensajeSolo">
                            <FaInbox size={150} color="#e2e2e2" />
                            <h3>{t("Pages.Control-panel.Payments.empty.message")}</h3>
                            <p>{t("Pages.Control-panel.Payments.empty.advice")}</p>
                        </div>
                    </div>
                </div>
            ) : (
                <div className="container main-section" style={{ paddingTop: 50 }}>
                    <div className="row px-4" style={{ marginBottom: 30 }}>
                        <div className="col-md-6 col-sm-12 d-flex flex-column">
                            <span className="results-title">{t("Pages.Control-panel.results.title")}</span>
                            <span>
                                {Object.keys(payments).length}{" "}
                                {t("Pages.Control-panel.Payments.assigned-payments.label")}
                            </span>
                        </div>
                        <div className="col-md-6 col-sm-12 text-right"></div>
                    </div>
                    <div className="row">
                        <table class="table" id="paymentsTable">
                            <thead>
                                <tr>
                                    <th scope="col">
                                        {t("Pages.Control-panel.Payments.assigned-payments.description")}
                                    </th>
                                    <th scope="col">{t("Pages.Control-panel.Payments.assigned-payments.amount")}</th>
                                    <th scope="col">{t("Pages.Control-panel.Payments.assigned-payments.date")}</th>
                                    <th scope="col">{t("Pages.Control-panel.Payments.assigned-payments.state")}</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                {Object.keys(payments).map((index) => (
                                    <tr>
                                        <td>
                                            {t(
                                                `Pages.Control-panel.Payments.descriptions.${payments[index].descripcion}`
                                            )}
                                        </td>
                                        <td>$ {payments[index].monto}</td>
                                        <td>
                                            {dateWithFormatFromTime(payments[index].fecha).map((piece, i) =>
                                                !(i % 2) ? t(`Date.${i === 0 ? "days" : "months"}.${piece}`) : piece
                                            )}
                                        </td>
                                        <td>{t(`Pages.Control-panel.Payments.status.${payments[index].estado}`)}</td>
                                        <td>
                                            <img width="24" height="24" src="/images/svg/gray-download.svg" />
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            )}
        </React.Fragment>
    )
}
