import React, { useContext, useEffect, useState } from "react"
import { NavLink, Link } from "react-router-dom"
import * as FirebaseMngr from "../../../Utilities/FirebaseMngr"
import { FaInbox, FaTrophy, FaBitcoin } from "react-icons/fa"
import { useTranslation } from "react-i18next"
import { UserContext } from "../../../Context/UserContext"
import Panel from "../../../Panel"
import { dateWithFormatFromTime, dateFromTimeStamp } from "../../../Utilities/DateUtilities.js"
import "./index.css"
import Constants from "../../../Utilities/Constants"

export default function Pagos() {
    const { t } = useTranslation()
    const { user } = useContext(UserContext)
    const firebaseDatabase = FirebaseMngr.get()
    const [invoices, setInvoices] = useState(undefined)
    const [packages, setPackages] = useState([])

    useEffect(() => {
        console.log("mando el equiato ", user.userID, user)
        const getInfo = async () => {
            const snap = await firebaseDatabase.ref("packages").once("value")
            setPackages(snap.val())
        }
        firebaseDatabase.ref("facturas/" + user.userID).on("value", (snap) => {
            let returnedInvoices = snap.val()
            setInvoices(returnedInvoices)
        })
        getInfo()
    }, [user])

    return (
        <React.Fragment>
            {invoices === undefined ? (
                <div></div>
            ) : (
                <React.Fragment>
                    {invoices === null ? (
                        <div className="container main-section">
                            <div className="row">
                                <div className="col-xl-12 mensajeSolo">
                                    <FaInbox size={150} color="#e2e2e2" />
                                    <h3>{t("Pages.Control-panel.Invoices.empty.message")}</h3>
                                    <p>{t("Pages.Control-panel.Invoices.empty.advice")}</p>
                                    <Link to="/paquetes">
                                        <FaTrophy style={{ marginRight: 10 }} />{" "}
                                        {t("Pages.Control-panel.Invoices.empty.upgrade-button")}
                                    </Link>
                                </div>
                            </div>
                        </div>
                    ) : (
                        <React.Fragment>
                            {/*<div className="container filter-border"></div>
                            <div className={`container filter-section ${!showFilters ? "hide" : ""}`}>
                                <div className="row">
                                    <div className="col-4">
                                        <select id="filterOption">
                                            <option value="">A</option>
                                            <option value="">B</option>
                                            <option value="">C</option>
                                        </select>
                                    </div>
                                    <div className="col-8">
                                        <input type="text" id="filterText" placeholder="Buscar por..." />
                                    </div>
                                </div>
                            </div>
                            <div className="container filter-border shadow-box"></div>*/}
                            <div className="container main-section" style={{ paddingTop: 50 }}>
                                {/*<div className="row d-flex justify-content-center">
                                    <div className="filterButton" onClick={() => setShowFilters(!showFilters)}>
                                        {showFilters ? <span>Ocultar filtros</span> : <span>Mostrar filtros</span>}
                                        {!showFilters ? (
                                            <svg
                                                width="10"
                                                height="5"
                                                viewBox="0 0 10 5"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path d="M0 0L5 5L10 0H0Z" fill="#9B5BC1" />
                                            </svg>
                                        ) : (
                                            <svg
                                                width="10"
                                                height="5"
                                                viewBox="0 0 10 5"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path d="M10 5L5 -4.37114e-07L0 5L10 5Z" fill="#9B5BC1" />
                                            </svg>
                                        )}
                                    </div>
                                </div>
                                        <div className="row"></div>*/}
                                <div className="row px-4" style={{ marginBottom: 30 }}>
                                    <div className="col-md-6 col-sm-12 d-flex flex-column">
                                        <span className="results-title">{t("Pages.Control-panel.results.title")}</span>
                                        <span>
                                            {Object.keys(invoices).length}{" "}
                                            {t("Pages.Control-panel.Invoices.issued-invoices.label")}
                                        </span>
                                    </div>
                                    <div className="col-md-6 col-sm-12 text-right"></div>
                                </div>
                                <div className="row">
                                    <div class="col-12 listaFacturas">
                                        {Object.keys(invoices).map((index, i) => {
                                            const invoice = invoices[index]
                                            return (
                                                <div className="invoice" key={i}>
                                                    <div className="present">
                                                        <img
                                                            width="32"
                                                            height="32"
                                                            src="/images/svg/invoice-logo.svg"
                                                        />
                                                        <div>
                                                            <span className="gordo">
                                                                {t(
                                                                    `Pages.Control-panel.Invoices.descriptions.${invoice.subject}`
                                                                ) +
                                                                    " " +
                                                                    packages[invoice.packageID]?.name}
                                                            </span>
                                                            {invoice.paid ? (
                                                                <span className="peque">{`${dateFromTimeStamp(
                                                                    invoice.payDate
                                                                )} ${t(
                                                                    "Pages.Control-panel.Invoices.issued-invoices.to"
                                                                )} ${dateFromTimeStamp(invoice.expirationDate)}`}</span>
                                                            ) : (
                                                                ""
                                                            )}
                                                        </div>
                                                    </div>
                                                    <div className="data">
                                                        <span className="peque">
                                                            {t("Pages.Control-panel.Invoices.issued-invoices.amount")}
                                                        </span>
                                                        <span className="gordo">
                                                            {invoice.amount}{" "}
                                                            {t("Pages.Control-panel.Invoices.issued-invoices.euros")}{" "}
                                                            {!invoice.red ? (
                                                                <FaBitcoin size={24} />
                                                            ) : (
                                                                <img src="/images/tron.svg" width={24} />
                                                            )}
                                                        </span>
                                                    </div>
                                                    <div className="data">
                                                        <span className="peque">
                                                            {t("Pages.Control-panel.Invoices.issued-invoices.date")}
                                                        </span>
                                                        <span className="gordo">
                                                            {dateWithFormatFromTime(invoice.createdAt).map((piece, i) =>
                                                                !(i % 2)
                                                                    ? t(`Date.${i === 0 ? "days" : "months"}.${piece}`)
                                                                    : piece
                                                            )}
                                                        </span>
                                                    </div>
                                                    <div className="botones">
                                                        {invoice.status === "paid" &&
                                                            invoice.payments.map((invo) => (
                                                                <a
                                                                    target="_blank"
                                                                    href={
                                                                        (!invoice.red
                                                                            ? Constants.btcExplorer()
                                                                            : "https://tronscan.org/#/transaction") +
                                                                        "/" +
                                                                        invo.txid
                                                                    }
                                                                >
                                                                    <img
                                                                        width="24"
                                                                        height="24"
                                                                        src="/images/svg/purple-download.svg"
                                                                    />
                                                                </a>
                                                            ))}
                                                        {invoice.status === "pending" ? (
                                                            <NavLink to={`/factura/${index}`}>
                                                                <button>
                                                                    {t(
                                                                        "Pages.Control-panel.Invoices.issued-invoices.pay-button"
                                                                    )}
                                                                </button>{" "}
                                                            </NavLink>
                                                        ) : (
                                                            <span className="otroEstado">
                                                                {t(
                                                                    `Pages.Control-panel.Invoices.status.${invoice.status}`
                                                                )}
                                                            </span>
                                                        )}
                                                    </div>
                                                </div>
                                            )
                                        })}
                                    </div>
                                </div>
                            </div>
                        </React.Fragment>
                    )}
                </React.Fragment>
            )}
        </React.Fragment>
    )
}
