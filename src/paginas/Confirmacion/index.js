import React, { useContext, useEffect, useState } from "react"
import { NavLink } from "react-router-dom"
import * as FirebaseMngr from "../../Utilities/FirebaseMngr"
import { UserContext } from "../../Context/UserContext"
import Panel from "../../Panel"
import "./index.css"

export default function Confirmacion() {
    const { user } = useContext(UserContext)
    const [packageName, setpackageName] = useState("")
    const firebaseDatabase = FirebaseMngr.get()
    useEffect(() => {
        const getInfo = async () => {
            const snapshot = await firebaseDatabase
                .ref("packages/" + user.paquete)
                .once("value")
            const packageItem = snapshot.val()
            if (packageItem) {
                setpackageName(packageItem.name)
            }
        }
        if (user && user.userID) {
            getInfo()
        }
    }, [user])

    return (
        <Panel smallBackground={true} title="Facturación">
            <React.Fragment>
                {user.paquete === undefined ? (
                    <div></div>
                ) : (
                    <React.Fragment>
                        <div className="container pb-5">
                            <div className="row">
                                <div className="col-lg-6 col-md-12 route">
                                    <NavLink to="/controlPanel/facturas" className="route-link">
                                        Panel de control
                                    </NavLink>{" "}
                                    <span className="route-arrow">&gt;</span>{" "}
                                    <span className="route-current">Facturación</span>
                                </div>
                                <div className="col-lg-6 col-md-12 text-right">
                                    <NavLink to="/controlPanel/facturas" className="return-button">
                                        Volver al panel
                                    </NavLink>
                                </div>
                            </div>
                        </div>
                        <div className="container bg-white">
                            <div className="row py-5 d-flex">
                                <div className="col-2 d-flex flex-column pr-0 mr-0">
                                    <div className="billing-element">Información</div>
                                    <div className="billing-element">Factura</div>
                                    <div className="billing-element selected">Confirmación</div>
                                </div>
                                <div className="info-container col-10 ml-0 info-container d-flex flex-column justify-content-center align-items-center">
                                    <svg
                                        width="100"
                                        height="101"
                                        viewBox="0 0 100 101"
                                        fill="none"
                                        xmlns="http://www.w3.org/2000/svg"
                                    >
                                        <path
                                            d="M72.95 27.9016L40 60.8516L22.05 42.9516L15 50.0016L40 75.0016L80 35.0016L72.95 27.9016ZM50 0.00158691C22.4 0.00158691 0 22.4016 0 50.0016C0 77.6016 22.4 100.002 50 100.002C77.6 100.002 100 77.6016 100 50.0016C100 22.4016 77.6 0.00158691 50 0.00158691ZM50 90.0016C27.9 90.0016 10 72.1016 10 50.0016C10 27.9016 27.9 10.0016 50 10.0016C72.1 10.0016 90 27.9016 90 50.0016C90 72.1016 72.1 90.0016 50 90.0016Z"
                                            fill="#9B5BC1"
                                        />
                                    </svg>
                                    <div className="confirmation-title">Ya eres miembro {packageName}</div>
                                    <p className="confirmation-description">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean aliquet sapien
                                        et molestie euismod. Maecenas tristique odio vitae dapibus congue. Sed lorem
                                        odio, consectetur non bibendum et, semper in lacus.
                                    </p>
                                    <NavLink to="/controlPanel/facturas" className="confirmation-button">Volver a facturas</NavLink>
                                </div>
                            </div>
                        </div>
                    </React.Fragment>
                )}
            </React.Fragment>
        </Panel>
    )
}
