import React, { useState, useEffect } from "react"
import { BrowserRouter as Router, Switch, Route, Link, useParams } from "react-router-dom"
import { useTranslation } from "react-i18next"
import * as FirebaseMngr from "../../Utilities/FirebaseMngr"
import Panel from "../../Panel"
import Header from "./Header"
import "./index.css"

export default function Informe() {
    const { t } = useTranslation()
    const { reportID } = useParams()
    const [report, setReport] = useState({})
    const firebaseDatabase = FirebaseMngr.get()
    useEffect(() => {
        window.scrollTo(0, 0)
        const getInfo = async () => {
            const snapshot = await firebaseDatabase.ref("reports/" + reportID).once("value")
            setReport(snapshot.val())
        }
        if (reportID) getInfo()
    }, [])

    return (
        <Panel hideHeader={true} backgroundWhite={true} secondFooter={true}>
            <Header report={report} />

            <div id="signal-trading" className="container">
                <div className="row">
                    <div className="col-12 text-center">
                        <h2 id="signal-trading-title">{t("Pages.Report.signal-trading.title")}</h2>
                        <p>{t("Pages.Report.signal-trading.description")}</p>
                        <div id="levels-trading-container">
                            <div className="level-trading">
                                <img width="58" height="31" src="/images/svg/suscriptor-level-trading-logo.svg" />
                                <span>{t("Pages.Report.signal-trading.levels.suscriptor")}</span>
                            </div>
                            <div className="level-trading">
                                <img width="58" height="31" src="/images/svg/conservative-level-trading-logo.svg" />
                                <span>{t("Pages.Report.signal-trading.levels.conservative")}</span>
                            </div>
                            <div className="level-trading">
                                <img width="58" height="31" src="/images/svg/moderate-level-trading-logo.svg" />
                                <span>{t("Pages.Report.signal-trading.levels.moderate")}</span>
                            </div>
                            <div className="level-trading">
                                <img width="58" height="31" src="/images/svg/hard-level-trading-logo.svg" />
                                <span>{t("Pages.Report.signal-trading.levels.hard")}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12 col-lg-6 mt-5 d-flex flex-column align-items-center">
                        <table id="s-table">
                            <thead>
                                <tr>
                                    <td>{t("Pages.Report.table.title")}</td>
                                    <td>
                                        <img
                                            width="58"
                                            height="31"
                                            src="/images/svg/suscriptor-level-trading-logo.svg"
                                        />
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{t("Pages.Report.table.s.amount")}</td>
                                    <td>{report.sTotalImport}</td>
                                </tr>
                                <tr>
                                    <td>{t("Pages.Report.table.s.multiplier")}</td>
                                    <td>{report.sMultiplier}</td>
                                </tr>
                                <tr>
                                    <td>{t("Pages.Report.table.s.profits")}</td>
                                    <td>{report.sProfits}</td>
                                </tr>
                                <tr>
                                    <td colSpan="2">{t("Pages.Report.table.s.description")}</td>
                                </tr>
                            </tbody>
                        </table>
                        <button className="return-benefits-button mt-4">
                            {t("Pages.Report.table.open-position-button")}
                        </button>
                    </div>
                    <div className="col-sm-12 col-lg-6 mt-5 overflow-auto d-flex flex-column align-items-center">
                        <table id="cma-table">
                            <thead>
                                <tr>
                                    <td>{t("Pages.Report.table.title")}</td>
                                    <td>
                                        <img
                                            width="58"
                                            height="31"
                                            src="/images/svg/conservative-level-trading-logo.svg"
                                        />
                                    </td>
                                    <td>
                                        <img width="58" height="31" src="/images/svg/moderate-level-trading-logo.svg" />
                                    </td>
                                    <td>
                                        <img width="58" height="31" src="/images/svg/hard-level-trading-logo.svg" />
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{t("Pages.Report.table.cma.amount")}</td>
                                    <td>{report.cImport}</td>
                                    <td>{report.mImport}</td>
                                    <td>{report.aImport}</td>
                                </tr>
                                <tr>
                                    <td>{t("Pages.Report.table.cma.price")}</td>
                                    <td>{report.cPrice}</td>
                                    <td>{report.mPrice}</td>
                                    <td>{report.aPrice}</td>
                                </tr>
                                <tr>
                                    <td>{t("Pages.Report.table.cma.multiplier")}</td>
                                    <td>{report.cMultiplier}</td>
                                    <td>{report.mMultiplier}</td>
                                    <td>{report.aMultiplier}</td>
                                </tr>
                                <tr>
                                    <td>{t("Pages.Report.table.cma.profits")}</td>
                                    <td>{report.cProfits}</td>
                                    <td>{report.mProfits}</td>
                                    <td>{report.aProfits}</td>
                                </tr>
                                <tr>
                                    <td>{t("Pages.Report.table.cma.losses")}</td>
                                    <td>{report.cLosses}</td>
                                    <td>{report.mLosses}</td>
                                    <td>{report.aLosses}</td>
                                </tr>
                            </tbody>
                        </table>
                        <button className="return-benefits-button mt-4">
                            {t("Pages.Report.table.open-position-button")}
                        </button>
                    </div>
                </div>
            </div>

            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <h3 className="justification-title">{t("Pages.Report.justifications.title")}</h3>
                        <div className="Container" dangerouslySetInnerHTML={{ __html: report.technicalDesc }}></div>
                        <div className="Container" dangerouslySetInnerHTML={{ __html: report.fundamentalDesc }}></div>
                    </div>
                </div>
            </div>

            <div className="container">
                <div className="row">
                    <div className="col-12 arrow-background p-0">
                        <img src="/images/arrow-background.png" className="arrow-image" />
                        <div className="potency-bnefits">
                            <h2 id="potency-bnefits-next">{t("Pages.Main.potency-benefits.title")}</h2>
                            <h2 id="potency-bnefits-title">
                                <span className="benefits">{t("Pages.Main.potency-benefits.benefits")}</span>
                                {t("Pages.Main.potency-benefits.subtitle")}
                            </h2>
                            <p className="potency-bnefits-description">
                                {t("Pages.Main.potency-benefits.description")}
                            </p>
                            <Link to="/controlPanel/referidos">
                                <button className="potency-bnefits-button">{t("Pages.Main.start-now-button")}</button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
            <div className="container warning-section">
                <div className="row">
                    <div className="col-12">
                        <p>
                            <strong>{t("Pages.Control-panel.important.title")}:</strong>{" "}
                            {t("Pages.Control-panel.important.message")}
                        </p>
                    </div>
                </div>
            </div>
        </Panel>
    )
}
