import React, { useState, useContext, useEffect } from "react"
import { Link, NavLink } from "react-router-dom"
import { useTranslation } from "react-i18next"
import { UserContext } from "../../../Context/UserContext"
import "./index.css"

export default function Header({ report }) {
    const { t } = useTranslation()
    const { user, signOutUser } = useContext(UserContext)
    const [menuToggled, setMenuToggled] = useState(false)

    return (
        <div id="report-header-container">
            <div className="container">
                <div className="row mb-5">
                    <nav
                        className="navbar navbar-expand-lg navbar-dark d-flex"
                        style={{ backgroundColor: "#FFFFFF00", width: "100%" }}
                    >
                        <Link className="navbar-brand" to="/panel">
                            <img id="header-logo" width="254" height="48" src="/images/svg/logo.svg" />
                        </Link>
                        <a
                            id="support-button"
                            href="https://telegram.me/soporteadncrypto"
                            target="_blank"
                            className="ml-auto"
                        >
                            {t("Panel.Header.support-button")}
                        </a>
                        <div className="toggle">
                            <button
                                className="dropdown-toggle"
                                id="toggle-button"
                                onClick={() => {
                                    setMenuToggled(!menuToggled)
                                }}
                            >
                                {t("Panel.Header.toggle-button")} {user.nombre}
                            </button>
                            <div id="toggle-menu" className={`${menuToggled ? "visible" : ""}`}>
                                <NavLink to="/panel" className="toggle-item">
                                    {t("Panel.Header.toggle-menu.home")}
                                </NavLink>
                                <NavLink to="/controlPanel/facturas" className="toggle-item">
                                    {t("Panel.Header.toggle-menu.account")}
                                </NavLink>
                                <NavLink to="/paquetes" className="toggle-item">
                                    {t("Panel.Header.toggle-menu.upgrade")}
                                </NavLink>
                                <a
                                    className="toggle-item"
                                    onClick={() => {
                                        signOutUser()
                                    }}
                                >
                                    {t("Panel.Header.toggle-menu.logout")}
                                </a>
                            </div>
                        </div>
                    </nav>
                </div>

                <div className="title-n-desc-row row mt-5 pt-5 justify-content-between">
                    <div className="col-xl-3 col-lg-12 d-flex flex-column justify-content-center align-items-center">
                        <img width="265.2" height="75.6" src="/images/svg/report-logo.svg" />
                        <div className="return-benefits-container">
                            <span className="return-benefits-title">
                                {t("Pages.Report.Header.return-benefits.title")}
                            </span>
                            <span className="return-benefits">{report.returnBenefits}</span>
                        </div>
                        <button className="return-benefits-button">
                            {t("Pages.Report.Header.return-benefits.button")}
                        </button>
                    </div>
                    <div className="col-xl-9 col-lg-12 pl-5">
                        <h2 className="report-title">{report.title}</h2>
                        <p className="report-description">{report.description}</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-6 col-lg-4 col-xl-2 d-flex flex-column justify-content-start align-items-center">
                        <img className="icon-header" width="60" height="75" src="/images/svg/instrument-logo.svg" />
                        <div className="icon-header-title">{t("Pages.Report.Header.info.instrument")}</div>
                        <div className="icon-header-content">{report.instrument}</div>
                    </div>
                    <div className="col-sm-6 col-lg-4 col-xl-2 d-flex flex-column justify-content-start align-items-center">
                        <img className="icon-header" width="77" height="77" src="/images/svg/symbol-logo.svg" />
                        <div className="icon-header-title">{t("Pages.Report.Header.info.symbol")}</div>
                        <div className="icon-header-content">{report.symbol}</div>
                    </div>
                    <div className="col-sm-6 col-lg-4 col-xl-2 d-flex flex-column justify-content-start align-items-center">
                        <img className="icon-header" width="99" height="99" src="/images/svg/start-date-logo.svg" />
                        <div className="icon-header-title">{t("Pages.Report.Header.info.start-date")}</div>
                        <div className="icon-header-content">{report.startDate}</div>
                    </div>
                    <div className="col-sm-6 col-lg-4 col-xl-2 d-flex flex-column justify-content-start align-items-center">
                        <img className="icon-header" width="99" height="99" src="/images/svg/end-date-logo.svg" />
                        <div className="icon-header-title">{t("Pages.Report.Header.info.end-date")}</div>
                        <div className="icon-header-content">{report.endDate}</div>
                    </div>
                    <div className="col-sm-6 col-lg-4 col-xl-2 d-flex flex-column justify-content-start align-items-center">
                        <img className="icon-header" width="75" height="74" src="/images/svg/advice-logo.svg" />
                        <div className="icon-header-title">{t("Pages.Report.Header.info.advice")}</div>
                        <div className="icon-header-content">{report.advice}</div>
                    </div>
                    <div className="col-sm-6 col-lg-4 col-xl-2 d-flex flex-column justify-content-start align-items-center">
                        <img className="icon-header" width="74" height="74" src="/images/svg/final-price-logo.svg" />
                        <div className="icon-header-title">{t("Pages.Report.Header.info.final-price")}</div>
                        <div className="icon-header-content">{report.finalPrice}</div>
                    </div>
                </div>
            </div>
        </div>
    )
}
