import React from "react"
import firebase from "firebase"
import * as axios from "axios"
import { Link } from "react-router-dom"
import Loader from "react-loader-spinner"
import Constants from "../../Utilities/Constants"
import Scroller from "../../componentes/Scroller"
import { FaEye, FaLaughWink, FaEnvelope } from "react-icons/fa"
// import "./index.css"

export default function Registro() {
    const [error, setError] = React.useState(false)
    const [registroPublico, setRegistroPublico] = React.useState(false) //cambiar
    const [cargando, setCargando] = React.useState(false)
    const [form, setForm] = React.useState({})
    const handleInputChange = (event) => {
        event.persist()
        setForm((form) => ({ ...form, [event.target.name]: event.target.value }))
    }
    const registrarUsuario = async (ev) => {
        setCargando(true)
        ev.preventDefault()
        console.log(form)
        const { email } = form
        try {
            await firebase.auth().sendPasswordResetEmail(email)
        } catch (error) {
            setError(error.message)
        }
        setRegistroPublico(true)
        setCargando(false)
    }
    const resetPage = async (ev) => {
        setRegistroPublico(false)
        setError(null)
        setForm({})
    }
    return (
        <div className="fullScreen">
            <div className="container-form">
                <div>
                    <img src="./images/logoblanco.svg" width="100%" style={{ padding: 20 }} className="logo" />
                </div>
                <div></div>
                <div className="wrpLink">
                    <Link to="/" className="link">
                        Cancelar
                    </Link>
                </div>
                <div></div>
                <div className="form">
                    {!cargando && !registroPublico ? (
                        <form className="centered whited filas" onSubmit={registrarUsuario}>
                            <div style={{ fontSize: 24, opacity: 0.5, fontWeight: 400 }}>
                                Recuperación de contraseña
                            </div>
                            <div className="form-title" style={{ fontSize: 48, fontWeight: 700 }}>
                                Correo electrónico:
                            </div>
                            <input
                                type="email"
                                className="entradaDatos"
                                required
                                name="email"
                                value={form.correo}
                                onChange={handleInputChange}
                            />
                            <input className="buttnFull" type="submit" value="Solicitar correo de recuperación" />
                        </form>
                    ) : !registroPublico ? (
                        <div className="centered filas">
                            <Loader type="BallTriangle" color="white" height={100} width={100} />
                        </div>
                    ) : (
                        <div className="centered whited filas" style={{ fontSize: 24, textAlign: "center" }}>
                            {error ? (
                                <span>
                                    Error al enviar correo para recuperar contraseña: <br />
                                    {error}
                                    <button className="buttnFull" onClick={resetPage}>
                                        Regresar
                                    </button>
                                </span>
                            ) : (
                                <React.Fragment>
                                    <span>
                                        Por favor revisa tu correo electrónico para seguir con el restablecimiento de tu
                                        contraseña.
                                    </span>
                                    <br />
                                    <FaEnvelope size={50} />
                                    <Link className="buttnFull" style={{ textDecoration: "none" }} to="/acceder">
                                        Regresar
                                    </Link>
                                </React.Fragment>
                            )}
                        </div>
                    )}
                </div>
                <div>
                    <img src="./images/flechas.svg" className="flechas" />
                </div>
                <div></div>
                <div className="social-container">
                    <div style={{ width: "33%" }}></div>
                    <div className="social">
                        <a href="#">
                            <svg
                                className="mx-3"
                                width="35"
                                height="35"
                                viewBox="0 0 40 45"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    d="M20 0.5C8.95097 0.5 0 10.3475 0 22.5C0 34.6525 8.95226 44.5 20 44.5C31.049 44.5 40 34.6525 40 22.5C40 10.3475 31.0477 0.5 20 0.5ZM29.8232 15.5721L26.5406 32.5874C26.2981 33.7938 25.6452 34.0862 24.7342 33.5185L19.7342 29.4648L17.3226 32.0196C17.0568 32.312 16.831 32.5604 16.3148 32.5604L16.6697 26.9625L25.9355 17.7537C26.3394 17.3634 25.8465 17.1419 25.3135 17.5323L13.8619 25.4622L8.92645 23.7675C7.85419 23.3956 7.82968 22.588 9.15226 22.0203L28.4348 13.8405C29.3303 13.4857 30.1123 14.0804 29.8219 15.5707L29.8232 15.5721Z"
                                    fill="white"
                                />
                            </svg>
                        </a>
                        <a href="#">
                            <svg
                                className="mx-3"
                                width="35"
                                height="35"
                                viewBox="0 0 50 45"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    d="M49.2421 5.71193C47.8708 7.95554 46.1885 9.86812 44.1951 11.4497C44.2148 11.8738 44.2247 12.3532 44.2247 12.8879C44.2247 15.8586 43.8309 18.8328 43.0435 21.8107C42.256 24.7886 41.0531 27.6389 39.4349 30.3618C37.8166 33.0846 35.8889 35.4968 33.6519 37.5983C31.4149 39.6999 28.7258 41.3756 25.5848 42.6253C22.4438 43.8751 19.0775 44.5 15.4859 44.5C9.88054 44.5 4.71859 42.8354 0 39.5063C0.837493 39.6088 1.64072 39.6601 2.40968 39.6601C7.09248 39.6601 11.275 38.0735 14.9572 34.9003C12.7738 34.8563 10.8184 34.117 9.091 32.6823C7.36361 31.2477 6.17486 29.4146 5.52476 27.1829C6.16735 27.3175 6.80156 27.3848 7.4274 27.3848C8.32825 27.3848 9.21468 27.2556 10.0867 26.9971C7.7566 26.4837 5.8236 25.2072 4.28768 23.1673C2.75181 21.1275 1.98388 18.7732 1.98388 16.1043V15.9668C3.41409 16.8379 4.93964 17.3012 6.56052 17.3568C5.1802 16.3446 4.08581 15.0246 3.27736 13.3969C2.46885 11.7691 2.0646 10.0076 2.0646 8.11239C2.0646 6.11353 2.51995 4.25221 3.43064 2.52844C5.96456 5.94891 9.03493 8.68266 12.6418 10.7297C16.2486 12.7767 20.1181 13.9136 24.2502 14.1403C24.074 13.3324 23.9857 12.4893 23.9854 11.611C23.9854 8.54428 24.972 5.92578 26.9452 3.75547C28.9184 1.58516 31.2989 0.5 34.0868 0.5C37.0056 0.5 39.4642 1.66909 41.4625 4.00728C43.7456 3.50982 45.8834 2.61003 47.8758 1.30793C47.1082 3.96523 45.6291 6.01516 43.4385 7.45771C45.4519 7.19454 47.3864 6.61261 49.242 5.71193H49.2421Z"
                                    fill="white"
                                />
                            </svg>
                        </a>
                        <a href="#">
                            <svg
                                className="mx-3"
                                width="35"
                                height="35"
                                viewBox="0 0 40 45"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    d="M19.995 15.1635C16.3225 15.1635 13.3255 18.4603 13.3255 22.5C13.3255 26.5397 16.3225 29.8365 19.995 29.8365C23.6675 29.8365 26.6645 26.5397 26.6645 22.5C26.6645 18.4603 23.6675 15.1635 19.995 15.1635ZM39.9985 22.5C39.9985 19.4619 40.0235 16.4514 39.8684 13.4189C39.7133 9.89648 38.9828 6.77037 36.6412 4.19463C34.2946 1.61339 31.4577 0.815349 28.2556 0.644734C25.4937 0.474118 22.7569 0.501637 20 0.501637C17.2381 0.501637 14.5013 0.474118 11.7444 0.644734C8.54226 0.815349 5.70034 1.61889 3.35876 4.19463C1.01217 6.77588 0.286681 9.89648 0.131576 13.4189C-0.0235287 16.4569 0.00148823 19.4675 0.00148823 22.5C0.00148823 25.5325 -0.0235287 28.5486 0.131576 31.5811C0.286681 35.1035 1.01717 38.2296 3.35876 40.8054C5.70534 43.3866 8.54226 44.1847 11.7444 44.3553C14.5063 44.5259 17.2431 44.4984 20 44.4984C22.7619 44.4984 25.4987 44.5259 28.2556 44.3553C31.4577 44.1847 34.2997 43.3811 36.6412 40.8054C38.9878 38.2241 39.7133 35.1035 39.8684 31.5811C40.0285 28.5486 39.9985 25.5381 39.9985 22.5ZM19.995 33.7881C14.3162 33.7881 9.73306 28.7467 9.73306 22.5C9.73306 16.2533 14.3162 11.2119 19.995 11.2119C25.6738 11.2119 30.2569 16.2533 30.2569 22.5C30.2569 28.7467 25.6738 33.7881 19.995 33.7881ZM30.6772 13.3858C29.3513 13.3858 28.2806 12.208 28.2806 10.7496C28.2806 9.29107 29.3513 8.11328 30.6772 8.11328C32.0031 8.11328 33.0738 9.29107 33.0738 10.7496C33.0742 11.0959 33.0125 11.4389 32.8922 11.7589C32.7719 12.079 32.5954 12.3698 32.3727 12.6147C32.1501 12.8596 31.8858 13.0537 31.5948 13.1861C31.3039 13.3184 30.992 13.3863 30.6772 13.3858Z"
                                    fill="white"
                                />
                            </svg>
                        </a>
                    </div>
                </div>
                <div></div>
            </div>
        </div>
    )
}
