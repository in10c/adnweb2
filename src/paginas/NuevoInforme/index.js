import React, { useContext, useState, useEffect } from "react"
import { Redirect, NavLink, useParams } from "react-router-dom"
import Axios from "axios"
import * as FirebaseMngr from "../../Utilities/FirebaseMngr"
import Panel from "../../Panel"
import Constants from "../../Utilities/Constants"
import { UserContext } from "../../Context/UserContext"
import SunEditor from "suneditor-react"
import "suneditor/dist/css/suneditor.min.css"

export default function NuevoInforme() {
    const { reportID } = useParams()
    const firebaseDatabase = FirebaseMngr.get()

    const [report, setReport] = useState({
        title: "",
        description: "",
        returnBenefits: "",
        instrument: "",
        symbol: "",
        startDate: "",
        endDate: "",
        advice: "",
        finalPrice: "",
        sTotalImport: "",
        sMultiplier: "",
        sProfits: "",
        cImport: "",
        cPrice: "",
        cMultiplier: "",
        cProfits: "",
        cLosses: "",
        mImport: "",
        mPrice: "",
        mMultiplier: "",
        mProfits: "",
        mLosses: "",
        aImport: "",
        aPrice: "",
        aMultiplier: "",
        aProfits: "",
        aLosses: "",
    })

    const [redirect, setRedirect] = useState(false)

    const [fundamentalDesc, setFundamentalDesc] = useState("")
    const [technicalDesc, setTechnicalDesc] = useState("")

    useEffect(() => {
        const getInfo = async () => {
            const snapshot = await firebaseDatabase.ref("reports/" + reportID).once("value")

            const currentReport = snapshot.val()

            if (currentReport) {
                setReport(currentReport)
                setFundamentalDesc(currentReport.fundamentalDesc)
                setTechnicalDesc(currentReport.technicalDesc)
            }
        }
        if (reportID !== undefined) {
            getInfo()
        }
    }, [])

    const OnSubmit = async (event) => {
        event.preventDefault()
        const {
            title,
            description,
            returnBenefits,
            instrument,
            symbol,
            startDate,
            endDate,
            advice,
            finalPrice,
            sTotalImport,
            sMultiplier,
            sProfits,
            cImport,
            cPrice,
            cMultiplier,
            cProfits,
            cLosses,
            mImport,
            mPrice,
            mMultiplier,
            mProfits,
            mLosses,
            aImport,
            aPrice,
            aMultiplier,
            aProfits,
            aLosses,
        } = report
        if (
            !title.trim() ||
            !description.trim() ||
            !returnBenefits.trim() ||
            !instrument.trim() ||
            !symbol.trim() ||
            !startDate.trim() ||
            !endDate.trim() ||
            !advice.trim() ||
            !finalPrice.trim() ||
            !sTotalImport.trim() ||
            !sMultiplier.trim() ||
            !sProfits.trim() ||
            !cImport.trim() ||
            !cPrice.trim() ||
            !cMultiplier.trim() ||
            !cProfits.trim() ||
            !cLosses.trim() ||
            !mImport.trim() ||
            !mPrice.trim() ||
            !mMultiplier.trim() ||
            !mProfits.trim() ||
            !mLosses.trim() ||
            !aImport.trim() ||
            !aPrice.trim() ||
            !aMultiplier.trim() ||
            !aProfits.trim() ||
            !aLosses.trim() ||
            !fundamentalDesc ||
            !technicalDesc
        ) {
            alert("Faltan campos")

            return
        }

        console.log(report)

        const response = await Axios.post(Constants.servidorFunciones() + "/generateReport", {
            ...report,
            fundamentalDesc,
            technicalDesc,
        })

        if (response.data.success === 0) {
            alert("Ocurrio un error, intente de nuevo mas tarde")
            return
        }

        alert("Reporte guardado")
        setRedirect(true)
    }

    const OnChangeText = (event) => {
        const { id, value } = event.target
        setReport({
            ...report,
            ...{
                [id]: value,
            },
        })
    }

    const onChangeFundamental = (content) => {
        console.log(content)
        setFundamentalDesc(content)
    }

    const onChangeTechnical = (content) => {
        console.log(content)
        setTechnicalDesc(content)
    }

    return (
        <Panel>
            {redirect && <Redirect to="/reports" />}
            <div className="container">
                <div className="row mb-5">
                    <div className="col-12 white">
                        <form onSubmit={OnSubmit}>
                            <h2>Primera sección</h2>
                            <div className="form-group">
                                <label htmlFor="title">Titulo</label>
                                <input type="text" id="title" value={report.title} className="form-control" onChange={OnChangeText} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="description">Descripción</label>
                                <textarea
                                    type="text"
                                    id="description" 
                                    value={report.description}
                                    rows="10"
                                    className="form-control"
                                    onChange={OnChangeText}
                                ></textarea>
                            </div>
                            <div className="form-group">
                                <label htmlFor="returnBenefits">Retorno</label>
                                <input
                                    type="text"
                                    id="returnBenefits"
                                    value={report.returnBenefits}
                                    className="form-control"
                                    onChange={OnChangeText}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="instrument">Instrumento</label>
                                <input type="text" id="instrument" value={report.instrument} className="form-control" onChange={OnChangeText} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="symbol">Simbolo</label>
                                <input type="text" id="symbol" value={report.symbol} className="form-control" onChange={OnChangeText} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="startDate">Fecha de inicio</label>
                                <input type="date" id="startDate" value={report.startDate} className="form-control" onChange={OnChangeText} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="endDate">Fecha de cierre</label>
                                <input type="date" id="endDate" value={report.endDate} className="form-control" onChange={OnChangeText} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="advice">Consejo</label>
                                <input type="text" id="advice" value={report.advice} className="form-control" onChange={OnChangeText} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="finalPrice">Precio objetivo</label>
                                <input type="text" id="finalPrice" value={report.finalPrice} className="form-control" onChange={OnChangeText} />
                            </div>
                            <h2>Sección Señas de trading</h2>
                            <h3>S</h3>
                            <div className="form-group">
                                <label htmlFor="sTotalImport">Importe total de la operación</label>
                                <input type="text" id="sTotalImport" value={report.sTotalImport} className="form-control" onChange={OnChangeText} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="sMultiplier">Multiplicador</label>
                                <input type="text" id="sMultiplier" value={report.sMultiplier} className="form-control" onChange={OnChangeText} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="sProfits">Ganancias en $</label>
                                <input type="text" id="sProfits" value={report.sProfits} className="form-control" onChange={OnChangeText} />
                            </div>
                            <h3>C</h3>
                            <div className="form-group">
                                <label htmlFor="cImport">Importe de la operación</label>
                                <input type="text" id="cImport" value={report.cImport} className="form-control" onChange={OnChangeText} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="cPrice">Precio</label>
                                <input type="text" id="cPrice" value={report.cPrice} className="form-control" onChange={OnChangeText} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="cMultiplier">Multiplicador</label>
                                <input type="text" id="cMultiplier" value={report.cMultiplier} className="form-control" onChange={OnChangeText} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="cProfits">Ganancias en $</label>
                                <input type="text" id="cProfits" value={report.cProfits} className="form-control" onChange={OnChangeText} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="cLosses">Perdidas en $</label>
                                <input type="text" id="cLosses" value={report.cLosses} className="form-control" onChange={OnChangeText} />
                            </div>
                            <h3>M</h3>
                            <div className="form-group">
                                <label htmlFor="mImport">Importe de la operación</label>
                                <input type="text" id="mImport" value={report.mImport} className="form-control" onChange={OnChangeText} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="mPrice">Precio</label>
                                <input type="text" id="mPrice" value={report.mPrice} className="form-control" onChange={OnChangeText} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="mMultiplier">Multiplicador</label>
                                <input type="text" id="mMultiplier" value={report.mMultiplier} className="form-control" onChange={OnChangeText} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="mProfits">Ganancias en $</label>
                                <input type="text" id="mProfits" value={report.mProfits} className="form-control" onChange={OnChangeText} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="mLosses">Perdidas en $</label>
                                <input type="text" id="mLosses" value={report.mLosses} className="form-control" onChange={OnChangeText} />
                            </div>
                            <h3>A</h3>
                            <div className="form-group">
                                <label htmlFor="aImport">Importe de la operación</label>
                                <input type="text" id="aImport" value={report.aImport} className="form-control" onChange={OnChangeText} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="aPrice">Precio</label>
                                <input type="text" id="aPrice" value={report.aPrice} className="form-control" onChange={OnChangeText} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="aMultiplier">Multiplicador</label>
                                <input type="text" id="aMultiplier" value={report.aMultiplier} className="form-control" onChange={OnChangeText} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="aProfits">Ganancias en $</label>
                                <input type="text" id="aProfits" value={report.aProfits} className="form-control" onChange={OnChangeText} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="aLosses">Perdidas en $</label>
                                <input type="text" id="aLosses" value={report.aLosses} className="form-control" onChange={OnChangeText} />
                            </div>
                            <div>
                                <label htmlFor="aMultiplier">Justificación fundamental</label>
                                <SunEditor
                                    setContents={fundamentalDesc}
                                    lang="es"
                                    height="300px"
                                    width="100%"
                                    onChange={onChangeFundamental}
                                    setOptions={{
                                        buttonList: [
                                            [
                                                "blockquote",
                                                "align",
                                                "font",
                                                "fontColor",
                                                "fontSize",
                                                "formatBlock",
                                                "hiliteColor",
                                                "horizontalRule",
                                                "lineHeight",
                                                "list",
                                                "paragraphStyle",
                                                "table",
                                                "template",
                                                "textStyle",
                                                "image",
                                                "link",
                                            ],
                                        ],
                                    }}
                                />
                            </div>
                            <div>
                                <label htmlFor="aMultiplier">Justificación técnica</label>
                                <SunEditor
                                    setContents={technicalDesc}
                                    lang="es"
                                    height="300px"
                                    width="100%"
                                    onChange={onChangeTechnical}
                                    setOptions={{
                                        buttonList: [
                                            [
                                                "blockquote",
                                                "align",
                                                "font",
                                                "fontColor",
                                                "fontSize",
                                                "formatBlock",
                                                "hiliteColor",
                                                "horizontalRule",
                                                "lineHeight",
                                                "list",
                                                "paragraphStyle",
                                                "table",
                                                "template",
                                                "textStyle",
                                                "image",
                                                "link",
                                            ],
                                        ],
                                    }}
                                />
                            </div>
                            <button type="submit" className="btn btn-primary">
                                Enviar
                            </button>
                            <NavLink to="/reports" className="btn btn-danger">
                                Cancelar
                            </NavLink>
                        </form>
                    </div>
                </div>
            </div>
        </Panel>
    )
}
