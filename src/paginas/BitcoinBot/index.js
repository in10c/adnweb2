import React, { useState, useEffect } from "react"
import Panel from "../../Panel"
import { BrowserRouter as Router, Switch, Route, Link, useParams } from "react-router-dom"
import "./index.css"

export default function BitcoinBot() {
    useEffect(() => {
        window.scrollTo(0, 0)
    }, []);
    return (
        <Panel
            headerChild={
                <React.Fragment>
                    <div className="row d-flex justify-content-center mt-4 pt-4 mb-0">
                        <svg
                            width="259.2"
                            height="216"
                            viewBox="0 0 144 120"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                        >
                            <path
                                d="M40.2116 14.8261C44.5085 12.3267 49.0316 10.2817 53.7808 8.91838C58.53 7.55506 63.5053 6.64618 68.4807 6.64618C78.4314 6.41896 88.6083 8.91838 97.4283 13.69C103.987 17.3255 109.867 22.0971 114.616 28.0049L117.33 26.8688C112.58 20.2794 106.701 14.3717 99.6898 10.0545C90.4175 4.14676 79.5622 0.738465 68.4807 0.056805C62.8269 -0.170415 57.3992 0.284025 51.7454 1.42012C46.3177 2.55622 40.8901 4.6012 35.9147 7.10062C25.7378 12.3267 17.144 20.2794 10.8117 29.8226C4.47947 39.5931 0.634869 50.9541 0.182563 62.7695C-0.722048 74.3577 1.76563 86.4004 6.96715 97.0797C10.8117 104.805 16.0133 111.849 22.5717 117.757C26.4163 121.392 32.5224 120.483 35.4624 115.939C37.7239 112.303 36.8193 107.532 33.427 104.805C28.2255 100.715 23.9286 95.7164 20.5363 90.0359C15.7871 81.856 13.2994 72.54 13.0733 62.9967C12.8471 53.4535 15.3348 43.9103 20.084 35.5031C24.8332 27.3232 31.844 20.0522 40.2116 14.8261Z"
                                fill="url(#paint0_linear)"
                            />
                            <path
                                d="M123.21 37.3209L121.627 38.6842C123.436 42.0925 124.793 45.5008 125.924 49.1363C127.507 54.1351 128.185 59.3612 128.411 64.8145C128.637 59.5884 128.185 54.1351 126.828 48.9091C126.15 44.8191 124.793 40.9564 123.21 37.3209Z"
                                fill="url(#paint1_linear)"
                            />
                            <path
                                d="M143.111 18.9161L65.0884 50.0452C63.9576 50.2724 62.8269 50.7268 61.6961 51.4085C55.59 54.8168 53.5546 62.7695 57.1731 68.6772C60.5653 74.8122 68.4807 76.8571 74.3607 73.2216C75.4914 72.54 76.396 71.8583 77.0745 70.9494L143.111 18.9161Z"
                                fill="url(#paint2_linear)"
                            />
                            <defs>
                                <linearGradient
                                    id="paint0_linear"
                                    x1="99.2458"
                                    y1="18.9161"
                                    x2="99.2458"
                                    y2="75.0227"
                                    gradientUnits="userSpaceOnUse"
                                >
                                    <stop stop-color="white" />
                                    <stop offset="1" stop-color="#A5FFE9" />
                                </linearGradient>
                                <linearGradient
                                    id="paint1_linear"
                                    x1="99.2458"
                                    y1="18.9161"
                                    x2="99.2458"
                                    y2="75.0227"
                                    gradientUnits="userSpaceOnUse"
                                >
                                    <stop stop-color="white" />
                                    <stop offset="1" stop-color="#A5FFE9" />
                                </linearGradient>
                                <linearGradient
                                    id="paint2_linear"
                                    x1="99.2458"
                                    y1="18.9161"
                                    x2="99.2458"
                                    y2="75.0227"
                                    gradientUnits="userSpaceOnUse"
                                >
                                    <stop stop-color="white" />
                                    <stop offset="1" stop-color="#A5FFE9" />
                                </linearGradient>
                            </defs>
                        </svg>
                    </div>
                    <div className="row d-flex flex-column justify-content-center align-items-center mt-2">
                        <h1 id="bitcoin-bot-landing-main-title" className="text-center">
                            Beyond Cryptobot
                        </h1>
                        <h2 id="bitcoin-bot-landing-second-title" className="text-center">
                            Futuros Bitmex
                        </h2>
                    </div>
                </React.Fragment>
            }
            backgroundWhite={true}
            secondFooter={true}
        >
            <div className="container">
                <div className="row">
                    <div className="col-lg-6 col-md-12 mb-5">
                        <h2 id="coming-soon-next">Bitcoin Bitmex</h2>
                        <h2 id="coming-soon-title">Copy Trading</h2>
                        <p className="coming-soon-description">
                            Este Software busca por medio de ciertas condicionales algoritmicas ubicarse en la mejor
                            posición posible dentro del mercado generando un riesgo muy bajo o medio según decida el
                            cliente es la revolución y el futuro de las criptomonedas, sigue nuestros auditados para mas
                            información.
                        </p>
                        <a href="https://telegram.me/soporteadncrypto" target="_blank"><button className="coming-soon-button">Comenzar ahora</button></a>
                    </div>
                    <div className="col-lg-6 col-md-12">
                        <img
                            src={`/images/package-icons/Img-Copy-Tranding.svg`}
                            id="coming-soon-image"
                            className="img-fluid"
                        />
                    </div>
                </div>
            </div>

            <div id="bitcoin-bot-profits-section" className="container">
                <div className="row align-items-center">
                    <div className="col-xl-9 col-lg-8 col-md-12">
                        <h2>Puedes conseguir hasta un 22%* de ganancia</h2>
                        <p>
                            *El bot esta generando una media mensual de 7% a 22% dependiendo del nivel de riesgo (bajo o
                            medio) varía este porcentaje.
                        </p>
                    </div>
                    <div className="col-xl-3 col-lg-4 col-md-12">
                        <div id="pay-profits-container">
                            <h3>¡Pagas con tus ganancias!</h3>
                            <p>
                                De las ganancias generadas se reparte 30% a la empresa y 70% al usuario , esto con el
                                fin de garantizar el buen funcionamiento y soporte del producto.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div id="what-i-need-section" className="container">
                <div className="row">
                    <div className="col-12">
                        <h2>¿Qué necesito?</h2>
                    </div>
                </div>
                <div className="row mt-2 mb-2">
                    <div
                        className="col-lg-4 col-md-12 d-flex flex-column justify-content-center align-items-center
                    my-4"
                    >
                        <img src="/images/frame.svg" width="350px" />
                        <p>Poseer una cuenta VIP en nuestra plataforma</p>
                    </div>
                    <div
                        className="col-lg-4 col-md-12 d-flex flex-column justify-content-center align-items-center
                    my-4"
                    >
                        <svg
                            width="200"
                            height="200"
                            viewBox="0 0 160 160"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                        >
                            <path
                                d="M88.4762 88.4502H113.333V71.5498H88.4762V48H70.0476V71.5498H45.3333V88.4502H70.0476V112H88.4762V88.4502Z"
                                fill="#F4F4F4"
                            />
                            <path
                                fill-rule="evenodd"
                                clip-rule="evenodd"
                                d="M0 80C0 35.82 35.82 0 80 0C124.18 0 160 35.82 160 80C160 124.18 124.18 160 80 160C35.82 160 0 124.18 0 80ZM141.445 80C141.445 46.0667 113.933 18.5547 80 18.5547C46.0668 18.5547 18.5547 46.0667 18.5547 80C18.5547 113.933 46.0668 141.445 80 141.445C113.933 141.445 141.445 113.933 141.445 80Z"
                                fill="#F4F4F4"
                            />
                        </svg>
                    </div>
                    <div
                        className="col-lg-4 col-md-12 d-flex flex-column justify-content-center align-items-center
                    my-4"
                    >
                        <svg
                            width="303"
                            height="48"
                            viewBox="0 0 303 48"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                            xlink="http://www.w3.org/1999/xlink"
                        >
                            <rect width="302.298" height="48" fill="url(#pattern0)" />
                            <defs>
                                <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
                                    <use
                                        href="#image0"
                                        transform="translate(0 -0.00167975) scale(0.000711238 0.00447928)"
                                    />
                                </pattern>
                                <image
                                    id="image0"
                                    width="1406"
                                    height="224"
                                    href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABX4AAADgCAYAAAC0GH75AAAACXBIWXMAACE4AAAhOAFFljFgAAAFIGlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxNDUgNzkuMTYzNDk5LCAyMDE4LzA4LzEzLTE2OjQwOjIyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgeG1sbnM6cGhvdG9zaG9wPSJodHRwOi8vbnMuYWRvYmUuY29tL3Bob3Rvc2hvcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxOSAoTWFjaW50b3NoKSIgeG1wOkNyZWF0ZURhdGU9IjIwMTktMDQtMTFUMTQ6MTA6MTYtMDc6MDAiIHhtcDpNb2RpZnlEYXRlPSIyMDE5LTA1LTA2VDE4OjQ3OjMwLTA3OjAwIiB4bXA6TWV0YWRhdGFEYXRlPSIyMDE5LTA1LTA2VDE4OjQ3OjMwLTA3OjAwIiBkYzpmb3JtYXQ9ImltYWdlL3BuZyIgcGhvdG9zaG9wOkNvbG9yTW9kZT0iMyIgcGhvdG9zaG9wOklDQ1Byb2ZpbGU9InNSR0IgSUVDNjE5NjYtMi4xIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjg1N2I4YWE0LWQwOTktNGQ3ZC1hMzgyLTJmMmYxNDQ2ZGUzYyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo4NTdiOGFhNC1kMDk5LTRkN2QtYTM4Mi0yZjJmMTQ0NmRlM2MiIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDo4NTdiOGFhNC1kMDk5LTRkN2QtYTM4Mi0yZjJmMTQ0NmRlM2MiPiA8eG1wTU06SGlzdG9yeT4gPHJkZjpTZXE+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJjcmVhdGVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOjg1N2I4YWE0LWQwOTktNGQ3ZC1hMzgyLTJmMmYxNDQ2ZGUzYyIgc3RFdnQ6d2hlbj0iMjAxOS0wNC0xMVQxNDoxMDoxNi0wNzowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTkgKE1hY2ludG9zaCkiLz4gPC9yZGY6U2VxPiA8L3htcE1NOkhpc3Rvcnk+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+JQIE0gAAKpxJREFUeNrt3d2N48gVQOFZRyBsBApBISgEve+LQmAICoEhKASGoBAUgkJQBu1um2P3zs5MS938uXXvN8ABDMMw0MUSq3hIHn57eXn5BgAAAAAAAADIg0EAAADrb0i+fdu8cnjl9MrwymXkPP53e+MEAAAAAMQvAABoQ/juR9H78gD3UQRvjR0AAAAAVBe///rj+MoLmuKScS7++dfL7pWXpJySCqnTgzIK83D5gX48JqdRFr6xsZg3+/vajsf1s/Ojd/z/Np6XmX+PJ+Mc7oZJpvP93e/ZviniPqShsbdvRCROE83rQ/JxsvYtd47cJZ5HZ+L3X3+cidTmOCUVv11i8XtwAYMAkngYj9nBE6Hhf1vHcbP71eN+e9vIGVPil/itceGC8Dfz7sQv8QtMuWd44q2wVhmsIYucI6+Vbx5UEL83IrU59knF75BY/G6IXwRdCC/vZLA76jl/V2/H+WBciV/iNwVa3s5BxC/xC+L3/dzeJLyp9CMH64hr+jn3Tdml75ZEbZCk8/HPv17uSaXv1SKBhriOiQByYZ3fVG/TTPyC+P3dOdrxbXI+Zn0dm/gFAuwZJB/gbZSfZ+80fvV99X31fZeiJ37R8CbrPGYHbLaWyTvMfTx3xC/xS/wSBlh0Lm7G7A7xS/wCs53Hxz175jHrrSneRnkyd7chfvV99X31ffV9iV882Zf0qlXzd9uvNrZkHPGb4qacTrs3OYhf4hfEb5WbTHJH85wXO3OlhvjV99X31ffV9yV+8Zk7qCfiodmnNI7EL/FL/BJu8JV04hfI9eZGgbXvqac4IfFA/Or76vvq++r7Er+Y4Eu77r5PsvFadNNM/BK/xK+P3cBX0olfIF+yJ/lbBpIPEg+zvN2o7wt9X31ffV/iFw9c/BHATb1mtbPBJX6JXx+7gVdoiV8gnfitkHzYWWdCf1ekubmh7wt9X31ffV/iFwRwtjvuJ+NsTIlfTz7BK7TEL5DvI50F1sCrteZLNwbufld1xK++r76vvq++L/GLORMQGsBxL0YvxC/xS/z62A1mm39DlZu91log5p6hQPLBvsv6NNnNAH1f6Pvq++r7Er/4wqbMq8jEL/EL4teTT4Xm3qHSWz7WWiDunqFAZ1zywfr0PX21I371ffV99X31fYlfrPsFXk+kBZNTxC/x67eVis5xD/MK7Y34JX6BIOJ35xyEAomHL++B9H2h76vvq+9L/GKiFqWnf4lf4hfE72xPu8jr2BeRLsQviN9q5yU3PmsnHiZZh/R9oe+r76vv6wIHE76S7LWsEE9i3Ilf4pf4zddWd+xLns+JX+IXxG/l5IMbn3X3RJMde31f6Pvq++r7Er+YfpE+2ohp/BK/cJEzOQfHP+05hvglfkH8ujFlXytBNMPT3vq+0PfV99X3JX5BlM35O1qjt3UmZcxn4jdtU11SZ/n51hWdb8Qv0MieocB1m4dKfn7ce2851RS/+r76vvq++r42EIjC2YZsleZWR/wSv8Rv3p66OeCDOcQv8Qvit9ibCXc3PkslHjbEr76vvq++r74v8Yu2ur+bwpuy4wpjviV+iV8XO6nRUvfBHOKX+AXx++PvYJv8RpXWfY3Ew+QP2en7Qt9X31ffl/gF+ZvpabFL0XEmfonfUudU88A8s54QvyB+C6ZptO4lHsqLX31ffV99X31f4hcuIGv/lvbEL/FLyJWgMxc8TWXdJn5B/Eo+lFufdo5tbfGr76vvq++r70v8QvM35u/pqv1J/IL4dfFrD0T8Er8gfiUf7HeDX1OswWwPr+j7Qt9X31ffd92Lnj3+w+kH+lFkXRI+cXQqfHd+zk149ZYy8Uv8VkTvcD5pYn4Rv0Cze4YCyYe9m5Jkfj3xq++r76vvq+/b4MLiAvPpC9Hvkvjc+N3eA/lL+hK/IH5d/BY9nxC/xC+IXx+n/Bq3anvfxImH2Y+lvi/0faeXvlt9X+KX+F1cgJwau1B9k59b8pf0JX5B/Lr4DTa3juYU8QskEb9Lf2BY8kHiIeQNbH1f6PtOL36P+r7EL/G76nE7jE8E311Qhn56ewpR2RM+xC/xC/OTJCF+HS84J//muiDzWO9cl5P3lcSvvq++bxTxe9b3tcAQv6GeXBpsjMMen+6TkuHm9W7il/glFype/M48r87mEfELZNszJE8+XAusTVk/1rfYsdP3hb7v9OL3pu9L/BK/ITcMfdBNw7b4sdmMgv4jcXkfN+4Hc5r4dbyJX29RmFPmFPEL4tfbDPn3aIm78zviV99X31ffV9+X+MU8G79TsM0fWfFP+fC929yN/9mTfMQvSLpH6MyPT8+pm/lD/AJZ9wySD02/Heg3Qvzq++r76vvq+xK/+NQTwJFeafUUK4hfEL/TfDhT91s7kfgFiN+f/U56yQeJh4rHSt8X+r76vvq+xG91eRLhKaeb4wHiF8TvJJzNkacvrM0b4heoIH43yd9u6Oxtm7hBvSN+9X31ffV99X2JXyy/CYzw9O/R8QDxC+J3Enz8UTuR+AWI32rr5z3Lt0PG73+Q88Svvq++r76vvi/xi8k3GGu+TuSpXxC/cOHqfKp5SfwSvyB+JR/Kfjsk8cf4Vjs2+r7Q99X31fclfvH/Y75beaPhqV8QvyB+zVkX1gQK8QvnX8mHksmH13+Dp7GJX31ffV99X31f4hdZ5e/VMQDxC+J3MrbmS8kn3ohfgPitnnzYNLo2Hch44lffV99X31ffl/hFdvm7cwxA/IL49cqr+WP+EL8gftu+1luRwZsojkUW8avvq++r76vvS/wim/w9G38QvyDuJHRmnD9X84L4BewZ0p8PD42tTeeExyDE09f6vtD31ffV9yV+EUuu3I09iF8Qv1559YQb8Uv8gvhd5EEP6599TWr5ru8LfV99X31f4hfxLpIPxh7EL4hfb1JMPG+2PuhG/ALEb6kbYucGxj/rx/bC5Db0faHvq++r70v8It5rYL1xB/EL4ndS9ubN7OcJ4pf4BZrcMyRPPuyDj33Gj42Getpa3xf6vvq++r7EL+K9BnYz7iB+QfxOSvWb2QdzgPgFiN+SyYdb1ORD4v1MKNmu7wt9X31ffV/iFzHvRm+NO4hfEL/msddoiV/iF861i/yOusTHpLc2FR5rfV/o++r76vsSv3h4c7JkG/Fo3EH8gvid/NXLbcH50jv2xG/w3+UFqTjaT3kKtWhbOeTT1fq+0PfV99X3JX4Rc4Oi8wviF8RvYZnn9WVzpYj4vTivw0cwayWPEq9NIR921PeFvq++r74v8YvnNoMuhED8gvhtm0OhuXJ1vIlf4wxIPkTazyVdm8L6E31f6Pvq++r7Er94bl4MS736aLxB/IL4zf+1bfKCkCR+ndch+bAgO4mHWh+Q1feFvq++r74v8Yvn5sWSX0bfGHMQvyB+5XS8rkxIEr+Ac2jrkjJx4mFH/Or7Qt9X35f4JX5zzY3SnSgQvyB+tfi8nUL8Er/EL5KvtcfE57dO4qHOHlnfF/q++r76vsQv4l5QE78gfkH8Fn01s5E3U4hf4pf4hT1/m8mjrfxQjX2Evi/0ffV99X2JX8TduJBtIH5B/CZ86mnGubF55ea4Er/GGZjsnHp3jpPO+IU83xG/+r7Q99X3JX6JX9KFbAPxC+LXU08+mkOKEL/EL7xFUfLmZ9KP5TVz41jfF/q++r76vsQv4l5kkW0gfkH8zs+QZF7sHEvi1zgDkg9P3vzczDx2nfWB+H1G/B6IVH1ffV99X+IXQebHjfgF8QviNw0H5wEQv8YZKJh8GGYct6yJhy3xO5/47YlUfV99X31f4heFLrDJNhC/IH6X4Tb3U0+eqCIkiV/iF5IPlW5+SjwQv58Rv1ciVd9X31ffl/gF8QsQv8QvMTcDfaPzIfOTaIQk8QtE+q31kg+lRXmTaaiWpO+GRNX31ffV9yV+UewDOmQbiF8Qv8uy054E8Wucgd/caLslPecNE4/TnRwnfvV99X31ffV9iV/jbH6QbSB+QfxG4mougPg1zkDR8+5+ojHKeEOy2SSmvi/0ffV99X2JXxC/GTbJFxfZaOWCyEWnfp8nzwhJaxLxC8mHjL17iQfiV99X31ffV9+X2CN+zQ/il/gF8Uv8ep2zjXWI+CV+jTNQ68Zb/8VxkXggfvV99X31ffV9iT3i1/wgfolfEL/Er6d7vjAHto4RIUn8AtbiSPud139nY0H86vvq++r76vsSe8Sv+fE4R5tj4hc2/8Svi70Vut4gfo0zUPfNi5u9ydeefiZ+9X31ffV99X0t5MRv+/NjcIeZ+AURSPym5sutw5mO/9GxISSJXyDM7++a9Bx4emIMMqYvQu4BMotffV99X31ffV/iFxWfttoRv8QviF/it40L3wW7knfHhZAkfoEwv79d4vPg7sEx6O35iF9933pskopffV/il/jFYnf3ddCIX7gIIH7bufBd6NifHQ9CkvgFJB8W4lp0T5Iqg6nvizm4JpW++r4WcGIPS15k3Yhf4hfEL/FLTDnuhCTxC3goJOKbL0kTD+l8lr4v5qBPKn71fYlf4hdLfk39QvwSvyB+CcAwdAGO+81xICSJX0DyYWHe8kLbQk8674hffV98zCGp+NX3JX6JX3yfG4dqXUnil/gF8Vuc+5ofeUn8GjEhaU0ifpFpv9tVOR8mFd2nlPNS3xf6vvq+FmniF0/PjSU+YHAgfolfEL/EbyjOid8yAfFrnIF2PgC9+psvCdMWab2Ivi/0ffV99X2JX8RseG2JX+IXxC/xaw4klgiEpDWJ+EXWJNw9c/Ih4Vso94yJh1bEr76vvq++r74v8Yto82Ljw27ELyFC+hG/ZbktfKwPxpyQJH4ByYcgDEkTD13q+ajvC31ffV+LM/GLp+bFMevrxMQv8QviF3EagOONxrvxJiSJX0DyIdINUOd64lffV99X31ffl/glfjPPi0Hfl/glRIhf4rc82wWOc2+cyQDiF5B8wPz5CuJX3xf6vvq+xC/xiyU/sLMhfolfEL/Eb11Z5RgTksQvkGL/K9cj8UD86vvq++r76vsSvzAn/t7OSjp2xC+IX1IwG8fGPyIK4tc4AzneFoRrribFr76vvq++7/xsLcbEL8K1Fo/EL/EL4pf4beYV0U2jNxlB/BpnQK/d+k386vtC3zdx3/dmISZ+Ee5C/J54/IhfEL/Eb0bOEx9bTUhCkvgFJB/geyrpxa++r76vvu/8nC3CxC/C3anviV/iF8Qv8Vt3XiT7CvxASFqTiF9A8kHigfjV99X31fddh6MFmPhFuC+rb4lf4hfEbwHxe0k2L64THddMT4Tdx5umhKQ1ifgFJB8kHohffV99X31ffV/iF2WF5SyvCxO/i/w9kZj7Q1DngH/zGmyI32luciW8+D1NIARu2V6jJSSJX+IX8MaOG/jEr76vvq++r74v8YvId+aXuhDf2ui6cAz8evjJOcGF45Rr3+u/LuFTRNsvHNM+0VgMhCTxa/0GSpzvW6IvO+f0faHvq+8L4he/nANnT/sSv8Qv8Uv8zrP2JUw+XD55PHdZX6MlJIlf6zeQ/g2PFrhVTDxEFr+Z+76XpHT6vvq+xC/xm/D4L/k02paocuFI/KKg+M2YfDh84nheE/39R0KS+LV+A5IPEg/Eb8W+r1fr9X31fYlf4redY39spQtpg+vCkfh1wdiq+E2cfNg8cSy7zE88E5LEr/UbkHyQeCB+q/R9vVqv76vvS/wSv6Rv2deOiF/iF8Tvr9a+hMmH/sHjmOmJ5582jglJ4tf6DUg+rMjVXIsnfjP3fb1ar++r70v8Er+kb9nXjohf4hfE72/Eb8bkw/6B4zgk+ns7QpL4tX4DpRvvEdmZZ/HEb+a+r1fr9X31fYlf4pf0LfvaEfFL/IL4/d3at8SaG+kpo7cWcIWP2hGSxK/1G1j/mrMo9q5Bxa++L/R99X2JX6xxrM9LC4FqX5YlfolfEL8frX3JPnL2u6dgM73ee//dB0oJSeL3wT3RHqviqcj1f8NXolbiIb/41feFvq++L/GLdV6vuq5wkbwjqohf4hfEb/pXXn/VvT1ll9vEL/ELcx2SDyuuvW5mBBW/+r7Q99X3JX6x9PFdoyl5JKpctBC/IH7LvPI6JL64vyQXksQviF9IPiS7IUn86vvq+0LfF8RvDWGy1utUJ6LKRQvxC+K33CuvhwV/qyESD8Qv8QtzHZIP5jLxq+8LfV99X+LXOC95PLcrX3CfiSobPeIXxO9D4jfbK6+3sevbVXuiikAgfmGu4+nkw92cnO+GJPGr76vvC31fEL85Bcmw8kbk7DgQv8QviN/H176Er7yeE13MX4oISeIXxC/W+D135qTEQ0bxq+8LfV99X+IXUx67tyerjkFel/LbI36JXxC/n1j7vPIalh3xS/yC+EXT+7psDOZNfPGr7wt9X31f4hdTHLNDsKeqSF/il/gF8ft58esr5/E4FRKSxC+IX6yZqJN8eDzxsDFv4otffV/o++r7Er/4bAerC5ByIMqIX+IXxO8Ma5+vnIfiWkxIEr8gfiH50NAHVBFV/Or7Qt9X35f4xeN3vvfjcRmC3wX3hD3xS/yC+J1o7ZN8aC/xQPwSvzDXIfkg8UD86vtC31ffl/g1zj9K3f2Yazi9E7wtbXjun7kwJqpctBC/IH7z/r0VEw/EL/ELcx2SDxIPxK++L/R99X3Li1/k2jTbfBC/xC+I33luer7+660z7SQeiF/iF+Y6Jv2GiXn6T/bmRzviV98X+r76vsQvWqfzmyF+iV8Qv7OK380rN+tNG4kH4pf4hbmOSX/jg3n6N3rzohXxq+8LfV99X+IX7T/lK+1A/BK/IH4XyBxJPrT3oVIyjPiFuY5JfuMbyYf/cfOWZVviV98X+r76vsQvWm1KecqX+CV+Qfwu3LeXfGgj8UD8Er8w1yH5IPFA/Or7Qt9X35f4RYuc3WUmfolfEL+riV/JhwYSD8Qv8QtzHZIPEg/Er74v9H31fYlfyDoQVS5aiF8Qv5IPyRIPxC/xC3Mdkg/R3kIhfvV99X31ffV9QfziV8LXK0XEL/FrHhO/QcSv5EM7/UQyjPiFuQ57gChvoRC/+r5Tovuo76vvS/yC8AXxS/zCRd984lfyoYF+IhlG/MJcxyy/+Wo3P+1DGxW/mfu+7kTo++r7Er9o/2mn7hWpFOKX+LXhJn4Dil/Jhzb6iWQY8QtzHbMlH6rc/JR4aFj8Zu373k0sfV99X+IXuTYbJDDxS/wSv8RvPPE7jsXZOhUv8UD8Er8w12EvMBEeZmtS/Obu+w4mlr6vvi/xi9QS+Dj1hbHNqYsW4hfErw/dZEo8EL/EL8x1zP6733niF5HFr74v9H31fYlftM5ZA5j4JX5B/K4rfsfxOFiTYiUeiF/iF+Y6Zv/dX61RiCx+9X2h76vvS/wi06uxfm/EL/EL4ncl8TuOyWA9ipN4IH6JX5jrcJ0a/a0U4lffV99X31ff14JK/OIZ7uOckYEgfolfEL/Li1/Jh4AX02QY8QtzHRIP0W9SEr/6vvq++r76vsQvQAATv8SveUz8Bha/kg8xX58lw4hfmOuY/Cbn1ZqF6OJX3xf6vvq+xC8IYKLKRQvxC+JX8iH501NkGPELcx2T/tZ781XyoQXxq+8LfV99X+IX1QTw0e+J+CV+QfwuIn4lHx7nQEgSvwDxa+1305L41ffV99X31fclfoGv8/Za1s5m1UUL8Qvid/bxkXz4mIGQJH4Be6imEg83c3XZNYz41ffV99X31fclfoFPtakq3qUmfolfEL9LiV/Jh4feRNkQksQvYA/VzG/8bJ4u/9YK8avvq++r76vvS/wCX3lFaU9UuWghfkH8Sj5kvlgmw4wzzHV4i6X1m5jEr76vvq++r74v8QuQacQv8QviN5D4HcfpaJ1Z9/VYMsw4w1yHm5iSD7XEr74voojfXt8XxC+ibLYr3K0mfolfEL9Li9+FfhuejiJ+iV8Qv5AtknwgfvV9EUz8XvV9EUH8Guffjv92lBvvOb3jMnJPcjG+I6pctBC/IH5nWUs8LbXSxTEZZpxhrkPiYYHrKA+/BRG/+r6IIn03+r4gflMer924QTqNd8evDW5ajkSVixbiF8Tv5OPVuTBe53VYMsw4w1yHm5bmcB3xq++LKOL3oO8L4recIOlGGdzCBupIVNnwEb8gfiUfMnwAh0gwzjDXYc1aCA9kBhC/+r7Q99X3JX6J3yhPBnfBN1QnospFC/EL4tfTU633D8kw4wxzHd5SkXyoIH71faHvq+9L/BK/MY/xZvzq+0D+Er/EL/Hr95RX/Ba+mB4ISeIXsIeSeDCXiV99X31ffV99XxC/Nlr/PeY32Qfil/glfonfnGtfsddnV3/6iUAwzjDXYY2SfKghfvV9oe+r70v8Er8tHftjoI3Xkahy0UL8gvj1NFWLF79kmHGGuY44156F4OlWEL/6vtD31fe1+BK/rcqVi80L8Uv8Er/Eb561r0jy4UJIEr+APVRT3yAxF6fjal4tKX71faHvq+9L/BK/OZ4AXjMBsdoX2Ylf4pf4JX4zrn3JX6cN84EbMsw4w1zHQ7/hq3lo79qy+NX3hb6vvi/xS/xm+RDcyaac+CV+QfymEL+Zn67qCEniF7CHcs0JyYelxK++L/R99X0twsRvtlexbu5cE7/EL4jftte+pBfbF0KS+G2M2/hbxHq4lrSeSz4Qv/q++r76vvq+xC/xix+e/j27c038Er9wodj22pfs9dowiQfi1zhbv4GHridu5Ozs9MSvvq++r76vvi+IX7T0gaALUeXCkfglfolfyYfIiQdC0jhbv4EPf7c9KbsYe+JX31ffV99X3xfELz47T44rbF6ORJULR+KX+CV+JR+inwONqXG2fgMSD0GSMhviV99X31ffV98XxC9akb/31jYvxC/xCxeMkde+xpMP4RIPhKRxtn4DEg+SDzXEr74v9H31fYlf4pf8JeKIX+IXxG908dty8qEjJIlf4ww083sdSFjJhxziV98X+r76vsQv8WvOeOqX+CV+Qfw2svY1mny4EpLWFeMMNPNbPZCvkg+ZxK++b3ty9JT4qVh9XxC/yHgXvyOqXDgSv8Qv8Vs6+bAjJK0rxhloJvFwJ19X50z86vuW7fv++dfLhUjV9wXxi6a6XTeiyoUj8Uv8Er9lkw8nQtK6YpwBD4fgaQ7Er75vyb4viarvC+IXTUqZgzFx4Uj8OscQv+WSD1dCkpA0zkAzv9GObA33UdQN8avvW6rv++dfL3sSVd8XxC+avKs/EFUuHIlf4pf4bfbNjZSJB0LSOFu/gf/9PrcSDyEZiF99X31f6PtapIlffHWTt9TGZUNUEb/EL/FL/JYZ4xMhSUgaZ8AeDZIPa4pffV99X+j7Er/Erzn07dt5oU3LkUQhfokm4pf4nXyce4kHQtI4G2dA4kHygfjV99X3hb4v8Uv8Ys2nfgeiivglfolf4rdE8mFH/BKSxhlo4ne5k3hwDsopfvV99X2h70v8Er9YuvV7J6qIX+KX+CV+04/1qcG5SgYYZ+s3qq7V14RPx96Syt+O+NX31feFvi/xS/zis/Po6Ckw4pf4BfHb7toXJPlwbXSuEpLG2foN15FJerit7z8+kNpb4vdx8XvS99X3hb6vBZv4xeIXZB3xS/wSv8Qv8Zs2+bC3/hGSxhloJvGQTYwOC7/N6FwUXPxe9H31faHvS/wSv1g89zAQv8Qv8Uv8Er8px7x345MEMM6AxEOED6CNN0Oztos74vcx8avvq+8LfV/il/jF3+fSEl/0vRK/xC/xS/wSv+mSD7eWvzhOSBpn6zeKrc99Qhl6+MnfeUicfNgRv7+Xvnt9X31f6PsSv8Qv1nnli/glfolf4pf4TZd82Dc+VwlJ42z9hrU54e8zcfLhSvzq++r7Qt+X+CV+EfWibEf8Er/Er4tL4nfWsV/yKafe2kdIGmfAjcGIHzxLnnw4Eb/6vvq+0PclfolfROx97Ylf4tcGmPglflN025tOPBCSxtn6DYmH/L3bxMmH0A/UrC1+9X31faHvS/wSv1hHzIWWc8Qv8QviN5H4XeIpp32SuUpIGmfrN7KvyRnl5+WJv1/yoYz41ffV94W+L/FL/GLV+UT8Er/EL/FL/Ka40O8TzVVC0jhbv5E98ZAtd/DbxMNPxmAr+VBH/Or76vtC35eoI35B/BK/xC+I3xJr30xPOaVIPBCSxtn6jSLrccanXbtPjEOXOPmwJ371ffV9oe9L1BG/iCJ+L8Qv8Uv8Er/Eb9NPeu2TzVVC0jhbvyHxUOD3uFDWbg2avyGr76vvC31fEL8gfolf4pf4JX6J3/Uv/PuEc5WQNM7Wb0g8JEw8FEs+9MSvvq++L/R9iTriF8Qv8Uv8gvgtuPZN9KrvPVPigZA0ztZv2HflTzxIPtQSv/q++r7Q9yXqXPyC+CV+iV8QvxXF7xRPfh2SzlVC0jhbv5HtvJZRbl5I8bzJB33fun3fO5Gq7wviF8Qv8Uv8Er/EL/G7evJhSDxXiRDjbP1GpnNa1pzBduIxyvrU77my+NX3bUv67khUfV8QvyB+iV/il/glfonf1ZMPKRMPhKRxtn7DfqsZTo1e96zFoZ741fdtUfx2RKq+L4hfEL/ErwsR4pf4JX5XfxLskHyuEpLG2foNe/m4XGccr2tS8dvcDVt935p934FI1fcF8YuUTwgQv+YX8Uv8Er/ttB+HAnOVkDTO1m9kOJftkkrMnTHLn2jS99X3hb4viF/kEb898Wt+Eb/EL/HbxO8xdeKBkDTO1m8kO5dlfHr1lOF6WvJhGfGr76vvC31f4tfFL2JclJ2IX+KX+CV+id8mkg8Hax8haZyt33BNmC3xIPmQTfzq++r7Qt/XIu/iF5FedSJ+iV/il/glfuMnH4ZCc5WQNM7Wb9jDF0o8FEs+XCqIX31ffV/o+xK/Ln7x8Vw6Vn/liPglfkH8Vlv7fvG7LJF4ICSNs/UbCc5fm1duEg+TjGWfWP522cWvvq++L/R9iV8Xv/h4Lp0X2njsiV/il/glfonf0MmHY7G5SkgaZ+s3Wj1/ZZSVVxJ9luTDNrP41ffV94W+L/Hr4hcfz6X7QhuPDfFL/BK/xC/xGzb5cCk4VwlJ42z9hnW2YOKh0JiGP4/p++r7Qt8XxC9ydK3uxTfQxO+842s9ckFq7fvaWx+X6E8EEZLEr3EGUj+d2nuKumbyQd9X3xex6Sy+xC9kHlLcaSZ+Wxe/LsyJX2sfCEnjbH2BvXub3CK8GVgg+bDLJn71ffV9MT87iy/xi+afGLhXuYtP/BK/IH6tfSAkjbP1BQ2fsw5JpeTeHiZvQ3lO8avvq++LeblbfIlfND+HjgtuNA7Er6dTnK+IX+IXhCTxa5yB8A9slEo8FHmqOux3MfR99X0Rl8ECTPyi+Tm05KtMW+LX+arqxwOJX+IXhCTxa5yBL/6OBokHkr31j+hNKX71ffV9oe9LpLj4RZynfW9EFfFb6TVB5xfiF4QkIWmcgUb37SUTD4WyGuGSD/q++r7Q9wXxi/Y/XNATVeXFb1f5a8XEL/ELQpL4Nc7AJ38/W4kHT1lnTj7o++r7Qt8XxC/afPqymb4v8ZtGBJ79volfax8ISeMMZDrXL/CBXIkHyYfGxK++r74v9H3JOxe/+N282S28obgTVcTvQvPu5jdO/Fr7QPwaZyDLuX6hN6YkHuomH0IIeH1ffV/o+4L4RbuJh2aewiR+01z4b/3WiV9rH4hf4wy0fq6XeJB8qHI89H31faHvC+IX082Z8wqbiQNRRfyOY7zETQc3Jolfax+IX+MMZBC/14Tjfm8h8VAw+bBvTfzq++r7Qt+X+HXxi/W7vk29ek/8pmnUSRERv9Y+EL/GGWj6XL/Svt0DITWzG6snH/R99X2h7wviF1+fK8fqX4slfktdxMg9EL/WPhC/xhlo8ly/wvc4lmJIsGZcEv8m+lbEr76vvi/0fUkUF7+IIX2bepWL+E01F61TxK+1D8SvcQZaFb8SD7rLpZ7I1vfV94W+L4hftCd9m/moG/G7+GZZYoT4JQNASBK/gHP9z38rPaEo+VBN0Ov76vtC3xfEL9r5kFuzr9sTv4uN81JPSRyNN/Fr7QPxa5yBVs71ra+dlb6/kDz5MMQVv/q++r7Q9yV+Xfzi+1dn196M9EQV8fuLcR489Uv8kgEgJIlfwLn+H/v3mydIJR8qPqGt76vvC31fEL94TrbcbfCIX/kRT/0Sv9Y+EL/GGWhG/Eo8uBYvK+z1ffV9oe8L4hePPSUQZcPYEVXEb4DOb9qnTPyeiF8QksQvkOdcL/HQ9BpyTfwbGSKKX31ffV/o+xK/Ln6rPkEZ5VWjK1FF/AbbJEsUuYi19oH4Nc5AyHP9+PDG3ROjza4hu+S/ky6O+NX31feFvi/x6+K3pliJdpd5R1QRvw+M9dJfQ5Z8IH6tfSB+jTMQUfwOScf36Jo8jcDfRhG/+r76vtD3JX5d/FZ6wjfil2RPRBXxGzD38H3TKlVE/Fr7QPwaZyDMuf6tf5t0bC8F15LMyYdLFPGr76vvC31f4tfFb3ZRdgr8td8rUWUTHHyDTP4Sv9Y+EL/GGQhxrk+eeNgWXEskHxYQv/q++r7Q9yV+Xfxm/GDbsYFXwFJs8IjfVZ5cfyF/iV8yAIQk8QsUFL8DQejanNB/VPzq++r7Qt/X4uLiN9eTvV1jm8E9UUX8NvSkC/lL/Fr7QPwaZ+Blxd9Dl3RML/a2Yd/ODP2Gp76vvi/0fUH8Zn8t6G3zd250o3AkqmyGG30yoksyhofx/HEifq19IH6NMxD7XD8+5CHx4MZ3q5zWEr/6vvq+0PclTVz8trAJOIzH7JzkAwBnGzXit7GPvP3jmLR4kfJO9t6jflyR+AUhSfwSv3Cu/+lv4ZJ0PD0o9v9j3Cf/7ezWEL/6vvq+0Pclfme8q4enGMYN3SX5qz5nd+iJ34nG/Rzh6YW31/OCC/Lvve/7kk9gEL8gfolf4wziV+LBHlfyYc7kg76vvi/0fVEzIA/Sl/j11O/Sryie1+7/jhcL+3c3kh69cCB+iV8Qv8YZCHquH/NuPvol+SD58Anxq++r7wt9X+IXIH2JX+eyKbmNr+nt53oSePz//i54+wneEiB+iV8Qv8YZiCt+r0nHkSuQfJhd/Or76vtC35csAUhf4rf9V+Iif+jkNorZ91mX/Q/8Kv9yWSj/QvwSvyB+jTMQ8Fyf+Jrtah1pen87xf54s4T4zfq07yWp9NX31feFTQTczSd+8asPlvmtEb/ELwhJ4hdIc65PnHiY5SNf9rfN0c8rfnP3fU9Jxa++r74viF+01+06aHERvwsdg8FvjvglfkFIEr9AhnP9+MTn1b7D/jb5b2k/p/jN3PfdJxW/+r76viB+0dDrW5Xu5BO/XolzAUb8Er/EL/FrnEH8arxKPNjfLpt8qNj3TbvZ1PfV9wXxi3Ze25nro1bELyQfiF/iF4Qk8QtkPte3vg5KPNjfLpl80PfV94W+L4hfSDsQv76CDOKX+AUhSfwC4c/14xOeN/sNFEs+HKYVv/q++r7Q9wXxi6k5V3vKl/i1OXYhRvwSv8Qv8WucQfxOPN/PEg8omHy4f/ZaUt9X3xf6viB+MX+Xae/3RPwG3Bxf/T6JX+IXhCTxC7Ryrk/+Sr/Ew9fnR5f8dzVMKX71ffV9oe9r4SB+8fW7sm7wEL/kL/FL/BK/xC/xa5xB/HqiU+JhmfPhRfLhMfGr76vvC31fiwbxi88L31PlrAPxS/66ICN+iV8Qv8YZxK9MlcTDCufDreTDR+JX31ffF/q+IH5B+BK/5C+IX+IXxK9xBgKc65MnHvbWC8mHOZMP+r76vtD3BfGLrzd8O8KX+E0gfy9+z8Qv8QtCkvgFIp3rkz/B2VsrJB8+SfcV8avvq+8LfV8Qv3jgTutnGkvErwv04Mfp7LdN/BK/ICSJXyCQ+M0q8G4eHJF8+OLbptvPil99X31f6PuC+MWvN2inRxdZEL+NHqtj8o0y8Uv8gpAkfoEGzvXJX9mXeJB8WGSd0vfV94W+L4hffCx7+1c8qU/8VntKQvqB+CV+QfwaZ2CVc73EAyY6P2b/jkX3rPjV99X3hb4viF/8V3h1ZC/x67j953fg6d8nOmrEL/EL4tc4A5OIX4kHTHF+3BX4wPj2GfGr76vvC31fEL9VRW+v2Uv84pcffjsXPjfsC/6eiF8QksQvsNq5Pvl1mMSD6/qpuT4jfvV99X2h7wviNzvXUWJ1Nl7EL55+5bKCAL6PN4K2hX9PxC8ISeIXWOVcn/wJTYkHyYfFs2T6vvq+0PcF8ZtV3FxGSfV2HA+yDcQvJhXAfbIExH08Xxz8nohfEJLEL7Cq+M0q6CQeJB/mZveR+D2OqYeM7JKK39MrFzTF0Um3uS/bXxCSfhS6p+9P7o5szd3Qm61Z54RxXiUB8XaeHLS80/2eZsXvp9R5oum9hnEGlj3Xjw9rZB0TbxrG+HZF6mvkj1MPAAAAwNckcMQngd8nXjz9DwAAgBr7dIMAAACAGZ5I7UbZutQrmzeJFwAAAID4BQAAwLIyeDsmWY7vUi39A6+tDe/+96d3Uvd74mVjfAEAAIB/8m++76CaXgUdOQAAAABJRU5ErkJggg=="
                                />
                            </defs>
                        </svg>
                        <p>Capital en la cuenta de bitmex igual o mayor a 0.25 btc .</p>
                    </div>
                </div>
            </div>

            <div id="routesheet-section" className="bitcoinbot">
                <div className="container">
                    <div className="row">
                        <div className="col-12 text-center mt-4 mb-5">
                            <h2 className="number-stadistics text-white">¿Cómo empezar?</h2>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-3 d-flex justify-content-center route-card-container">
                            <div className="route-card text-center">
                                <div className="route-card-code">
                                    <b>01</b>
                                </div>
                                <div className="route-card-title">Crea una cuenta Bitmex</div>
                                <div className="route-card-body">Debe ser una cuenta verificada.</div>
                            </div>
                        </div>
                        <div className="col-md-3 d-flex justify-content-center route-card-container">
                            <div className="route-card text-center">
                                <div className="route-card-code">
                                    <b>02</b>
                                </div>
                                <div className="route-card-title">En Bitmex, genera las API Keys</div>
                                <div className="route-card-body">Sigue las instrucciones de la plataforma.</div>
                            </div>
                        </div>
                        <div className="col-md-3 d-flex justify-content-center route-card-container">
                            <div className="route-card text-center">
                                <div className="route-card-code">
                                    <b>03</b>
                                </div>
                                <div className="route-card-title">Contáctanos y comparte las API keys</div>
                                <div className="route-card-body">Escríbenos a Soporte ADN</div>
                            </div>
                        </div>
                        <div className="col-md-3 d-flex justify-content-center route-card-container">
                            <div className="route-card text-center">
                                <div className="route-card-code">
                                    <b>04</b>
                                </div>
                                <div className="route-card-title">En 24 horas seras conectado</div>
                                <div className="route-card-body">De acuerdo al número de solicitudes.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="referers-bitcoinbot-section" className="container">
                <div className="row">
                    <div className="col-lg-6 col-md-12 mb-5">
                        <h2>Referidos UNINIVEL</h2>
                        <p>
                            El plan de referidos te permite incrementar tus ganancias y disfrutar de más tiempo para
                            invertir.
                        </p>
                        <div className="refers-bitcoinbot-feature">
                            Ganar 35% del pago de la licencia de la persona.
                        </div>
                        <div className="refers-bitcoinbot-feature">
                            3% pasivo por siempre de las ganancias de la persona en el servicio.
                        </div>
                    </div>
                    <div className="col-lg-6 col-md-12">
                        <img src={`/images/bitcoinbot-arrow.png`} className="img-fluid"
                        width="700px" />
                    </div>
                </div>
            </div>
        </Panel>
    )
}
