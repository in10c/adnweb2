import React, { useContext, useState } from "react"
import { UserContext } from "../../Context/UserContext"
import Panel from "../../Panel"

export default function LinkReferido() {
    const { user } = useContext(UserContext)

    const [copied, setCopied] = useState(false)

    const copy = () => {
        navigator.clipboard.writeText(`${window.location.origin}/registrarReferido/${user.userID}`).then(function () {
            setCopied(true)
        })
        setTimeout(() => {setCopied(false)}, 3000)
    }

    return (
        <Panel title="Dashboard">
            <div className="content-body">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12">
                            <div className="card">
                                <div className="card-header d-block">
                                    <h4 className="card-title" style={{ textTransform: "none" }}>
                                        Tu link de referidos es:{" "}
                                    </h4>
                                </div>
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="alert alert-primary notification text-center">
                                                <p className="notificaiton-title mb-2">
                                                    <strong>
                                                        {window.location.origin}/signupRefered/{user.userID}
                                                    </strong>
                                                </p>
                                                <button className="btn btn-primary btn-sm" onClick={copy}>Copiar al portapapeles</button>
                                                {copied && (
                                                    <p className="notificaiton-title mt-3">Link copiado al portapapeles</p>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Panel>
    )
}
