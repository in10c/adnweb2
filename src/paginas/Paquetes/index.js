import React, { useContext, useState, useEffect } from "react"
import { NavLink } from "react-router-dom"
import MultiSwitch from "react-multi-switch-toggle"
import { useTranslation } from "react-i18next"
import * as FirebaseMngr from "../../Utilities/FirebaseMngr"
import Panel from "../../Panel"
import { UserContext } from "../../Context/UserContext"
import "./index.css"

export default function Paquetes() {
    const { t } = useTranslation()
    const { user } = useContext(UserContext)
    const [packages, setPackages] = useState([])
    const [tipoPlan, setTipoPlan] = useState(0)
    const porcentajes = [null, "15%", "25%", "35%"]
    const firebaseDatabase = FirebaseMngr.get()
    useEffect(() => {
        const getInfo = async () => {
            const snapshot = await firebaseDatabase.ref("packages").once("value")
            setPackages(snapshot.val())
        }
        getInfo()
    }, [])

    const obtenerPrecio = (packageItem) => {
        if (packageItem.free) {
            return t("Pages.Package-request.packages.types.free")
        }
        switch (tipoPlan) {
            case 0:
                return packageItem.price + " €"
            case 1:
                return packageItem.trimPrice + " €"
            case 2:
                return packageItem.semPrice + " €"
            case 3:
                return packageItem.anualPrice + " €"
            default:
                break
        }
    }
    const obtenerDescuento = (packageItem) => {
        if (packageItem.free) {
            return ""
        }
        switch (tipoPlan) {
            case 0:
                return ""
            case 1:
                return packageItem.trimDisc + " €"
            case 2:
                return packageItem.semDisc + " €"
            case 3:
                return packageItem.anualDisc + " €"
            default:
                break
        }
    }

    return (
        <Panel smallBackground={true} title="Planes disponibles">
            <div className="container">
                <div className="row d-flex justify-content-center mb-5 pb-4">
                    <MultiSwitch
                        texts={[
                            t("Pages.Package-request.packages.types.monthly"),
                            t("Pages.Package-request.packages.types.quarterly"),
                            t("Pages.Package-request.packages.types.biannual"),
                            t("Pages.Package-request.packages.types.annual"),
                        ]}
                        selectedSwitch={tipoPlan}
                        bgColor={"white"}
                        onToggleCallback={(selectedItem) => {
                            setTipoPlan(selectedItem)
                        }}
                        fontColor={"#9B5BC1"}
                        selectedSwitchColor="#9B5BC1"
                        selectedFontColor="white"
                        eachSwitchWidth={150}
                        height={"35px"}
                        fontSize={"16px"}
                        borderWidth={0}
                    ></MultiSwitch>
                </div>
                <div className="row d-flex justify-content-around mb-5 pb-4">
                    {Object.keys(packages).map((index, i) => {
                        const packageItem = packages[index]
                        let _precio = obtenerPrecio(packageItem)
                        let _descuento = obtenerDescuento(packageItem)
                        let _antiguo = parseInt(_precio) + parseInt(_descuento)
                        return (
                            <div className="col-xl-3 col-lg-6 col-md-6 col-sm-12" key={i}>
                                <div className="package text-center mt-3">
                                    <h4 className="package-title">{packageItem.name}</h4>
                                    <p className="package-price">
                                        <span className="price-discount">{_precio}</span>
                                        {_descuento && (
                                            <span className="price-real">
                                                <s style={{ textDecorationThickness: 0 }}>
                                                    {_antiguo + " € (-" + _descuento + ")"}
                                                </s>
                                            </span>
                                        )}
                                    </p>
                                    <div className={`border-recomended ${packageItem.recomended ? "recomended" : ""}`}>
                                        <div className="package-features-box">
                                            {!packageItem.free && porcentajes[tipoPlan] && (
                                                <div className="package-discount-container">
                                                    <svg
                                                        width="82"
                                                        height="66"
                                                        viewBox="0 0 82 66"
                                                        fill="none"
                                                        xmlns="http://www.w3.org/2000/svg"
                                                    >
                                                        <path d="M82 0H0V66L42 53L82 65.5V0Z" fill="#9B5BC1" />
                                                    </svg>

                                                    <div className="package-discount">
                                                        {" "}
                                                        <span className="package-discount-number">
                                                            {porcentajes[tipoPlan]}
                                                        </span>{" "}
                                                        <span>
                                                            {t("Pages.Package-request.chackout-info.price.discount")}
                                                        </span>
                                                    </div>
                                                </div>
                                            )}

                                            <ul
                                                className="package-features"
                                                style={{ marginTop: porcentajes[tipoPlan] ? 40 : 0 }}
                                            >
                                                {packageItem.features.map((feature, i) => (
                                                    <React.Fragment>
                                                        <li>{t(`Pages.Packages.packages-features.${feature}`)}</li>
                                                        {packageItem.features.length - 1 !== i ? (
                                                            <li>
                                                                <hr style={{ width: "150px" }} />
                                                            </li>
                                                        ) : (
                                                            <div></div>
                                                        )}
                                                    </React.Fragment>
                                                ))}
                                            </ul>
                                            {packageItem.recomended ? (
                                                <div className="package-recomended">
                                                    {t("Pages.Packages.recommended")}
                                                </div>
                                            ) : (
                                                <div></div>
                                            )}
                                        </div>
                                    </div>
                                    {user.licenciaADN == index ? (
                                        <div className="package-current">{t("Pages.Packages.active")}</div>
                                    ) : user.licenciaADN > index ? (
                                        <div className="package-current">{t("Pages.Packages.downgrade")}</div>
                                    ) : (
                                        <NavLink
                                            className="package-select-button"
                                            to={`/packageRequest/${index}/${tipoPlan}`}
                                        >
                                            {t("Pages.Packages.select")}
                                        </NavLink>
                                    )}
                                </div>
                            </div>
                        )
                    })}
                </div>
                <div className="row">
                    <div className="col-12 d-flex justify-content-center">
                        <p className="services-text">{t("Pages.Packages.benefits")}</p>
                    </div>
                </div>
            </div>
        </Panel>
    )
}
