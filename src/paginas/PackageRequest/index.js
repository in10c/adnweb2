import React, { useContext, useState, useEffect } from "react"
import { NavLink, useParams, Redirect } from "react-router-dom"
import Panel from "../../Panel"
import { useToasts } from "react-toast-notifications"
import Loader from "react-loader-spinner"
import * as ccxt from "ccxt"
import Axios from "axios"
import { useTranslation } from "react-i18next"
import * as FirebaseMngr from "../../Utilities/FirebaseMngr"
import { UserContext } from "../../Context/UserContext"
import Constants from "../../Utilities/Constants"
import "./index.css"

export default function PackageRequest() {
    const { t } = useTranslation()
    const { addToast } = useToasts()
    const firebaseDatabase = FirebaseMngr.get()
    const { packageID, tipoPlan } = useParams()
    const { user } = useContext(UserContext)
    const [packg, setPackage] = useState(false)
    const [metodoPago, setMetodoPago] = useState("usdt") //!Change this when bitcoin will be reimplemented
    const [cargando, setCargando] = useState(false)
    const [aPagar, setAPagar] = useState(false)
    const [ticker, setTicker] = useState(false)
    const [redirect, setRedirect] = useState(false)
    const binance = new ccxt.binance()
    const tipoPaquetes = [
        t("Pages.Package-request.packages.types.monthly"),
        t("Pages.Package-request.packages.types.quarterly"),
        t("Pages.Package-request.packages.types.biannual"),
        t("Pages.Package-request.packages.types.annual"),
    ]

    useEffect(
        async () => {
            //if(Object.keys(user).length > 0){
            let snapshot = await firebaseDatabase.ref("packages").child(packageID).once("value")
            setPackage(snapshot.val())
            console.log(snapshot.val())
            //setInterval(updateTicker, 30000)
        },
        [
            //user
        ]
    )
    const buscarFacturasPendientes = async () => {
        if (user) {
            let snapshot = await firebaseDatabase.ref("usuarios/" + user.userID + "/pendingInvoice").once("value")
            let pendingInv = snapshot.val()
            console.log("de fac pendiente", pendingInv)
            if (pendingInv) {
                addToast(
                    t("Pages.Package-request.pending-invoice.message"),
                    { appearance: "error", autoDismiss: true, autoDismissTimeout: 10000 }
                )
                setRedirect(pendingInv)
            }
        }
    }
    useEffect(buscarFacturasPendientes, [user])

    useEffect(async () => {
        if (packg.free) {
            metodoPago === "bitcoin" ? setAPagar("0 ₿") : setAPagar("0 USDT")
        }
        switch (parseInt(tipoPlan)) {
            case 0:
                console.log("", packg.price * ticker.ask)
                metodoPago === "bitcoin"
                    ? setAPagar((packg.price / ticker.ask).toFixed(8) + " ₿")
                    : setAPagar((packg.price * ticker.ask).toFixed(2) + " USDT")
                break
            case 1:
                metodoPago === "bitcoin"
                    ? setAPagar((packg.trimPrice / ticker.ask).toFixed(8) + " ₿")
                    : setAPagar((packg.trimPrice * ticker.ask).toFixed(2) + " USDT")
                break
            case 2:
                metodoPago === "bitcoin"
                    ? setAPagar((packg.semPrice / ticker.ask).toFixed(8) + " ₿")
                    : setAPagar((packg.semPrice * ticker.ask).toFixed(2) + " USDT")
                break
            case 3:
                metodoPago === "bitcoin"
                    ? setAPagar((packg.anualPrice / ticker.ask).toFixed(8) + " ₿")
                    : setAPagar((packg.anualPrice * ticker.ask).toFixed(2) + " USDT")
                break
            default:
                break
        }
    }, [ticker, tipoPlan, packg])

    const updateTicker = async () => {
        let tickString = metodoPago === "bitcoin" ? "BTC/EUR" : "EUR/USDT"
        let ticker = await binance.fetch_ticker(tickString)
        console.log("el ticker es", metodoPago, tickString, ticker)
        setTicker(ticker)
        //setTimeout(updateTicker.bind(this), 60000)
    }

    useEffect(updateTicker, [metodoPago])

    const generarFactura = async () => {
        const servidor = metodoPago === "bitcoin" ? Constants.servidorBitcoin() : Constants.servidorTron()
        setCargando(true)
        const response = await Axios.post(servidor + "/makePackageRequest", {
            packageID,
            userID: user.userID,
            tipoPlan,
        })
        setCargando(false)
        if (response.data.success === 1) {
            setRedirect(response.data.facturaID)
        } else {
            addToast(response.data.error, { appearance: "error", autoDismiss: true, autoDismissTimeout: 5000 })
        }
    }

    const obtenPrecio = () => {
        //console.log("al obtener precio", packg, tipoPlan)
        if (packg.free) {
            //console.log("paso a free")
            return t("Pages.Package-request.packages.types.free")
        }
        switch (parseInt(tipoPlan)) {
            case 0:
                return packg.price + " €"
            case 1:
                return packg.trimPrice + " €"
            case 2:
                return packg.semPrice + " €"
            case 3:
                //console.log("paso a 3")
                return packg.anualPrice + " €"
            default:
                //console.log("paso a break")
                break
        }
    }
    const obtenerDescuento = () => {
        if (packg.free) {
            return "0 €"
        }
        switch (parseInt(tipoPlan)) {
            case 0:
                return ""
            case 1:
                return packg.trimDisc + " €"
            case 2:
                return packg.semDisc + " €"
            case 3:
                return packg.anualDisc + " €"
            default:
                break
        }
    }
    const obtenerNormal = () => {
        if (packg.free) {
            return "0 €"
        }
        switch (parseInt(tipoPlan)) {
            case 0:
                return packg.price + " €"
            case 1:
                return packg.trimDisc + packg.trimPrice + " €"
            case 2:
                return packg.semDisc + packg.semPrice + " €"
            case 3:
                return packg.anualDisc + packg.anualPrice + " €"
            default:
                break
        }
    }

    return (
        <Panel smallBackground={true} title="Facturación">
            {redirect && <Redirect to={"/factura/" + redirect} />}
            <div className="container pb-5">
                <div className="row">
                    <div className="col-lg-6 col-md-12 route">
                        <NavLink to="/paquetes" className="route-link">
                            {t("Pages.Package-request.packages.types.title")}
                        </NavLink>{" "}
                        <span className="route-arrow">&gt;</span>{" "}
                        <span className="route-current">{t("Pages.Package-request.packages.types.details")}</span>
                    </div>
                    <div className="col-lg-6 col-md-12 text-right">
                        <NavLink to="/controlPanel/facturas" className="return-button">
                            {t("Pages.Package-request.return-to-panel-button")}
                        </NavLink>
                    </div>
                </div>
            </div>

            <div className="container bg-white">
                <div className="row py-5 d-flex">
                    <div className="col-2 d-flex flex-column pr-0 mr-0">
                        <div className="billing-element selected">
                            {t("Pages.Package-request.billing-element.information")}
                        </div>
                        <div className="billing-element">{t("Pages.Package-request.billing-element.invoice")}</div>
                    </div>
                    {packg && ticker && !cargando ? (
                        <React.Fragment>
                            <div className="col-6 ml-0 info-container">
                                <h3>{t("Pages.Package-request.checkout-info.title")}</h3>
                                <div
                                    style={{
                                        height: "20px",
                                        width: "150px",
                                        borderBottom: "solid 1px #CCC",
                                        marginBottom: "30px",
                                    }}
                                ></div>
                                <h3>
                                    {t("Pages.Package-request.checkout-info.selected-plan")}:{" "}
                                    <strong>{packg.name}</strong>
                                </h3>
                                <ul>
                                    {packg.features.map((feature, i) => (
                                        <li key={i}>{feature}</li>
                                    ))}
                                </ul>
                                <div
                                    style={{
                                        height: "20px",
                                        width: "150px",
                                        borderBottom: "solid 1px #CCC",
                                        marginBottom: "30px",
                                    }}
                                ></div>
                                <h3>
                                    {t("Pages.Package-request.checkout-info.frequency")}:{" "}
                                    <strong>{tipoPaquetes[tipoPlan]}</strong>
                                </h3>
                                <div
                                    style={{
                                        height: "20px",
                                        width: "150px",
                                        //!borderBottom: "solid 1px #CCC",
                                        marginBottom: "30px",
                                    }}
                                ></div>
                                {/* <h3> //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            Método de pago:
                        </h3>
                        <div className="todosMetodos">
                            <div className="metodoPago">
                                <img src="/images/bitcoin.png"/>
                                <span>Bitcoin</span>
                                <input type="radio" checked={metodoPago==="bitcoin"} onChange={ev=>setMetodoPago("bitcoin")} />
                            </div>
                            <div className="metodoPago">
                                <img src="/images/usdt.png"/>
                                <span>USDT (TRON TRC20)</span>
                                <input type="radio" checked={metodoPago==="usdt"} onChange={ev=>setMetodoPago("usdt")} />
                            </div>   //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        </div> */}
                            </div>
                            <div className="col-4 d-flex flex-column justify-content-center align-items-center">
                                <div className="price-section">
                                    <span className="price-title">
                                        {t("Pages.Package-request.checkout-info.price.label")}:{" "}
                                        <span className="price">{obtenPrecio()}</span>
                                    </span>
                                    <span>
                                        {t("Pages.Package-request.checkout-info.price.normal")}: {obtenerNormal()}
                                    </span>
                                    <span>
                                        {t("Pages.Package-request.checkout-info.price.discount")}:{" "}
                                        {obtenerDescuento() || " 0"}
                                    </span>
                                </div>
                                {metodoPago === "bitcoin" ? (
                                    <div className="currency-section">
                                        <span>
                                            <img
                                                src="/images/coins/coin-circle-btc.png"
                                                alt="BITCOIN"
                                                width="80px"
                                                className="mb-3"
                                            />
                                        </span>
                                        <span>{t("Pages.Package-request.checkout-info.aproximate-amount")}:</span>
                                        <span className="price-title price">{aPagar}</span>
                                        <span>
                                            {t("Pages.Package-request.checkout-info.btc-price")}: {ticker.ask} €
                                        </span>
                                    </div>
                                ) : (
                                    <div className="currency-section">
                                        <span>
                                            <img src="/images/usdt.png" alt="USDT" width="80px" className="mb-3" />
                                        </span>
                                        <span>{t("Pages.Package-request.checkout-info.aproximate-amount")}:</span>
                                        <span className="price-title price">{aPagar}</span>
                                        <span>
                                            {t("Pages.Package-request.checkout-info.usdt-price")}: {ticker.ask} €
                                        </span>
                                    </div>
                                )}
                                <button className="generate-button" onClick={(ev) => generarFactura("usdt")}>
                                    {t("Pages.Package-request.checkout-info.submit-button")}
                                </button>
                            </div>
                        </React.Fragment>
                    ) : (
                        <div className="loaderWrap col-10">
                            <Loader type="BallTriangle" color="#9b5bc1" height={100} width={100} />
                        </div>
                    )}
                </div>
            </div>
        </Panel>
    )
}
