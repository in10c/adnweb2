import React, { useContext } from "react"
import { Link, Redirect } from "react-router-dom"
import Loader from "react-loader-spinner"
import { useToasts } from "react-toast-notifications"
import { useTranslation } from "react-i18next"
import * as FirebaseMngr from "../../Utilities/FirebaseMngr"
import { FaEye, FaLaughWink } from "react-icons/fa"
import "./index.css"

export default function Login() {
    const { t } = useTranslation()
    const firebaseAuth = FirebaseMngr.authGetter()
    const { addToast } = useToasts()
    const [redirect, setRedirect] = React.useState(false)
    const [cargando, setCargando] = React.useState(true)
    const [tipoPass, setTipoPass] = React.useState(true)
    const input1 = React.useRef(null)
    const [form, setForm] = React.useState({})
    const handleInputChange = (event) => {
        event.persist()
        setForm((form) => ({ ...form, [event.target.name]: event.target.value }))
    }
    const acceder = async (ev) => {
        setCargando(true)
        ev.preventDefault()
        try {
            let user = await firebaseAuth.signInWithEmailAndPassword(form.correo, form.contrasena)
            console.log("el usuario es", user)
            setRedirect(true)
        } catch (error) {
            setCargando(false)
            console.log("error", error)
            addToast(error.message, { appearance: "error", autoDismiss: true })
        }
    }
    React.useEffect(() => {
        if (firebaseAuth.currentUser) {
            console.log("usuario logueado")
            setRedirect(true)
        } else {
            console.log("usuario NO logueado")
            setCargando(false)
            setTimeout(() => {
                input1.current.focus()
            }, 500)
        }
    }, [])
    return (
        <div className="fullLogin">
            <div className="container-form">
                {redirect && <Redirect to="/panel" />}
                <img src="./images/logoblanco.svg" className="logo" width="100%" style={{ padding: 20 }} />

                {!cargando ? (
                    <form className="box whited" onSubmit={acceder}>
                        <h1 className="form-title">{t("Pages.Login.form.title")}</h1>
                        <input
                            type="email"
                            className="entradaDatos"
                            required
                            ref={input1}
                            placeholder={t("Pages.Login.form.email.placeholder")}
                            name="correo"
                            value={form.correo}
                            onChange={handleInputChange}
                        />
                        <div style={{ width: "100%", position: "relative", zIndex: 2 }}>
                            <input
                                type={tipoPass ? "password" : "text"}
                                className="entradaDatos"
                                placeholder="Contraseña"
                                required
                                minLength="8"
                                name="contrasena"
                                value={form.contrasena}
                                onChange={handleInputChange}
                            />
                            <FaEye
                                size={30}
                                style={{ position: "absolute", right: 0, bottom: 7, cursor: "pointer" }}
                                onClick={(ev) => setTipoPass((tipoPass) => !tipoPass)}
                            />
                        </div>
                        <button type="submit">Iniciar</button>
                        <p>
                            {t("Pages.Login.new-user.label")},{" "}
                            <Link to="/registrar">{t("Pages.Login.new-user.link")}</Link>
                        </p>
                        <p>
                            {t("Pages.Login.fogotten-password.label")},{" "}
                            <Link to="/resetPass">{t("Pages.Login.fogotten-password.link")}</Link>
                        </p>
                    </form>
                ) : (
                    <div className="box whited">
                        <Loader type="BallTriangle" color="white" height={100} width={100} />
                    </div>
                )}
            </div>
        </div>
    )
}
