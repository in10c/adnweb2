import { useEffect } from 'react'

const useOnWindowsResize = (callback) => {
    useEffect(() => {
        window.addEventListener('resize', callback)
        return () => {
            window.removeEventListener('resize', callback)
        }
    }, [callback])
}

export default useOnWindowsResize
