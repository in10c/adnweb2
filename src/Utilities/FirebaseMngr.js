let firebaseDatabase
let firebaseAuth

export function set(data) {
    firebaseDatabase = data
}

export function get(){
    return firebaseDatabase
}

export function authSetter(data) {
    firebaseAuth = data
}

export function authGetter(){
    return firebaseAuth
}