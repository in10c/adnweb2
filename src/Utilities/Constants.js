const Constants = {
    production: true,
    btcExplorer: ()=>{
        if(Constants.production){
            return 'https://blockstream.info/tx'
        } else {
            return 'https://blockstream.info/testnet/tx'
        }
    },
    servidorBitcoin: () => {
        if(Constants.production){
            return 'https://servidor.adncrypto.com:3001'
            //?return 'https://134.209.74.159:3001'
        } else {
            return 'http://localhost:3001'
        }
    },
    servidorTron: () => {
        if(Constants.production){
            return 'https://servidor.adncrypto.com:3002'
            //?return 'https://servidortron.adncrypto.com:3001'
            //?return 'https://134.209.74.159:3002'
        } else {
            return 'http://localhost:3002'
        }
    },
    servidorFunciones: () => {
        if(Constants.production){
            return 'https://us-central1-adncrypto-8f596.cloudfunctions.net'
        } else {
            return 'http://localhost:5001/adncrypto-8f596/us-central1'
        }
    }
};

export default Constants