const dateFromTimeStamp = (timestamp) => {
    const date = new Date(timestamp)

    return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`
}

const meses = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"]
const dias = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"]

const dateWithFormatFromTime = (param) => {
    let fecha = new Date(param)
    return [dias[fecha.getDay()], " " + fecha.getDate() + "/", meses[fecha.getMonth()], "/" + fecha.getFullYear()]
}

export { dateFromTimeStamp, dateWithFormatFromTime }
