import React, { useEffect, useState } from "react"
import Axios from "axios"
import { useTranslation } from "react-i18next"
import Constants from "../Utilities/Constants"
require("firebase/auth")

const UserContext = React.createContext()
const { Provider, Consumer } = UserContext
const UserProvider = ({ children, firebaseDatabase, firebaseAuth }) => {
    const { i18n } = useTranslation()
    const [user, setUser] = useState({})
    const [language, setLanguage] = useState("es")

    const url = Constants.servidorFunciones()

    const [isAnUserAuthenticated, setIsAnUserAuthenticated] = useState(undefined)

    const updateUserData = async (uid) => {
        let snapshot = await firebaseDatabase.ref("usuarios/" + uid).once("value")
        const userData = snapshot.val()
        setUser({ userID: uid, ...userData })
    }

    useEffect(() => {
        console.log("en busca fire")
        if (firebaseAuth) {
            updateAuth()
        } else {
            setTimeout(updateAuth, 2000)
        }
    }, [firebaseAuth])

    useEffect(() => {
        i18n.changeLanguage(language)
    }, [language])

    const updateAuth = async () => {
        firebaseAuth.onAuthStateChanged((authUser) => {
            if (authUser) {
                console.log("el usuario logueado es", authUser)
                firebaseDatabase.ref("users/" + authUser.uid).on("value", (snapshot) => {
                    const userInfo = snapshot.val()
                    if (userInfo) updateUserData(authUser.uid)
                })
                updateUserData(authUser.uid)
                setIsAnUserAuthenticated(true)
                console.log("logged")
            } else {
                setUser({})
                setIsAnUserAuthenticated(false)
                console.log("NOT logged")
            }
        })
    }

    const signUpUser = async (email, password, name) => {
        try {
            const response = await Axios.post(url + "/signUpUser", {
                email,
                password,
                name,
            })
            if (response.data.success === 1) {
                return true
            }
            console.log("Error on signUpUser: ", response.data.error)
            return false
        } catch (error) {
            console.log("Error on signUpUser: ", error)
            return false
        }
    }

    const signUpReferedUser = async (email, password, name, nickname, packageID, referringUser) => {
        try {
            const response = await Axios.post(url + "/registrarUsuarioReferido", {
                correo: email,
                contrasena: password,
                nombre: name,
                apodo: nickname,
                referente: referringUser,
                paquete: packageID,
            })
            if (response.data.success === 1) {
                return true
            }
            console.log("Error on signUpReferedUser: ", response.data.error)
            return false
        } catch (error) {
            console.log("Error on signUpReferedUser: ", error)
            return false
        }
    }

    const setTelegramID = async (telegramID) => {
        try {
            const response = await Axios.post(url + "/setTelegramID", {
                userID: user.userID,
                telegramID,
            })
            if (response.data.success === 1) {
                return true
            }
            console.log("Error on setTelegramID: ", response.data.error)
            return false
        } catch (error) {
            console.log("Error on setTelegramID: ", error)
            return false
        }
    }

    const updateUserInfo = async (name, telegramID) => {
        try {
            const response = await Axios.post(url + "/updateUserInfo", {
                userID: user.userID,
                name,
                telegramID,
            })
            if (response.data.success === 1) {
                return true
            }
            console.log("Error on updateUserInfo: ", response.data.error)
            return false
        } catch (error) {
            console.log("Error on updateUserInfo: ", error)
            return false
        }
    }

    const signInUser = async (email, password) => {
        try {
            const response = await firebaseAuth.signInWithEmailAndPassword(email, password)
            if (!response.user) return false
            return true
        } catch (error) {
            console.log("Error on signInUser")
            console.log("Error code: ", error.code)
            console.log("Error message: ", error.message)
            if (error.code === "auth/user-not-found" || error.code === "auth/wrong-password") {
                return { message: "Usuario y/o contraseña invalidos" }
            }
            return false
        }
    }

    const signOutUser = async () => {
        try {
            await firebaseAuth.signOut()
            console.log("logout", user.userID)
            // await firebase
            //     .database()
            //     .ref("users/" + user.userID)
            //     .off()
            setUser({})
            return true
        } catch (error) {
            console.log("Error on signOutUser")
            console.log("Error code: ", error.code)
            console.log("Error message: ", error.message)
            return false
        }
    }

    const makePackageRequest = async (packageID) => {
        try {
            const response = await Axios.post(url + "/makePackageRequest", {
                userID: user.userID,
                packageID,
            })
            if (response.data.success === 1) {
                updateUserData(user.userID)
                return true
            }
            console.log("Error on makePackageRequest: ", response.data.error)
            return false
        } catch (error) {
            console.log("Error on makePackageRequest: ", error)
            return false
        }
    }

    const toggleStrategy = async (strategyID) => {
        try {
            const response = await Axios.post(url + "/toggleSelectedStrategy", {
                userID: user.userID,
                strategyID,
            })
            if (response.data.success === 1) {
                updateUserData(user.userID)
                return true
            }
            console.log("Error on toggleStrategy: ", response.data.error)
            return false
        } catch (error) {
            console.log("Error on toggleStrategy: ", error)
            return false
        }
    }

    const aprovePackageRequestFromUser = async (invoiceUserID) => {
        try {
            const response = await Axios.post(url + "/aprovePackageRequestFromUser", {
                userID: invoiceUserID,
            })
            if (response.data.success === 1) {
                return true
            }
            console.log("Error on aprovePackageRequestFromUser: ", response.data.error)
            return false
        } catch (error) {
            console.log("Error on aprovePackageRequestFromUser: ", error)
            return false
        }
    }

    const cancelPackageRequestFromUser = async () => {
        try {
            const response = await Axios.post(url + "/cancelPackageRequestFromUser", {
                userID: user.userID,
            })
            if (response.data.success === 1) {
                return true
            }
            console.log("Error on cancelPackageRequestFromUser: ", response.data.error)
            return false
        } catch (error) {
            console.log("Error on cancelPackageRequestFromUser: ", error)
            return false
        }
    }

    const aprovePaymentFromUser = async (paymentID) => {
        try {
            const response = await Axios.post(url + "/aprovePaymentFromUser", {
                userID: user.userID,
                paymentID,
            })
            if (response.data.success === 1) {
                return true
            }
            console.log("Error on aprovePaymentFromUser: ", response.data.error)
            return false
        } catch (error) {
            console.log("Error on aprovePaymentFromUser: ", error)
            return false
        }
    }

    const getLeftDays = () => {
        return (
            30 -
            (Math.ceil(Math.abs(Date.now() - new Date(user.lastUpdate || user.createdAt)) / (1000 * 60 * 60 * 24)) - 1)
        )
    }

    return (
        <Provider
            value={{
                user,
                language,
                setLanguage,
                signInUser,
                signUpUser,
                signUpReferedUser,
                isAnUserAuthenticated,
                makePackageRequest,
                toggleStrategy,
                getLeftDays,
                signOutUser,
                setTelegramID,
                updateUserInfo,
                aprovePackageRequestFromUser,
                cancelPackageRequestFromUser,
                aprovePaymentFromUser,
            }}
        >
            {children}
        </Provider>
    )
}
export { UserProvider, Consumer as UserConsumer, UserContext }
