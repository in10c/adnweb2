/* eslint-disable react/no-array-index-key */
import React, {
    useRef,
    useState,
    useLayoutEffect,
    useEffect,
} from 'react'
import classnames from 'classnames'
import PropTypes from 'prop-types'
import styles from './style.module.css'
import useOnWindowsResize from '../../hooks/useOnWindowsResize'

const AHRowAutoScroller = ({ direction, currentItemIndex, children }) => {
    const autoScrollerRef = useRef(undefined)

    const [scrollerItemSize, setScrollerItemSize] = useState(0)

    useEffect(() => {
        setTimeout(() => {
            const top = direction === 'vertical' ? scrollerItemSize * currentItemIndex : 0
            const left = direction === 'horizontal' ? scrollerItemSize * currentItemIndex : 0
            autoScrollerRef.current.scrollTo({
                top,
                left,
                behavior: 'smooth',
            })
        }, 0)
    }, [direction, currentItemIndex, scrollerItemSize])

    useLayoutEffect(() => {
        const dimentionTag = direction === 'vertical' ? 'clientHeight' : 'clientWidth'
        setScrollerItemSize(autoScrollerRef.current[dimentionTag])
    }, [direction])

    useOnWindowsResize(() => {
        const dimentionTag = direction === 'vertical' ? 'clientHeight' : 'clientWidth'
        setScrollerItemSize(autoScrollerRef.current[dimentionTag])
    })

    const itemStyle = {
        height: direction === 'vertical' ? scrollerItemSize : '100%',
        width: direction === 'horizontal' ? scrollerItemSize : '100%',
    }

    return (
        <div
            ref={autoScrollerRef}
            className={classnames(styles.autoScrollerWrapper, {
                [styles.autoScrollerWrapper_vertical]: direction === 'vertical',
                [styles.autoScrollerWrapper_horizontal]: direction === 'horizontal',
            })}
        >
            {children.map((child, index) => (
                <div
                    key={index}
                    style={itemStyle}
                    className={styles.autoScrollItemWrapper}
                >
                    {child}
                </div>
            ))}
        </div>
    )
}

AHRowAutoScroller.propTypes = {
    direction: PropTypes.oneOf(['horizontal', 'vertical']),
    currentItemIndex: PropTypes.number,
    children: PropTypes.arrayOf(PropTypes.node).isRequired,
}

AHRowAutoScroller.defaultProps = {
    direction: 'vertical',
    currentItemIndex: 0,
}

export default AHRowAutoScroller
