import React from "react"
import Binance from "binance-api-node"
import "./index.css"
const client = Binance()

export default function CarrouselCrypto() {
  const [tickers, setTickers] = React.useState([])

  React.useEffect(async () => {
    let ticks = await client.futuresPrices()
    setTickers(ticks)
  }, [])

  return (
    <div id="criptocards-container">
      <div className="container">
        <div className="row carrusel">
          <div className="carruselPista">
            {Object.keys(tickers).map((index, i) => (
              <div className="elementoCarrusel">
                <div className="row no-gutters" key={i}>
                  <div className="col-4 d-flex align-items-center justify-content-end">
                    <img
                      width="60px"
                      src={
                        "./images/coins/coin-circle-" +
                        index
                          .replace("USDT", "")
                          .replace("BUSD", "")
                          .toLowerCase() +
                        ".png"
                      }
                      alt={index
                        .replace("USDT", "")
                        .replace("BUSD", "")
                        .toLowerCase()}
                      onError={(e) => {
                        e.target.onerror = null
                        e.target.src = "./images/coins/coin-circle-default.png"
                      }}
                    />
                  </div>
                  <div className="col-8">
                    <div className="card-body">
                      <h5
                        className="card-title m-0"
                        style={{ color: "#9b5bc1" }}
                      >
                        <b>{index}</b>
                      </h5>
                      <h6 className="card-text m-0">{tickers[index]} USD</h6>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  )
}
