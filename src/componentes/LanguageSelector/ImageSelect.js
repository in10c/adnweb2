import React, { PropTypes, Component } from "react"
import "./index.css"

class ImageSelect extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }
    componentDidMount() {
        if (this.props.defaultIndex) {
            this.setState({
                selected: this.props.images[this.props.defaultIndex] || "",
            })
        } else {
            this.setState({
                selected: this.props.images[0] || "",
            })
        }

        window.addEventListener("click", this._handleCloseOptions.bind(this))
    }
    componentWillUnmount() {
        window.removeEventListener("click", this._handleCloseOptions.bind(this))
    }
    getInitialState() {
        return {
            selected: "",
        }
    }
    render() {
        var options = []
        for (var i = 0; i < this.props.images.length; i++) {
            options.push(
                <span
                    key={i}
                    className="imageSelect-options-image-wrapper"
                    onClick={this._handleClickOnImage(i, this.props.images[i]).bind(this)}
                >
                    <span
                        className="imageSelect-options-image"
                        style={{
                            backgroundImage: "url('" + this.props.images[i] + "')",
                            width: this.props.width || "30px",
                            height: this.props.height || "30px",
                        }}
                    ></span>
                </span>
            )
        }

        return (
            <span className={"imageSelect"}>
                {this._style}
                <span className="imageSelect-selected" onClick={this._handleOpenOptions.bind(this)}>
                    <span
                        className="imageSelect-selected-image"
                        style={{
                            backgroundImage: "url('" + this.state.selected + "')",
                            width: this.props.width || "30px",
                            height: this.props.height || "30px",
                        }}
                    ></span>
                </span>

                <div className={"imageSelect-options" + (this.state.isOptionsShow ? " show" : "")}>{options}</div>
            </span>
        )
    }

    _handleOpenOptions(e) {
        if (this.props.onOptionsOpen) this.props.onOptionsOpen(e)
        e.stopPropagation()
        this.setState({
            isOptionsShow: true,
        })
    }

    _handleCloseOptions(e) {
        if (this.props.onOptionsClose) this.props.onOptionsClose(e)
        this.setState({
            isOptionsShow: false,
        })
    }

    _handleClickOnImage(index, image) {
        var component = this
        return function () {
            if (component.props.onChange) component.props.onChange(index, image)
            component.setState({
                selected: image,
            })
            component._handleCloseOptions()
        }
    }
}

export default ImageSelect
