import React, { useContext } from "react"
import ImageSelect from "./ImageSelect"
import { UserContext } from "../../Context/UserContext"

export default function LanguageSelector() {
    const { language, setLanguage } = useContext(UserContext)
    const languages = ["es", "en", "pt", "de"]

    const OnChangeLanguage = (value) => {
        setLanguage(languages[value])
    }

    return (
        <div>
            <ImageSelect
                images={[
                    "http://purecatamphetamine.github.io/country-flag-icons/3x2/ES.svg",
                    "http://purecatamphetamine.github.io/country-flag-icons/3x2/US.svg",
                    "http://purecatamphetamine.github.io/country-flag-icons/3x2/PT.svg",
                    "http://purecatamphetamine.github.io/country-flag-icons/3x2/DE.svg",
                ]}
                width={30}
                height={30}
                defaultIndex={languages.indexOf(language)}
                onChange={OnChangeLanguage}
            />
        </div>
    )

    /*return (
        <div>
            <select name="" id="" onChange={OnChangeLanguage}>
                <option value="us">
                dnjdjd
                </option>
                <option value="es-MX">
                🇲🇽
                </option>
            </select>
        </div>
    )*/

    // return (
    //     <div>
    //         <select name="countries">
    //             <option value="NL">🇳🇱&emsp;Netherlands</option>
    //             <option value="DE">🇩🇪&emsp;Germany</option>
    //             <option value="FR">🇫🇷&emsp;France</option>
    //             <option value="ES">🇪🇸&emsp;Spain</option>
    //         </select>
    //     </div>
    // )

    // return (
    //     <div class="select-sim" id="select-color">
    //         <div class="options">
    //             <div class="option">
    //                 <input type="radio" name="color" value="" id="color-" checked />
    //                 <label for="color-">
    //                     <img src="http://placehold.it/22/ffffff/ffffff" alt="" /> Select an option
    //                 </label>
    //             </div>
    //             <div class="option">
    //                 <input type="radio" name="color" value="red" id="color-red" />
    //                 <label for="color-red">
    //                     <img src="http://placehold.it/22/ff0000/ffffff" alt="" /> Red
    //                 </label>
    //             </div>
    //             <div class="option">
    //                 <input type="radio" name="color" value="green" id="color-green" />
    //                 <label for="color-green">
    //                     <img src="http://placehold.it/22/00ff00/ffffff" alt="" /> Green
    //                 </label>
    //             </div>
    //             <div class="option">
    //                 <input type="radio" name="color" value="blue" id="color-blue" />
    //                 <label for="color-blue">
    //                     <img src="http://placehold.it/22/0000ff/ffffff" alt="" /> Blue
    //                 </label>
    //             </div>
    //             <div class="option">
    //                 <input type="radio" name="color" value="yellow" id="color-yellow" />
    //                 <label for="color-yellow">
    //                     <img src="http://placehold.it/22/ffff00/ffffff" alt="" /> Yellow
    //                 </label>
    //             </div>
    //             <div class="option">
    //                 <input type="radio" name="color" value="pink" id="color-pink" />
    //                 <label for="color-pink">
    //                     <img src="http://placehold.it/22/ff00ff/ffffff" alt="" /> Pink
    //                 </label>
    //             </div>
    //             <div class="option">
    //                 <input type="radio" name="color" value="turquoise" id="color-turquoise" />
    //                 <label for="color-turquoise">
    //                     <img src="http://placehold.it/22/00ffff/ffffff" alt="" /> Turquoise
    //                 </label>
    //             </div>
    //         </div>
    //     </div>
    // )
}
