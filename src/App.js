import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom"
import { ToastProvider } from "react-toast-notifications"
import * as FirebaseMngr from "./Utilities/FirebaseMngr"
import Constants from "./Utilities/Constants"
import Inicio from "./paginas/Inicio"
import Registro from "./paginas/Registro"
import ResetPass from "./paginas/ResetPass"
import RegistroReferido from "./paginas/RegistroReferido"
import Principal from "./paginas/Principal"
import Paquetes from "./paginas/Paquetes"
import PackageRequest from "./paginas/PackageRequest"
import Facturas from "./paginas/Facturas"
import Factura from "./paginas/Factura"
import Tree from "./paginas/Tree"
import Pagos from "./paginas/Pagos"
import LinkReferido from "./paginas/LinkReferido"
import NuevoInforme from "./paginas/NuevoInforme"
import Informe from "./paginas/Informe"
import Informes from "./paginas/Informes"
import PanelDeControl from "./paginas/PanelDeControl"
import Login from "./paginas/Login"
import Confirmacion from "./paginas/Confirmacion"
import BitcoinBot from "./paginas/BitcoinBot"
import firebase from "firebase/app"
import { UserProvider } from "./Context/UserContext"
require('firebase/auth');
require('firebase/database');

if (!firebase.apps.length) {
    firebase.initializeApp({
        apiKey: "AIzaSyCQxHCf9-5lU-E1lSqyfHMSQBK5tMe2rMM",
        authDomain: "adncrypto-8f596.firebaseapp.com",
        databaseURL: "https://adncrypto-8f596-default-rtdb.firebaseio.com",
        projectId: "adncrypto-8f596",
        storageBucket: "adncrypto-8f596.appspot.com",
        messagingSenderId: "101011996067",
        appId: "1:101011996067:web:74f04008818ab43e98ae16",
        measurementId: "G-B6PPL041W0",
    })
} else {
    firebase.app() // if already initialized, use that one
}

let databaseFB = firebase.database()
let authFB = firebase.auth()
if(!Constants.production){
    databaseFB.useEmulator("localhost", 9000)
    authFB.useEmulator("http://localhost:9099")
}
FirebaseMngr.set(databaseFB)
FirebaseMngr.authSetter(authFB)

function App() {
    return (
        <UserProvider firebaseDatabase={databaseFB} firebaseAuth={authFB}>
            <ToastProvider>
                <Router>
                    <Switch>
                        <Route exact path="/about">
                            Acerca
                        </Route>
                        <Route exact path="/">
                            <Inicio />
                        </Route>
                        <Route exact path="/registrar">
                            <Registro />
                        </Route>
                        <Route exact path="/resetPass">
                            <ResetPass />
                        </Route>
                        <Route exact path="/registrarReferido/:referringUser">
                            <RegistroReferido />
                        </Route>
                        <Route exact path="/acceder">
                            <Login />
                        </Route>
                        <Route exact path="/panel">
                            <Principal />
                        </Route>
                        <Route exact path="/paquetes">
                            <Paquetes />
                        </Route>
                        <Route exact path="/packageRequest/:packageID/:tipoPlan">
                            <PackageRequest />
                        </Route>
                        <Route exact path="/invoices">
                            <Facturas />
                        </Route>
                        <Route exact path="/factura/:invoiceID">
                            <Factura />
                        </Route>
                        <Route exact path="/tree">
                            <Tree />
                        </Route>
                        <Route exact path="/payments">
                            <Pagos />
                        </Route>
                        <Route exact path="/referLink">
                            <LinkReferido />
                        </Route>
                        <Route exact path="/newReportAFfeaewfAWEGawefgWAEFwefaawefWAFawefAWEFawefawefwaefawef/:reportID?">
                            <NuevoInforme />
                        </Route>
                        <Route exact path="/reports">
                            <Informes />
                        </Route>
                        <Route exact path="/report/:reportID">
                            <Informe />
                        </Route>
                        <Route exact path="/controlPanel/:pestana">
                            <PanelDeControl />
                        </Route>
                        <Route exact path="/confirmacion">
                            <Confirmacion />
                        </Route>
                        <Route exact path="/bitcoinBot">
                            <BitcoinBot />
                        </Route>
                    </Switch>
                </Router>
            </ToastProvider>
        </UserProvider>
    )
}

export default App
