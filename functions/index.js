const functions = require("firebase-functions");
const cors = require('cors')({origin: true});
const axios = require('axios');
var convert = require('xml-js');
const nodemailer = require("nodemailer");
var EmailTemplates = require('swig-email-templates');
// The Firebase Admin SDK to access Cloud Firestore.
const admin = require('firebase-admin');
admin.initializeApp();

exports.registrarUsuario = functions.https.onRequest((request, response) => {
    cors(request, response, async () => {
        let {correo, nombre, apodo, contrasena, paquete, deBitwabi} = request.body
        let user = { email: correo, password: contrasena, displayName: nombre }
        try {
            let userRecord = await admin.auth().createUser(user)
            let link = await admin.auth().generateEmailVerificationLink(correo)
            let transporter = nodemailer.createTransport({
                host: "cpanel115.wnpservers.net",
                port: 465,
                secure: true, // true for 465, false for other ports
                auth: {
                  user: "info@adncrypto.com", // generated ethereal user
                  pass: "m_8SSgm^@b+s", // generated ethereal password
                },
            });
            var templates = new EmailTemplates({
                root: "./mail_templates"
            });
            templates.render('verificarMail.html', { nombre, link }, async (err, html, text, subject) => {
                if(err){
                    console.log("error", err)
                }
                let pruebas = process.env.FIREBASE_DATABASE_EMULATOR_HOST === "localhost:9000"
                console.log("antesde enviar mail", process.env.FIREBASE_DATABASE_EMULATOR_HOST)
                await transporter.sendMail({
                    from: '"ADN Crypto" <info@adncrypto.com>', // sender address
                    to: pruebas ? "criztian.torr3s@gmail.com" : correo, // list of receivers
                    subject: pruebas ?  "Verifique su dirección de correo electrónico DEV" : "Verifique su dirección de correo electrónico", // Subject line
                    text,
                    html, // html body
                })
                let aux = {
                    correo, nombre, apodo, licenciaADN: 0
                }
                if(deBitwabi){
                    aux.deBitwabi = true
                    aux.paquete = paquete
                } else {
                    aux.deBitwabi = false
                }
                await admin.database().ref("usuarios").child(userRecord.uid).set(aux)
                response.send({success:1})  
            })

            
        } catch (error) {
            console.log("ocurrio un eror", error)
            response.send({success:0, error: error.message})   
        }
        
    })  
})  

exports.registrarUsuarioReferido = functions.https.onRequest((request, response) => {
    cors(request, response, async () => {
        let {correo, nombre, apodo, contrasena, referente} = request.body
        if (!correo.trim() || !nombre.trim() || !apodo.trim() || !contrasena.trim() || !referente.trim()) {
            response.send({ success: 0, error: "Todos los parametros son requeridos" })
            return
        }
        let user = { email: correo, password: contrasena, displayName: nombre }
        try {
            let snapshot = await admin
                .database()
                .ref("usuarios/" + referente)
                .once("value")

            const referUser = snapshot.val()

            if (!referUser) {
                response.send({ success: 0, error: "No existe el referente" })
                return
            }

            let userRecord = await admin.auth().createUser(user)
            let link = await admin.auth().generateEmailVerificationLink(correo)
            let transporter = nodemailer.createTransport({
                host: "cpanel115.wnpservers.net",
                port: 465,
                secure: true, // true for 465, false for other ports
                auth: {
                  user: "info@adncrypto.com", // generated ethereal user
                  pass: "m_8SSgm^@b+s", // generated ethereal password
                },
            });
            var templates = new EmailTemplates({
                root: "./mail_templates"
            });
            templates.render('verificarMail.html', { nombre, link }, async (err, html, text, subject) => {
                if(err){
                    console.log("error", err)
                }
                await transporter.sendMail({
                    from: '"ADN Crypto" <info@adncrypto.com>', // sender address
                    to: correo, // list of receivers
                    subject: "Verifique su dirección de correo electrónico", // Subject line
                    text,
                    html, // html body
                });

                await admin.database().ref("usuarios").child(userRecord.uid).set({
                    correo, nombre, apodo, licenciaADN: 0, referido: true
                })

                snapshot = await admin
                    .database()
                    .ref("path/" + referente)
                    .once("value")
    
                let referPath = snapshot.val()

                await admin
                    .database()
                    .ref("path")
                    .child(userRecord.uid)
                    .set(referPath ? [...referPath, referente] : [referente])

                let formatPath = ""
        
                for (let index = 0; referPath && index < referPath.length; index++) {
                    const pathUser = referPath[index]
        
                    formatPath += "/" + pathUser + "/children"
                }

                await admin
                    .database()
                    .ref("tree" + formatPath)
                    .child(referente)
                    .child("children")
                    .child(userRecord.uid)
                    .set({ name: nombre, email: correo, packageID: "1"})

                response.send({success:1})  
            })

            
        } catch (error) {
            console.log("Error en registrarUsuarioReferido", error.message)
            response.send({ success: 0, error: error.message })
        }
        
    })  
})  



exports.aprovePackageRequest = functions.https.onRequest((request, response) => {
    cors(request, response, async () => {
        console.log(request.body)
        const devKey = request.headers["dev-key"]
        if (!devKey || devKey !== secretDeveloperKey) {
            response.send({ success: 0, message: "You don´t have access to here" })
            return
        }
        let { userID } = request.body
        if (!userID.trim()) {
            response.send({ success: 0, error: "userID parameter are required" })
            return
        }
        try {
            let snapshot = await admin
                .database()
                .ref("usuarios/" + userID)
                .once("value")

            const actualUser = snapshot.val()

            if (!actualUser) {
                response.send({ success: 0, error: "User doesn´t exist" })
                return
            }

            if (!actualUser.pendingInvoice) {
                response.send({ success: 0, error: "User don't have a pending invoice" })
                return
            }

            snapshot = await admin
                .database()
                .ref("invoices/" + actualUser.pendingInvoice)
                .once("value")

            const pendingInvoice = snapshot.val()

            if (!pendingInvoice) {
                response.send({ success: 0, error: "PendingInvoice doesn´t exist" })
                return
            }

            await admin
                .database()
                .ref("invoices")
                .update({
                    [actualUser.pendingInvoice + "/pending"]: false,
                    [actualUser.pendingInvoice + "/paid"]: true,
                    [actualUser.pendingInvoice + "/cancelled"]: false,
                    [actualUser.pendingInvoice + "/status"]: "paid",
                })

            await admin
                .database()
                .ref("usuarios")
                .update({
                    [userID + "/lastUpdate"]: admin.database.ServerValue.TIMESTAMP,
                    [userID + "/pendingInvoice"]: null,
                    [userID + "/paquete"]: pendingInvoice.packageID,
                    [userID + "/active"]: true,
                    [userID + "/daysLeft"]: pendingInvoice.generated ? actualUser.daysLeft + 30 : 30,
                })

            // await admin
            //     .database()
            //     .ref("process")
            //     .update({
            //         [userID + "/active"]: true,
            //     })

            snapshot = await admin
                .database()
                .ref("path/" + userID)
                .once("value")

            const userPath = snapshot.val()

            let treeUrl = ""

            for (let index = 0; userPath && index < userPath.length; index++) {
                const referUser = userPath[index]

                treeUrl += referUser + "/children/"
            }
            await admin
                .database()
                .ref("tree/" + treeUrl + userID)
                .child("packageID")
                .set(pendingInvoice.packageID)

            response.send({ success: 1 })

            if (actualUser.refered) {
                snapshot = await admin
                    .database()
                    .ref("packages/" + pendingInvoice.packageID)
                    .once("value")

                const pendingPackage = snapshot.val()

                snapshot = await admin.database().ref("appSettings").once("value")

                const { firstLevelCommission, secondLevelCommission } = snapshot.val()

                if (userPath[userPath.length - 1]) {
                    const referUser = userPath[userPath.length - 1]
                    await admin
                        .database()
                        .ref("payments")
                        .push({
                            userID: referUser,
                            packageID: pendingInvoice.packageID,
                            amount: pendingPackage.price * firstLevelCommission,
                            subject: `Pago por referencia de paquete ${pendingPackage.name} del usuario ${actualUser.nombre} (${actualUser.correo})`,
                            pending: true,
                            paid: false,
                            cancelled: false,
                            status: "pending",
                            createdAt: admin.database.ServerValue.TIMESTAMP,
                        })
                }

                // if (userPath[userPath.length - 2]) {
                //     const referUser = userPath[userPath.length - 2]
                //     await admin
                //         .database()
                //         .ref("payments")
                //         .push({
                //             userID: referUser,
                //             packageID: pendingInvoice.packageID,
                //             amount: pendingPackage.price * secondLevelCommission,
                //             subject: `Pago por referencia de paquete ${pendingPackage.name} del usuario ${actualUser.name} (${actualUser.email})`,
                //             pending: true,
                //             paid: false,
                //             cancelled: false,
                //             status: "pending",
                //             createdAt: admin.database.ServerValue.TIMESTAMP,
                //         })
                // }
            }
        } catch (error) {
            console.log("Error on aprovePackageRequest", error.message)
            response.send({ success: 0, error: error.message })
        }
    })
})

exports.aprovePackageRequestFromUser = functions.https.onRequest((request, response) => {
    cors(request, response, async () => {
        console.log(request.body)
        let { userID } = request.body
        if (!userID.trim()) {
            response.send({ success: 0, error: "userID parameter are required" })
            return
        }
        try {
            let snapshot = await admin
                .database()
                .ref("usuarios/" + userID)
                .once("value")

            const actualUser = snapshot.val()

            if (!actualUser) {
                response.send({ success: 0, error: "User doesn´t exist" })
                return
            }

            if (!actualUser.pendingInvoice) {
                response.send({ success: 0, error: "User don't have a pending invoice" })
                return
            }

            snapshot = await admin
                .database()
                .ref("invoices/" + actualUser.pendingInvoice)
                .once("value")

            const pendingInvoice = snapshot.val()

            if (!pendingInvoice) {
                response.send({ success: 0, error: "PendingInvoice doesn´t exist" })
                return
            }

            await admin
                .database()
                .ref("invoices")
                .update({
                    [actualUser.pendingInvoice + "/pending"]: false,
                    [actualUser.pendingInvoice + "/paid"]: true,
                    [actualUser.pendingInvoice + "/cancelled"]: false,
                    [actualUser.pendingInvoice + "/status"]: "paid",
                })

            await admin
                .database()
                .ref("users")
                .update({
                    [userID + "/lastUpdate"]: admin.database.ServerValue.TIMESTAMP,
                    [userID + "/pendingInvoice"]: null,
                    [userID + "/packageID"]: pendingInvoice.packageID,
                    [userID + "/active"]: true,
                    [userID + "/daysLeft"]: pendingInvoice.generated ? actualUser.daysLeft + 30 : 30,
                })

            // await admin
            //     .database()
            //     .ref("process")
            //     .update({
            //         [userID + "/active"]: true,
            //     })

            snapshot = await admin
                .database()
                .ref("path/" + userID)
                .once("value")

            const userPath = snapshot.val()

            let treeUrl = ""

            for (let index = 0; userPath && index < userPath.length; index++) {
                const referUser = userPath[index]

                treeUrl += referUser + "/children/"
            }
            await admin
                .database()
                .ref("tree/" + treeUrl + userID)
                .child("packageID")
                .set(pendingInvoice.packageID)

            response.send({ success: 1 })

            if (actualUser.refered) {
                snapshot = await admin
                    .database()
                    .ref("packages/" + pendingInvoice.packageID)
                    .once("value")

                const pendingPackage = snapshot.val()

                snapshot = await admin.database().ref("appSettings").once("value")

                const { firstLevelCommission, secondLevelCommission } = snapshot.val()

                if (userPath[userPath.length - 1]) {
                    const referUser = userPath[userPath.length - 1]
                    await admin
                        .database()
                        .ref("payments")
                        .push({
                            userID: referUser,
                            packageID: pendingInvoice.packageID,
                            amount: pendingPackage.price * firstLevelCommission,
                            subject: `Pago por referencia de paquete ${pendingPackage.name} del usuario ${actualUser.nombre} (${actualUser.correo})`,
                            pending: true,
                            paid: false,
                            cancelled: false,
                            status: "pending",
                            createdAt: admin.database.ServerValue.TIMESTAMP,
                        })
                }

                // if (userPath[userPath.length - 2]) {
                //     const referUser = userPath[userPath.length - 2]
                //     await admin
                //         .database()
                //         .ref("payments")
                //         .push({
                //             userID: referUser,
                //             packageID: pendingInvoice.packageID,
                //             amount: pendingPackage.price * secondLevelCommission,
                //             subject: `Pago por referencia de paquete ${pendingPackage.name} del usuario ${actualUser.name} (${actualUser.email})`,
                //             pending: true,
                //             paid: false,
                //             cancelled: false,
                //             status: "pending",
                //             createdAt: admin.database.ServerValue.TIMESTAMP,
                //         })
                // }
            }
        } catch (error) {
            console.log("Error on aprovePackageRequestFromUser", error.message)
            response.send({ success: 0, error: error.message })
        }
    })
})

exports.aprovePaymentFromUser = functions.https.onRequest((request, response) => {
    cors(request, response, async () => {
        console.log(request.body)
        let { userID, paymentID } = request.body
        if (!userID.trim() || !paymentID.trim()) {
            response.send({ success: 0, error: "All parameter are required" })
            return
        }
        try {
            let snapshot = await admin
                .database()
                .ref("usuarios/" + userID)
                .once("value")

            const actualUser = snapshot.val()

            if (!actualUser) {
                response.send({ success: 0, error: "User doesn´t exist" })
                return
            }

            if (!actualUser.admin) {
                response.send({ success: 0, error: "User unauthorized" })
                return
            }

            snapshot = await admin
                .database()
                .ref("payments/" + paymentID)
                .once("value")

            const pendingPayment = snapshot.val()

            if (!pendingPayment) {
                response.send({ success: 0, error: "pendingPayment doesn´t exist" })
                return
            }

            await admin
                .database()
                .ref("payments")
                .update({
                    [paymentID + "/pending"]: false,
                    [paymentID + "/paid"]: true,
                    [paymentID + "/cancelled"]: false,
                    [paymentID + "/status"]: "paid",
                })

            response.send({ success: 1 })
        } catch (error) {
            console.log("Error on aprovePaymentFromUser", error.message)
            response.send({ success: 0, error: error.message })
        }
    })
})

exports.cancelPackageRequest = functions.https.onRequest((request, response) => {
    cors(request, response, async () => {
        console.log(request.body)
        let { userID, facturaID } = request.body
        if (!userID.trim() && !facturaID.trim()) {
            response.send({ success: 0, error: "All parameters are required" })
            return
        }
        try {
            let snapshot = await admin
                .database()
                .ref("usuarios/" + userID)
                .once("value")

            const actualUser = snapshot.val()
            if (!actualUser) {
                response.send({ success: 0, error: "User doesn´t exist" })
                return
            }
            if (!actualUser.pendingInvoice && facturaID != actualUser.pendingInvoice) {
                response.send({ success: 0, error: "El usuario no tiene una factura pendiente o la factura enviada es diferente a la correcta." })
                return
            }

            let snapFac = await admin.database().ref("facturas/"+userID+"/"+facturaID).once("value")
            const antiguaFactura = snapFac.val()

            const urlActivas = !antiguaFactura.red ? "direccionesActivas" : "direccionesActivasTron"
            let snapActiv = await admin.database().ref("appSettings/"+urlActivas+"/"+userID).once("value")
            const direccionActiva = snapActiv.val()
            console.log("antes de new")
            if(direccionActiva && direccionActiva.newOne){ 
                //ponemos la direccion que estaba activa como en espera
                const urlEspera = !antiguaFactura.red ? "direccionesEnEspera" : "direccionesEnEsperaTron"
                await admin.database().ref("appSettings/"+urlEspera+"/").push({
                    derivate : direccionActiva.derivate,
                    wallet : direccionActiva.wallet
                })
            } 
            await admin.database().ref("appSettings/"+urlActivas+"/"+userID).set(null)

            await admin.database().ref("facturas/"+userID+"/"+facturaID)
                .update({
                    pending: false,
                    paid: false,
                    cancelled: true,
                    status: "cancelled",
                    canceledTime: admin.database.ServerValue.TIMESTAMP
                })
            await admin
                .database()
                .ref("usuarios")
                .update({
                    [userID + "/pendingInvoice"]: null,
                })
            response.send({ success: 1 })
        } catch (error) {
            console.log("Error on makeAPackageRequest", error.message)
            response.send({ success: 0, error: error.message })
        }
    })
})

exports.cancelPackageRequestFromUser = functions.https.onRequest((request, response) => {
    cors(request, response, async () => {
        console.log(request.body)
        let { userID } = request.body
        if (!userID.trim()) {
            response.send({ success: 0, error: "userID parameter are required" })
            return
        }
        try {
            let snapshot = await admin
                .database()
                .ref("usuarios/" + userID)
                .once("value")

            const actualUser = snapshot.val()

            if (!actualUser) {
                response.send({ success: 0, error: "User doesn´t exist" })
                return
            }

            if (!actualUser.pendingInvoice) {
                response.send({ success: 0, error: "User don't have a pending invoice" })
                return
            }
            snapshot = await admin
                .database()
                .ref("invoices/" + actualUser.pendingInvoice)
                .once("value")

            const pendingInvoice = snapshot.val()

            if (pendingInvoice.generated) {
                response.send({ success: 0, error: "Trying to cancel a generated invoice" })
                return
            }

            await admin
                .database()
                .ref("invoices")
                .update({
                    [actualUser.pendingInvoice + "/pending"]: false,
                    [actualUser.pendingInvoice + "/paid"]: false,
                    [actualUser.pendingInvoice + "/cancelled"]: true,
                    [actualUser.pendingInvoice + "/status"]: "cancelled",
                })

            await admin
                .database()
                .ref("usuarios")
                .update({
                    [userID + "/pendingInvoice"]: null,
                })
            response.send({ success: 1 })
        } catch (error) {
            console.log("Error on cancelPackageRequestFromUser", error.message)
            response.send({ success: 0, error: error.message })
        }
    })
})

exports.obtenerNoticias = functions.https.onRequest((request, response) => {
    cors(request, response, async () => {
        let respuesta = await axios.get("https://cryptopanic.com/api/v1/posts/?auth_token=8b329ac3d2775c7dccf27520ef9cd2387d8021ed&public=true")
        console.log("response", respuesta.data)
        response.send({success:1, data: respuesta.data})  
    })  
})  

exports.cryptoNoticias = functions.https.onRequest((request, response) => {
    cors(request, response, async () => {
        let respuesta = await axios.get("https://www.criptonoticias.com/feed")
        console.log("response", respuesta.data)
        var json = JSON.parse(convert.xml2json(respuesta.data, { compact: true, ignoreComment: true, spaces: 4}));
        let gettedNews = json.rss.channel.item
        let news = []
        for (let index = 0; index < 3; index++) {
            const element = gettedNews[index];
            news.push({
                title: element.title._text,
                link: element.link._text,
                description: element.description._cdata
            })
            
        }
        response.send({success:1, data: news})  
    })  
})  

exports.validateBWEmail = functions.https.onRequest((request, response) => {
    cors(request, response, async () => {
        let {correo} = request.body
        admin.database().ref("/usuariosBitwabi").orderByChild("email").equalTo(correo).once("value", snap=>{
            let usuario = snap.val()
            console.log("el usuario es ", usuario)
            if(usuario){
                let key = Object.keys(usuario)[0]
                response.send({exito:1, usuario: usuario[key]})  
            } else {
                response.send({exito:0})  
            }
            
        })
    })  
})  

exports.generateReport = functions.https.onRequest((request, response) => {
    cors(request, response, async () => {
        console.log(request.body)
        let { 
            title,
            description,
            returnBenefits,
            instrument,
            symbol,
            startDate,
            endDate,
            advice,
            finalPrice,
            sTotalImport,
            sMultiplier,
            sProfits,
            cImport,
            cPrice,
            cMultiplier,
            cProfits,
            cLosses,
            mImport,
            mPrice,
            mMultiplier,
            mProfits,
            mLosses,
            aImport,
            aPrice,
            aMultiplier,
            aProfits,
            aLosses,
            fundamentalDesc,
            technicalDesc
        } = request.body
        if (
            !title.trim() ||
            !description.trim() ||
            !returnBenefits.trim() ||
            !instrument.trim() ||
            !symbol.trim() ||
            !startDate.trim() ||
            !endDate.trim() ||
            !advice.trim() ||
            !finalPrice.trim() ||
            !sTotalImport.trim() ||
            !sMultiplier.trim() ||
            !sProfits.trim() ||
            !cImport.trim() ||
            !cPrice.trim() ||
            !cMultiplier.trim() ||
            !cProfits.trim() ||
            !cLosses.trim() ||
            !mImport.trim() ||
            !mPrice.trim() ||
            !mMultiplier.trim() ||
            !mProfits.trim() ||
            !mLosses.trim() ||
            !aImport.trim() ||
            !aPrice.trim() ||
            !aMultiplier.trim() ||
            !aProfits.trim() ||
            !aLosses.trim() ||
            !fundamentalDesc ||
            !technicalDesc
            ){
            response.send({ success: 0, error: "All parameters are required" })
            return
        }
        try {
            await admin
                .database()
                .ref("reports")
                .push({
                    title,
                    description,
                    returnBenefits,
                    instrument,
                    symbol,
                    startDate,
                    endDate,
                    advice,
                    finalPrice,
                    sTotalImport,
                    sMultiplier,
                    sProfits,
                    cImport,
                    cPrice,
                    cMultiplier,
                    cProfits,
                    cLosses,
                    mImport,
                    mPrice,
                    mMultiplier,
                    mProfits,
                    mLosses,
                    aImport,
                    aPrice,
                    aMultiplier,
                    aProfits,
                    aLosses,
                    fundamentalDesc,
                    technicalDesc
                })

            response.send({ success: 1 })
        } catch (error) {
            console.log("Error on generateReport", error.message)
            response.send({ success: 0, error: error.message })
        }
    })
})

exports.updateReport = functions.https.onRequest((request, response) => {
    cors(request, response, async () => {
        console.log(request.body)
        let { 
            reportID,
            title,
            description,
            returnBenefits,
            instrument,
            symbol,
            startDate,
            endDate,
            advice,
            finalPrice,
            sTotalImport,
            sMultiplier,
            sProfits,
            cImport,
            cPrice,
            cMultiplier,
            cProfits,
            cLosses,
            mImport,
            mPrice,
            mMultiplier,
            mProfits,
            mLosses,
            aImport,
            aPrice,
            aMultiplier,
            aProfits,
            aLosses,
            fundamentalDesc,
            technicalDesc
        } = request.body
        if (
            !reportID.trim() ||
            !title.trim() ||
            !description.trim() ||
            !returnBenefits.trim() ||
            !instrument.trim() ||
            !symbol.trim() ||
            !startDate.trim() ||
            !endDate.trim() ||
            !advice.trim() ||
            !finalPrice.trim() ||
            !sTotalImport.trim() ||
            !sMultiplier.trim() ||
            !sProfits.trim() ||
            !cImport.trim() ||
            !cPrice.trim() ||
            !cMultiplier.trim() ||
            !cProfits.trim() ||
            !cLosses.trim() ||
            !mImport.trim() ||
            !mPrice.trim() ||
            !mMultiplier.trim() ||
            !mProfits.trim() ||
            !mLosses.trim() ||
            !aImport.trim() ||
            !aPrice.trim() ||
            !aMultiplier.trim() ||
            !aProfits.trim() ||
            !aLosses.trim() ||
            !fundamentalDesc ||
            !technicalDesc
            ){
            response.send({ success: 0, error: "All parameters are required" })
            return
        }
        try {

            let snapshot = await admin.database().ref("reports/" + reportID).once("value")

            const report = snapshot.val()

            if(!report) {
                response.send({ success: 0, error: "Report doesn't exist" })
                return
            }

            await admin
                .database()
                .ref("reports/" + reportID)
                .set({
                    title,
                    description,
                    returnBenefits,
                    instrument,
                    symbol,
                    startDate,
                    endDate,
                    advice,
                    finalPrice,
                    sTotalImport,
                    sMultiplier,
                    sProfits,
                    cImport,
                    cPrice,
                    cMultiplier,
                    cProfits,
                    cLosses,
                    mImport,
                    mPrice,
                    mMultiplier,
                    mProfits,
                    mLosses,
                    aImport,
                    aPrice,
                    aMultiplier,
                    aProfits,
                    aLosses,
                    fundamentalDesc,
                    technicalDesc
                })

            response.send({ success: 1 })
        } catch (error) {
            console.log("Error on generateReport", error.message)
            response.send({ success: 0, error: error.message })
        }
    })
})

//* Scheduled functions

exports.dailyCheckup = functions.pubsub
    .schedule("0 3 * * *")
    .timeZone("America/Mexico_City")
    .onRun(async (context) => {

        const date = new Date()

        console.log(date)

        let snapshot = await admin.database().ref("usuarios").once("value")
        const users = snapshot.val()
        
        snapshot = await admin.database().ref("packages").once("value")
        const packages = snapshot.val()
        
        
        for (const userID in users) {
            let actionLog = "license free"
            const user = users[userID]
            const userPackage = packages[user.licenciaADN] || {free: true}

            if (user.expirationDate < date.getTime()) {
                actionLog = "expirate license " + userPackage.name
                await admin
                    .database()
                    .ref("usuarios")
                    .update({
                        [userID + "/licenciaADN"]: 0,
                        [userID + "/tipoPlan"]: 0,
                        [userID + "/expirationDate"]: null,
                    })
            } else if(!userPackage.free) {
                actionLog = "license " + userPackage.name
            }
            console.log(userID, ":", actionLog)
        }

        return null
    })
